# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd


df = pd.read_csv("Set_TA_with_endpoint", skiprows = 1) 

df[["s1","StartX", "StartY","s2"]] = df["StartPosition"].str.split(pat="^\\(([0-9\\.]+),([0-9\\.]+)\\)$",expand=True)

df[["s3","EndX", "EndY","s4"]] = df["EndPosition"].str.split(pat="^\\(([0-9\\.]+),([0-9\\.]+)\\)$",expand=True)

df=df.drop(columns=['s1', 's2','s3','s4'])

df.to_csv("Set_TA_with_SplitedEndpoint.csv")