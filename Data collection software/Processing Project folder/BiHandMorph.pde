import android.view.MotionEvent;
import android.app.ActivityManager;
import android.annotation.SuppressLint;


import java.io.IOError;
import java.sql.Timestamp;
import android.os.Environment;
import controlP5.*;

//import java.util.concurrent.*;

final float Y_OffsetForComfort=32.5*16;
boolean UsedMousePressed=false;
int UsedMouseX=0;
int UsedMouseY=0;
int pUsedMouseX=0;
int pUsedMouseY=0;
int tempUsedMouseX=0;
int tempUsedMouseY=0;
int oldMouseX=0; 
int oldMouseY=0; 
color oldCorlor;
color colorCursor= color(255,0,0);
color colorCursorTrajectory=color(255,0,255);

color colorInactivated=color(200,200,200);
color colorActivated=color(29,107,0);
color colorAcquired=color(212,199,17);
color colorMissed=color(178,0,25);
color colorBackground=color(226,226,226);
color colorStartLine=color(0);
int widthStartLine=5;
boolean bEnableCursorTrajectory=false;
boolean bEnableCursor=false;

int CursorSize=10;
color colorText=color(0, 102, 153);
int holdImagTime=0;

final boolean bEnableMirrorImage=true;
boolean bEnableRecordTrajectory=true;
boolean bRecordTrajectory=false;
Vector<PVector> TrajectoryVector=new Vector<PVector>();
Vector<PVector> AccelerometerVector=new Vector<PVector>();
Vector<PVector> GyroscopeVector=new Vector<PVector>();

/*log concerned variables*/
File CurrentRecordCSV;
Table CurrentTable;
TableRow CurrentRow;
int CurrentRowId=0;
long StartTime=0;
long EndTime=0;

final int ImagHoldingTime=50;
//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//TimestampBefore=timestamp.getTime();

//int DisableEdgeWidth=140;
void setup() {
  //size(800,1920);
  fullScreen();
  background(colorBackground);
  InitStatePreStartVar();
  initStateStart();
  initStateTTA();
  initStateTCC();
  initStateTCL();
  initStateATA();
  initStateACC();
  initStateACL();
  initIMU();
}

boolean bNeedSpeedWarning=false;
int bNeedSpeedUp=0;// 2 means need to speed up, 1 means need to slow down, 0 means noting to do
void draw() {  
  //println("UsedMousePressed : ",UsedMousePressed);
  if(tempUsedMouseX!=UsedMouseX||tempUsedMouseY!=UsedMouseY){
    pUsedMouseX=tempUsedMouseX;
    pUsedMouseY=tempUsedMouseY;
    tempUsedMouseX=UsedMouseX;
    tempUsedMouseY=UsedMouseY;
  }
  
  //TA_Patch();
    
  if (bEnableCursor){
    fill(oldCorlor);
    noStroke();
    if(bEnableMirrorImage){
      ellipse((width-oldMouseX),oldMouseY,CursorSize,CursorSize);
    }
    else{
      ellipse(oldMouseX,oldMouseY,CursorSize,CursorSize); 
    }
  }
  delay(holdImagTime);
  holdImagTime=0;
  
  if(bRecordTrajectory){
    PVector TempTrajectoryPoint = new PVector(UsedMouseX,UsedMouseY);
    TrajectoryVector.add(TempTrajectoryPoint);
    PVector TempAccelerometerVector = new PVector(ax,ay,az);
    AccelerometerVector.add(TempAccelerometerVector);
    PVector TempGyroscopeVector = new PVector(wx,wy,wz);
    GyroscopeVector.add(TempGyroscopeVector);
  }

  
  switch(CurrentState){
    /*PreProcedure*/
    case StatePreStart:{
      FuncStatePreStart();
    }break;
    case StateStart:{
      FuncStateStart();
    }break;
    
    case StateUpdateExperimentsPara_xml:{
      FuncStateUpdateExperimentsPara_xml();
    }break;
    
    case StateUpdateExperimentRecord_xml:{
      Func_StateUpdateExperimentRecord_xml();
    }break;
    
    case StateCreatRecord_csv:{
      FuncStateCreatRecord_csv();    
    }break;

    case StateIniParameters:{
      FuncStateIniParameters();
    }break;
    
    case StateDecideTask:{
      FuncStateDecideTask();
    }break;
    
    case StateWarnSpeed:{
      FuncStateWarnSpeed();
    }break;
    /*TrainingTA*/
    case StateTrainingTA: {
        FuncStateTrainingTA();
    }break;
    
    case StateTrainingTA_Init: {
      FuncStateTrainingTA_Init();
    }break;
    
    case StateTrainingTA_Decide:{
      FuncStateTrainingTA_Decide();
    }break;
    
    case StateTrainingTA_Wait:{
      FuncStateTrainingTA_Wait();
    }break;
    
    case StateSubTTAinit:{
      FuncStateSubTTAinit();
    }break;
    
    case StateSubTTApressed:{
      FuncStateSubTTApressed();
    }break;
    
    case StateSubTTAjudge:{
      FuncStateSubTTAjudge();
    }break;
    
    case StateTrainingTA_Acquired:{
      FuncStateTrainingTA_Acquired();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingTA_Missed:{
      FuncStateTrainingTA_Missed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    
    /*TrialTA*/
    case StateTrialTA:{
        FuncStateTrialTA();
    }break;
    case StateTrialTA_Init:{
      FuncStateTrialTA_Init();
    }break;
    case StateTrialTA_Decide:{
      FuncStateTrialTA_Decide();
    }break;
    case StateTrialTA_Wait:{
      FuncStateTrialTA_Wait();
    }break;
    case StateSubATAinit:{
      FuncStateSubATAinit();
    }break;
    case StateSubATApressed:{
      FuncStateSubATApressed();
    }break;
    case StateSubATAjudge:{
      FuncStateSubATAjudge();
    }break;
    case StateTrialTA_Acquired:{
      FuncStateTrialTA_Acquired();
    }break;
    case StateTrialTA_Missed:{
      FuncStateTrialTA_Missed();
    }break;
    
    
    /*TrainingCC*/
    case StateTrainingCC :{
      FuncStateTrainingCC();
    }break;
    
    case StateTrainingCC_Decide:{
      FuncStateTrainingCC_Decide();
    }break;
    
    case StateTrainingCC_Wait:{
      FuncStateTrainingCC_Wait();
    }break;
    
    case StateTrainingCC_EnterStartArc:{
      FuncStateTrainingCC_EnterStartArc();
    }break;
    
    case StateTrainingCC_BeginSteer:{
      FuncStateTrainingCC_BeginSteer();
    }break;
    
    case StateTrainingCC_Failed:{
      Func_StateTrainingCC_Failed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingCC_Succeeded:{
      Func_StateTrainingCC_Succeeded();
      holdImagTime=ImagHoldingTime;
    }break;  
    
    /*Trial CC*/
    case StateTrialCC:{
      FuncStateTrialCC();
    }break;
    case StateTrialCC_Decide:{
      FuncStateTrialCC_Decide();
    }break;
    case StateTrialCC_Wait:{
      FuncStateTrialCC_Wait();
    }break;
    case StateTrialCC_EnterStartArc:{
      FuncStateTrialCC_EnterStartArc();
    }break;
    case StateTrialCC_BeginSteer:{
      FuncStateTrialCC_BeginSteer();
    }break;
    case   StateTrialCC_Succeeded:{
      FuncStateTrialCC_Succeeded();
    }break;
    case StateTrialCC_Failed:{
      FuncStateTrialCC_Failed();
    }break;
    /*TrainingCL*/
     case StateTrainingCL:{
       FuncStateTrainingCL();
     }break; 
    case StateTrainingCL_Decide:{
      FuncStateTrainingCL_Decide();
    }break;
    
    case StateTrainingCL_Wait:{
      FuncStateTrainingCL_Wait();
    }break;
    
    case StateTrainingCC_EnterStartBar:{
      FuncStateTrainingCC_EnterStartBar();
    }break;
    
    case StateTrainingCL_BeginSteer:{
      FuncStateTrainingCL_BeginSteer();
    }break;
    
    case StateTrainingCL_Succeeded:{
      FuncStateTrainingCL_Succeeded();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingCL_Failed:{
      FuncStateTrainingCL_Failed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    /*Trial CL*/
      case StateTrialCL:{
        FuncStateTrialCL();
      }break;
    case StateTrialCL_Decide:{
      FuncStateTrialCL_Decide();
    }break;
    case StateTrialCL_Wait:{
      FuncStateTrialCL_Wait();
    }break;
    case StateTrialCC_EnterStartBar:{
      FuncStateTrialCC_EnterStartBar();
    }break;
    case StateTrialCL_BeginSteer:{
      FuncStateTrialCL_BeginSteer();
    }break;
    case StateTrialCL_Succeeded:{
      FuncStateTrialCL_Succeeded();
    }break;
    case StateTrialCL_Failed:{
      FuncStateTrialCL_Failed();
    }break;
    
    case StateSaveCSV:{
      FuncStateSaveCSV();
      holdImagTime=ImagHoldingTime;
    }break;
    case StateFishedXP:{
      FuncStateFishedXP();
    }break;
    
    default:
    // Default executes if the case labels
    println("None");   // don't match the switch parameter
    break;
    
    /*case StateTrainingTA_Init: {
      clear();
    }break;*/
  }
  
  if (bEnableCursorTrajectory){
      stroke(colorCursorTrajectory);
      if (UsedMousePressed == true) {
      strokeWeight(CursorSize); 
      strokeCap(ROUND);
      if(bEnableMirrorImage){
        line((width-UsedMouseX), UsedMouseY, (width-pUsedMouseX), pUsedMouseY);
      }
      else{
        line(UsedMouseX, UsedMouseY, pUsedMouseX, pUsedMouseY);      
      }
    }
  }
  
  if (bEnableCursor){
    if (UsedMousePressed == true) {
      oldMouseX=UsedMouseX;
      oldMouseY=UsedMouseY;
      oldCorlor=get(oldMouseX,oldMouseY);
      noStroke();
      fill(colorCursor);
      if(bEnableMirrorImage){
        ellipse((width-UsedMouseX),UsedMouseY,CursorSize,CursorSize);
      }
      else{
        ellipse(UsedMouseX,UsedMouseY,CursorSize,CursorSize); 
      }
    }
  }

}



@SuppressLint("MissingSuperCall")
@Override
public void onBackPressed()
{

   // super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
}



int StayInOnePosition=0;

void mousePressed(){

  switch(CurrentState){
    case StateSubTTAinit:{
      CurrentState=StateSet.StateSubTTApressed;
    }break;
    case StateStart:{
        if(RectButtonClicked(Pre_StartButtonCenterX,Pre_StartButtonCenterY,Pre_StartButtonWidth,Pre_StartButtonHeight)){
        bPassToStateUpdateExperimentsPara_xml=true;
      }
    }break;
    case StateTrainingTA: {
        if(RectButtonClicked(TTA_StartButtonCenterX,TTA_StartButtonCenterY,TTA_StartButtonWidth,TTA_StartButtonHeight)){
        bPassToTTA_Init=true;
      }
    }break;
    case StateTrialTA:{
      if(RectButtonClicked(ATA_StartButtonCenterX,ATA_StartButtonCenterY,ATA_StartButtonWidth,ATA_StartButtonHeight)){
        bPassToATA_Init=true;
      }
    }break;
    case StateTrainingCC:{
        if(RectButtonClicked(TCC_StartButtonCenterX,TCC_StartButtonCenterY,TCC_StartButtonWidth,TCC_StartButtonHeight)){
        bPassToTCC_Init=true;
      }
    } break;
    case StateTrialCC:{
        if(RectButtonClicked(ACC_StartButtonCenterX,ACC_StartButtonCenterY,ACC_StartButtonWidth,ACC_StartButtonHeight)){
        bPassToACC_Init=true;
      }
    } break;
  
    case StateTrainingCL:{
        if(RectButtonClicked(TCL_StartButtonCenterX,TCL_StartButtonCenterY,TCL_StartButtonWidth,TCL_StartButtonHeight)){
        bPassToTCL_Init=true;
      }
    } break;
    case StateTrialCL:{
        if(RectButtonClicked(ACL_StartButtonCenterX,ACL_StartButtonCenterY,ACL_StartButtonWidth,ACL_StartButtonHeight)){
        bPassToACL_Init=true;
      }
    } break;
    default:break;
  }  
  //File ExternalStorage=Environment.getExternalStorageDirectory();
  //save(ExternalStorage+"/testImag.jpg");
  
 
}
  
 


boolean RectButtonClicked(float x0, float y0, float ButtonWidth, float ButtonHeight ){
  if(bEnableMirrorImage){
        if (UsedMouseX <= (width-x0+ButtonWidth/2.0) && UsedMouseX >= (width-x0-ButtonWidth/2.0) && 
          UsedMouseY >= (y0-ButtonHeight/2.0) && UsedMouseY <= (y0+ButtonHeight/2.0)) {
        return true;
      } else {
        return false;
      } 
  }
  else{
    if (UsedMouseX >= (x0-ButtonWidth/2.0) && UsedMouseX <= (x0+ButtonWidth/2.0) && 
        UsedMouseY >= (y0-ButtonHeight/2.0) && UsedMouseY <= (y0+ButtonHeight/2.0)) {
      return true;
    } else {
      return false;
    }  
  }

}

boolean RectButtonReleased(float x0, float y0, float ButtonWidth, float ButtonHeight,int ReleasedX, int ReleasedY ){
  if (ReleasedX >= (x0-ButtonWidth/2.0) && ReleasedX <= (x0+ButtonWidth/2.0) && 
      ReleasedY >= (y0-ButtonHeight/2.0) && ReleasedY <= (y0+ButtonHeight/2.0)) {
    return true;
  } else {
    return false;
  }
}

void FuncStateWarnSpeed(){
  println("enter EchoSpeedWarningInformation");
      background(colorBackground);
      fill(255);
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      switch(bNeedSpeedUp){
        case 2: text("Please try to speed up", width/2,height/2-Y_OffsetForComfort,600,370.8);holdImagTime=2000;break;
        case 1: text("Please try to slow down", width/2,height/2-Y_OffsetForComfort,600,370.8);holdImagTime=2000;break;
        case 0: break;
        default: break;
      }
       
    switch(OldState){
    case StateTrialTA_Missed:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialTA_Acquired:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialCC_Succeeded:{CurrentState=StateSet.StateTrialCC_Wait;}break;
    case StateTrialCC_Failed:{CurrentState=StateSet.StateTrialCC_Wait;}break;
    case StateTrialCL_Succeeded:{CurrentState=StateSet.StateTrialCL_Wait;}break;
    case StateTrialCL_Failed:{CurrentState=StateSet.StateTrialCL_Wait;}break;
    
    default:{
      println("ERRO STATE IN StateWarnSpeed");
    }break;
  }
  
}

void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.

  if (theEvent.isGroup()) {
    // check if the Event was triggered from a ControlGroup
    println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
  } 
  else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
    
    if(theEvent.getController()==dNoParticipant){
      NumParticipantFromDropDownList=(int) theEvent.getController().getValue();
    }
    else if (theEvent.getController()==dNoXpPhase){
      NumPaseFromDropDownList=(int) theEvent.getController().getValue();
    }
  }
}


final float xLeft=140;
final float xRight=940;
final float yTop=10;
final float ybottom=71*16;

//void initScreenBoard(){
//    xLeft=140;
//    xRight=940;
//    yTop=0;
//    ybottom=71*16;
//}
public boolean surfaceTouchEvent(MotionEvent event) {
    //int numPointers = event.getPointerCount();
    ////UsedMousePressed= false;
    //for(int i=0; i < numPointers; i++) {
    //  float x = event.getX(i);
    //  //float y = event.getY(i);
    //  if(x>xLeft && x<xRight){
        
    //    UsedMousePressed=true;
    //    //UsedMouseX=(int)x;
    //    //UsedMouseY=(int)y;
    //    break;
    //  }
    //}
    
    //if(event.getActionMasked() == MotionEvent.ACTION_UP){
    //    UsedMousePressed=false;
    //}  
    //println(event);
    int numPointers = event.getPointerCount();
    
    switch(event.getActionMasked()){
       case MotionEvent.ACTION_UP:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y=event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=false;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
          TA_Patch();
        }
      mousePressed=false;
      }break;
      case MotionEvent.ACTION_POINTER_UP:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y=event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=false;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
          TA_Patch();
        }
      mousePressed=false;
      }break;
      case MotionEvent.ACTION_DOWN:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y = event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=true;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
        }
        mousePressed=true;
      }break;      
      case MotionEvent.ACTION_POINTER_DOWN:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y = event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=true;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;       
        }
          mousePressed=true;           
      }break;
      case MotionEvent.ACTION_MOVE:{
        for(int i=0; i < numPointers; i++){
          float x = event.getX(i);
          float y = event.getY(i);        
          if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
            UsedMousePressed=true;
            UsedMouseX=(int)x;
            UsedMouseY=(int)y;
            break;
          }
        }
        mousePressed=true;
      }break;
      default:break;
    }
    //if(event.getActionMasked()==MotionEvent.ACTION_POINTER_UP){
    //  int pointIndex=event.getActionIndex();
    //  float x = event.getX(pointIndex);
    //  if(x>xLeft && x<xRight){
    //    UsedMousePressed=false;
    //  }
    //}
    return super.surfaceTouchEvent(event);
}

//void drawCursor(){
//  float x, float y;
  
//}
