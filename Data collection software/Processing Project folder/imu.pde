import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

Context context;
SensorManager manager;
Sensor sensorAccelerometer;
AccelerometerListener listenersensorAccelerometer;
float ax, ay, az;

Sensor sensorGyroscope;
GyroscopeListener listenersensorGyroscope;
float wx, wy, wz;

void initIMU() {
  //fullScreen();
  
  context = getActivity();
  manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
  
  /*Accelerometer*/
  sensorAccelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
  listenersensorAccelerometer = new AccelerometerListener();
  manager.registerListener(listenersensorAccelerometer, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
  /*Gyroscope*/
  sensorGyroscope = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
  listenersensorGyroscope = new GyroscopeListener();
  manager.registerListener(listenersensorGyroscope, sensorGyroscope, SensorManager.SENSOR_DELAY_GAME);
  
  //textFont(createFont("SansSerif", 30 * displayDensity));
}

//void draw() {
//  background(0);
//  text("X: " + ax + "\nY: " + ay + "\nZ: " + az, 0, 0, width, height);
  
//  text("X: " + wx + "\nY: " + wy + "\nZ: " + wz,width/2, height/2);
//}

class AccelerometerListener implements SensorEventListener {
  public void onSensorChanged(SensorEvent event) {
    ax = event.values[0];
    ay = event.values[1];
    az = event.values[2];    
  }
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}

class GyroscopeListener implements SensorEventListener {
  public void onSensorChanged(SensorEvent event) {
    wx = event.values[0];
    wy = event.values[1];
    wz = event.values[2];    
  }
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}
