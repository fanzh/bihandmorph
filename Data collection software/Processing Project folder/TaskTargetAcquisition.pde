import java.util.*;

enum TA_HandUsed{
        Left,
        Right
    }

enum TA_Direction{
        Horizontal,//0
        Vertical //1
    } 
    
enum TA_Amplitude{
        Pixel_144,
        Pixel_288,
        Pixel_576,
    }
enum TA_Width{
        Pixel_24,
        Pixel_48,
        Pixel_96,
    }

public class TargetAcquisitionPara{
    int RepeatNum=0;
    TA_HandUsed handUsed;
    TA_Direction direction;
    TA_Amplitude amplitude;
    TA_Width width;
    public int getTA_Amplitude(){
      switch (amplitude){
        case Pixel_144:{
          return 144;
        }
        case Pixel_288:{
          return 288;
        }
        case Pixel_576:{
          return 576;
        }
        default: {
          println("None valide width value for TA");
          return 0;
        }
      }
    }
    public int getTA_Width(){
      switch (width){
        case Pixel_24:{
          return 24;
        }
        case Pixel_48:{
          return 48;
        }
        case Pixel_96:{
          return 96;
        }
        default: {
          println("None valide width value for TA");
          return 0;
        }
      }
    }
   
}

int NumErrorTA=0;
int NumTotalTA=0;
float ErrorRateTA=0.0f;
float UpperLimitErrorRateTA=0.04f;
float LowerLimitErrorRateTA=0.04f;
boolean bEnableCalculateErrorRateTA=false;
boolean bCalculateErrorRateTA=true;
int TA_WarningState=0;

final int TA_StayInOnePositionThreshold=60;//Nearly 1000ms

public class TargetAcquisitionParavector extends TargetAcquisitionPara{
  Vector<TargetAcquisitionPara> TargetAcquisitionParaVector=new Vector<TargetAcquisitionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initTargetAcquisitionParaVectorManu(){
    
    for(int i=0;i<10;i++){
      TargetAcquisitionParaVector.add(creatTargetAcquisitionPara());
      println(i +"th element been created");
    }
  }
  
  public TargetAcquisitionPara creatTargetAcquisitionPara(){
    TargetAcquisitionPara TempTargetAcquisitionPara= new TargetAcquisitionPara();
    TempTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
    switch(ceil(random(2))){
      case 1:TempTargetAcquisitionPara.direction=TA_Direction.Horizontal;break;
      case 2:TempTargetAcquisitionPara.direction=TA_Direction.Vertical;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 2:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 3:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(ceil(random(3))){
      case 1:TempTargetAcquisitionPara.width=TA_Width.Pixel_24;break;
      case 2:TempTargetAcquisitionPara.width=TA_Width.Pixel_48;break;
      case 3:TempTargetAcquisitionPara.width=TA_Width.Pixel_96;break;
    }
    return TempTargetAcquisitionPara;
  }
  */
}

/*StateTrainingTA concerned variables*/
float TTA_StartButtonCenterX;
float TTA_StartButtonCenterY;
float TTA_StartButtonWidth;
float TTA_StartButtonHeight;
boolean bPassToTTA_Init;

TargetAcquisitionPara CurrentTargetAcquisitionPara=new TargetAcquisitionPara();

TA_HandUsed TA_CurrentHandUsed=TA_HandUsed.Left;
TA_Direction TA_CurrentDirection=TA_Direction.Horizontal;
TA_Amplitude TA_CurrentAmplitude=TA_Amplitude.Pixel_144;
TA_Width TA_CurrentWidth=TA_Width.Pixel_24;

/*StateTrainingTA_Decide concerned variables*/
boolean whichSideToHighlight=false;//False as left, ture as right
float TTA_TargetBarCenterX;
float TTA_TargetBarCenterY;
float TTA_TargetBarWidth;
float TTA_TargetBarHeight;
int TTA_ReleasedX;
int TTA_ReleasedY;

/*StateTrainingTA_Wait concerned variables*/
//int goalTATrainNum=4;
int currentTATrainNum=0;
//StateSubTASet CurrentStateSubTA=StateSubTASet.StateSubTAinit;

TargetAcquisitionParavector TTAParavector= new TargetAcquisitionParavector();
//Semaphore semaphoreTTAParavector = new Semaphore(1);
//Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
long TimestampBefore=0;
long TimestampCurrent=0;
final int awaitingTimeAfterInit =1000;

void initStateTTA(){
  /*StateTrainingTA concerned variables*/
  TTA_StartButtonCenterX=width/2-200;
  TTA_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TTA_StartButtonWidth=250;
  TTA_StartButtonHeight=100;
  bPassToTTA_Init=false;
}

void FuncStateTrainingTA(){
    println("Enter StateTrainingTA");
      background(colorBackground);
      fill(255);
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to \n target acquisition \n training set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TTA_StartButtonCenterX,TTA_StartButtonCenterY,TTA_StartButtonWidth,TTA_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTTA_Init){
                 //println(StateSet.StateTrainingTA_Init); 
          background(colorBackground);
          fill(255);
          
          //TTAParavector.initTargetAcquisitionParaVectorManu();
          //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
          println("StateTrainingTA"+ TTAParavector.TargetAcquisitionParaVector.size());
          if(TTAParavector.TargetAcquisitionParaVector.size()>0){
              CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Right;
                }break;
                case "left":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
                }break;
              }
      
              TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
              switch(TA_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to touch highlighted bar", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to touch highlighted bar", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingTA_Init;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonHeight); */
          }
          else{
            println("None valide training set for TA"); 
          }
        //CurrentState=StateSet.StateTrainingTA_Init;
        bPassToTTA_Init=false;
      }
}

void FuncStateTrainingTA_Init(){
  println("Enter StateTrainingTA_Init");
      //println("StateTrainingTA_Init"+ TTAParavector.TargetAcquisitionParaVector.size());
      //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      //TimestampCurrent=timestamp.getTime();
      //static boolean bAlreadyGetPara=false;
      if(TTAParavector.TargetAcquisitionParaVector.size()>0){
        CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
        TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
      }
      /*if((TimestampCurrent-TimestampBefore)>(2*awaitingTimeAfterInit)){
        CurrentState=StateSet.StateTrainingTA_Decide;
      }*/
      //else if((TimestampCurrent-TimestampBefore)>awaitingTimeAfterInit){
          background(colorBackground);
          fill(255);
          switch (TA_CurrentDirection){
            case Horizontal:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
            }break;
            case Vertical:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
            }break;
            default:{
                println("None valide direction TA"); 
            }break;
          }
          
          CurrentState=StateSet.StateTrainingTA_Decide;
          holdImagTime=1000;
      //}
}

void FuncStateTrainingTA_Decide(){
  println("Enter StateTrainingTA_Decide");
      //println("StateTrainingTA_Decide"+ TTAParavector.TargetAcquisitionParaVector.size());
      background(colorBackground);
      fill(255); 
      //int tempCounter=0;
      if(TTAParavector.TargetAcquisitionParaVector.size()>0){
          //tempCounter++;
          //println(tempCounter);
          CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
          TTAParavector.TargetAcquisitionParaVector.remove(0);
          whichSideToHighlight=(random(1)>0.5)?true:false;
          CurrentState=StateSet.StateTrainingTA_Wait;
          currentTATrainNum=0;
      }
      else{
          //println(tempCounter);
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
      }
}

void FuncStateTrainingTA_Wait(){
      println("Enter StateTrainingTA_Wait");
      if(currentTATrainNum<CurrentTargetAcquisitionPara.RepeatNum){
        whichSideToHighlight=!whichSideToHighlight;
        currentTATrainNum++;
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                if(bEnableMirrorImage){
                  TTA_TargetBarCenterX=whichSideToHighlight?(width-barAmplitude)/2:(width+barAmplitude)/2;
                }
                else{
                  TTA_TargetBarCenterX=whichSideToHighlight?(width+barAmplitude)/2:(width-barAmplitude)/2;                
                }
                TTA_TargetBarCenterY=height/2-Y_OffsetForComfort;
                TTA_TargetBarWidth=barWidth;
                TTA_TargetBarHeight=height+2*Y_OffsetForComfort;
                CurrentState=StateSet.StateSubTTAinit;
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                TTA_TargetBarCenterX=width/2;
                TTA_TargetBarCenterY=whichSideToHighlight?(height+barAmplitude)/2-Y_OffsetForComfort:(height-barAmplitude)/2-Y_OffsetForComfort;
                TTA_TargetBarWidth=width;
                TTA_TargetBarHeight=barWidth;
                CurrentState=StateSet.StateSubTTAinit;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      }
      else{
        CurrentState=StateSet.StateTrainingTA_Decide;
      }
}

void FuncStateSubTTAinit(){
  println("Enter StateSubTTAinit");
        
        // code move to UsedMousePressed Event
        if (UsedMousePressed){
          CurrentState=StateSet.StateSubTTApressed;
        }
        
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}


void FuncStateSubTTApressed(){
  println("Enter StateSubTTApressed");
        background(colorBackground);
        fill(255);
        
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
       if(UsedMousePressed==false){
            CurrentState=StateSet.StateSubTTAjudge;
            TTA_ReleasedX=UsedMouseX;
            TTA_ReleasedY=UsedMouseY;
            StayInOnePosition=0;       
       }
       //else{
       // if(oldMouseX==UsedMouseX &&oldMouseY==UsedMouseY){
       //   StayInOnePosition++;
       //   if(StayInOnePosition>=TA_StayInOnePositionThreshold){
       //     //CurrentState=StateSet.StateSubTTAjudge;
       //     TTA_ReleasedX=UsedMouseX;
       //     TTA_ReleasedY=UsedMouseY;
       //     //StayInOnePosition=0;
       //   }
       // }
       // else{
       //   StayInOnePosition=0;
       // }       
       //}
      
}

void TA_Patch(){
   println("Enter TA_Patch");
   TTA_ReleasedX=UsedMouseX;
   TTA_ReleasedY=UsedMouseY;
   ATA_ReleasedX=UsedMouseX;
   ATA_ReleasedY=UsedMouseY;            
  //if(UsedMousePressed==false){
    switch(CurrentState){
      case StateSubTTApressed:{
            CurrentState=StateSet.StateSubTTAjudge;
            StayInOnePosition=0;
      }break;
      case StateSubATApressed:{
            CurrentState=StateSet.StateSubATAjudge;
            StayInOnePosition=0;      
      }break;
      default:break;
    }
  //}
  //else{
  //  switch(CurrentState){
  //    case StateSubTTAinit:{
  //      CurrentState=StateSet.StateSubTTApressed;
  //    }break;
  //    case StateSubATAinit:{
  //      CurrentState=StateSet.StateSubATApressed;
  //    }break;
  //    default:break;      
  //  }

  //}
  
}

void FuncStateSubTTAjudge(){
  println("Enter StateSubTTAjudge");
      if(RectButtonReleased(TTA_TargetBarCenterX,TTA_TargetBarCenterY,TTA_TargetBarWidth,TTA_TargetBarHeight,TTA_ReleasedX,TTA_ReleasedY)){
        CurrentState=StateSet.StateTrainingTA_Acquired;
      }
      else{
        CurrentState=StateSet.StateTrainingTA_Missed;
      }
}

void FuncStateTrainingTA_Acquired(){
  println("Enter StateTrainingTA_Acquired");
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}


void FuncStateTrainingTA_Missed(){
  println("Enter StateTrainingTA_Missed");
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}

/*StateTrainingTA concerned variables*/
float ATA_StartButtonCenterX;
float ATA_StartButtonCenterY;
float ATA_StartButtonWidth;
float ATA_StartButtonHeight;
boolean bPassToATA_Init;


/*StateTrainingTA_Decide concerned variables*/
//boolean whichSideToHighlight=false;//False as left, ture as right
float ATA_TargetBarCenterX;
float ATA_TargetBarCenterY;
float ATA_TargetBarWidth;
float ATA_TargetBarHeight;
int ATA_ReleasedX;
int ATA_ReleasedY;

/*StateTrainingTA_Wait concerned variables*/
//int goalTATrialNum=4;
int currentTATrialNum=0;
//StateSubTASet CurrentStateSubTA=StateSubTASet.StateSubTAinit;

TargetAcquisitionParavector ATAParavector= new TargetAcquisitionParavector();

void initStateATA(){
  /*StateTrainingTA concerned variables*/
  ATA_StartButtonCenterX=width/2-200;
  ATA_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ATA_StartButtonWidth=250;
  ATA_StartButtonHeight=100;
  bPassToATA_Init=false;
}

void FuncStateTrialTA(){
  println("Enter StateTrialTA");
      background(colorBackground);
      fill(255);
      
      println("StateTrialTA"); 
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to \n target acquisition \n trial set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ATA_StartButtonCenterX,ATA_StartButtonCenterY,ATA_StartButtonWidth,ATA_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToATA_Init){
                 //println(StateSet.StateTrainingTA_Init); 
          background(colorBackground);
          fill(255);
          
          //ATAParavector.initTargetAcquisitionParaVectorManu();
          //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
          println("StateTrainingTA"+ ATAParavector.TargetAcquisitionParaVector.size());
          if(ATAParavector.TargetAcquisitionParaVector.size()>0){
              CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Right;
                }break;
                case "left":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
                }break;
              }
      
              TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
              //TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
              TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
              TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
              switch(TA_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to touch highlighted bar", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to touch highlighted bar", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialTA_Init;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonHeight); */
          }
          else{
            println("None valide trial set for TA"); 
          }
        //CurrentState=StateSet.StateTrainingTA_Init;
        bPassToATA_Init=false;
      }
}

void FuncStateTrialTA_Init(){
    println("Enter StateTrialTA_Init");
      //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      //TimestampCurrent=timestamp.getTime();
      //static boolean bAlreadyGetPara=false;
      if(ATAParavector.TargetAcquisitionParaVector.size()>0){
        CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
        //TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
      }
      /*if((TimestampCurrent-TimestampBefore)>(2*awaitingTimeAfterInit)){
        CurrentState=StateSet.StateTrainingTA_Decide;
      }*/
      //else if((TimestampCurrent-TimestampBefore)>awaitingTimeAfterInit){
          background(colorBackground);
          fill(255);
          switch (TA_CurrentDirection){
            case Horizontal:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
            }break;
            case Vertical:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
            }break;
            default:{
                println("None valide direction TA"); 
            }break;
          }
          
          CurrentState=StateSet.StateTrialTA_Decide;
          
          /*Code concerned error rate */
          bEnableCalculateErrorRateTA=false;
          NumErrorTA=0;
          NumTotalTA=0;
          ErrorRateTA=0.04f; // to avoid triggering warning when just begin test 

          holdImagTime=1000;
      //}
}

boolean bRecordTrialTA=false;
void FuncStateTrialTA_Decide(){
      println("Enter StateTrialTA_Decide");
      background(colorBackground);
      fill(255); 
      //int tempCounter=0;
      if(ATAParavector.TargetAcquisitionParaVector.size()>0){
          //tempCounter++;
          //println(tempCounter);
          CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
          ATAParavector.TargetAcquisitionParaVector.remove(0);
          whichSideToHighlight=(random(1)>0.5)?true:false;
          CurrentState=StateSet.StateTrialTA_Wait;
          bEnableCalculateErrorRateTA=false;
          currentTATrainNum=0;
          bRecordTrialTA=false;
      }
      else{
          //println(tempCounter);
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
      }
}

final int additionalTrainingNum=4;

void FuncStateTrialTA_Wait(){
      println("Enter StateTrialTA_Wait");
      if(currentTATrainNum<CurrentTargetAcquisitionPara.RepeatNum+additionalTrainingNum){
        
         if (currentTATrainNum>=additionalTrainingNum){
              bRecordTrialTA=true;
              bEnableCalculateErrorRateTA=true;// Only trial phase been used to calculate error rate, training phase not.
         } 
        
        if(bRecordTrialTA){
          //CurrentTable= new Table();
          /*log*/
          CurrentRow= CurrentTable.addRow();
          CurrentRowId++;
          CurrentRow.setInt("id", CurrentRowId);
          //println("id:"+ CurrentRowId);
          CurrentRow.setString("TaskType", "TA");
          //println("TaskType:"+ "TA");
          CurrentRow.setString("HandUsed", CurrentHandUsed);
          //println("HandUsed:"+ CurrentHandUsed);
        }
        println("bRecordTrialTA:" + bRecordTrialTA);
        whichSideToHighlight=!whichSideToHighlight;
        currentTATrainNum++;
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                if(bRecordTrialTA){
                  /*log*/
                  CurrentRow.setString("Direction", "Horizontal");
                  CurrentRow.setInt("Amplitude/Length", barAmplitude);
                  CurrentRow.setInt("Width", barWidth);
                }
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                if(bEnableMirrorImage){
                  ATA_TargetBarCenterX=whichSideToHighlight?(width-barAmplitude)/2:(width+barAmplitude)/2;
                  if(bRecordTrialTA){
                    String SubDirection=whichSideToHighlight?"R":"L";
                    /*log*/
                    CurrentRow.setString("SubDirection", SubDirection);
                  }
                }
                else{
                  ATA_TargetBarCenterX=whichSideToHighlight?(width+barAmplitude)/2:(width-barAmplitude)/2;
                  if(bRecordTrialTA){
                    String SubDirection=whichSideToHighlight?"R":"L";
                    /*log*/
                    CurrentRow.setString("SubDirection", SubDirection);
                  }
                }
                ATA_TargetBarCenterY=height/2-Y_OffsetForComfort;
                ATA_TargetBarWidth=barWidth;
                ATA_TargetBarHeight=height+2*Y_OffsetForComfort;
                CurrentState=StateSet.StateSubATAinit;
        }break;
        case Vertical:{
                if(bRecordTrialTA){
                  /*log*/
                  CurrentRow.setString("Direction", "Vertical");
                  CurrentRow.setInt("Amplitude/Length", barAmplitude);
                  CurrentRow.setInt("Width", barWidth);
                }
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                ATA_TargetBarCenterX=width/2;
                ATA_TargetBarCenterY=whichSideToHighlight?(height+barAmplitude)/2-Y_OffsetForComfort:(height-barAmplitude)/2-Y_OffsetForComfort;
                if(bRecordTrialTA){
                  String SubDirection=whichSideToHighlight?"D":"U";
                  /*log*/
                  CurrentRow.setString("SubDirection", SubDirection);
                }
                ATA_TargetBarWidth=width;
                ATA_TargetBarHeight=barWidth;
                CurrentState=StateSet.StateSubATAinit;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      }
      else{
        CurrentState=StateSet.StateTrialTA_Decide;
      }
      
}

void FuncStateSubATAinit(){
  println("Enter StateSubATAinit");
        if (UsedMousePressed == true){
          CurrentState=StateSet.StateSubATApressed;
           if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              StartTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("StartTime", Long.toString(StartTime));
              CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
              CurrentRow.setString("StartTime", Long.toString(StartTime));
              bRecordTrajectory= true;
           }
        }
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}

void FuncStateSubATApressed(){
  println("Enter StateSubATApressed");
        /*
        // move to mouseReleased
        ATA_ReleasedX=UsedMouseX;
        ATA_ReleasedY=UsedMouseY;
        if (UsedMousePressed == false){
          CurrentState=StateSet.StateSubATAjudge;
        }
        */
        
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
       if(UsedMousePressed==false){
          CurrentState=StateSet.StateSubATAjudge;
          ATA_ReleasedX=UsedMouseX;
          ATA_ReleasedY=UsedMouseY;
          StayInOnePosition=0;
       }
       //else{
       //   if(oldMouseX==UsedMouseX &&oldMouseY==UsedMouseY){
       //   StayInOnePosition++;
       //   if(StayInOnePosition>=TA_StayInOnePositionThreshold){
       //     //CurrentState=StateSet.StateSubATAjudge;
       //     ATA_ReleasedX=UsedMouseX;
       //     ATA_ReleasedY=UsedMouseY;
       //     //StayInOnePosition=0;
       //     CurrentRow.setString("Remark", "Over Time");
       //   }
       // }
       //   else{
       //     StayInOnePosition=0;
       //   }
       //}

  
}

void FuncStateSubATAjudge(){
  if(bEnableCalculateErrorRateTA){
    DisplayTA_WarningMsg();
  }
  println("Enter StateSubATAjudge");
      if(RectButtonReleased(ATA_TargetBarCenterX,ATA_TargetBarCenterY,ATA_TargetBarWidth,ATA_TargetBarHeight,ATA_ReleasedX,ATA_ReleasedY)){
        CurrentState=StateSet.StateTrialTA_Acquired;
          if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              EndTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("EndTime", Long.toString(EndTime));
              CurrentRow.setString("State", "Successful");
              CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
              CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
              bRecordTrajectory= false;
           }
      }
      else{
        CurrentState=StateSet.StateTrialTA_Missed;
        if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              EndTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("EndTime", Long.toString(EndTime));
              CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
              CurrentRow.setString("State", "Failed");
              CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
              bRecordTrajectory= false;
              //CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
      }
      
      /*Code concerned error rate */
      if(bCalculateErrorRateTA){
        if(bEnableCalculateErrorRateTA){
          NumTotalTA=NumTotalTA+1;
          println("NumTotalTA :", NumTotalTA,"NumErrorTA:", NumErrorTA, "ErrorRateTA:", ErrorRateTA);
        }
      }
}

void FuncStateTrialTA_Acquired(){
  println("Enter StateTrialTA_Acquired");
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        //if(bRecordTrialTA){
        //  saveTable(CurrentTable, ExternalStorage+CurrentTableName);
        //}
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        
        if(bCalculateErrorRateTA){
          FuncJudgeSpeedWarningTA();
        }
               
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Acquired;
               //holdImagTime=200;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
          
        }break;
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
         if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Acquired;
           }
            else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      
}

void FuncStateTrialTA_Missed(){
  println("Enter StateTrialTA_Missed");
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        
        if(bCalculateErrorRateTA){
            FuncJudgeSpeedWarningTA();
        }
               
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                    
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Missed;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Missed;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
      /*Code concerned error rate */
      if(bCalculateErrorRateTA){
        if(bEnableCalculateErrorRateTA){
          NumErrorTA=NumErrorTA+1;
        }
      }
      
}

void FuncJudgeSpeedWarningTA(){
  println("Enter FuncJudgeSpeedWarningTA");
       if(bEnableCalculateErrorRateTA && NumTotalTA%4==0 && NumTotalTA>=4 ){
           ErrorRateTA= (float)NumErrorTA/ (float)NumTotalTA;
           if(ErrorRateTA>UpperLimitErrorRateTA){
                 CurrentState=StateSet.StateWarnSpeed;
                 TA_WarningState=1; //nead to slow down
           }
           else if(ErrorRateTA<LowerLimitErrorRateTA){
              CurrentState=StateSet.StateWarnSpeed;
              TA_WarningState=2; //nead to speed up
            }
           else{
             TA_WarningState=0;//no need to warn
             CurrentState=StateSet.StateTrialTA_Wait;
           }
        }
        else{
          TA_WarningState=0;
        }
}

void DisplayTA_WarningMsg(){
    String MagWarningTA;
    
    switch(TA_WarningState){
      case 0: return;
      case 1:MagWarningTA="Try to slow down";break;
      case 2:MagWarningTA="Try to speed up";break;
      default: return;
    }
    
    switch (TA_CurrentDirection){
       case Horizontal:{
           switch(TA_CurrentAmplitude){
              case Pixel_144:{
                //return 144;
                rectMode(CORNER);
                textSize(80);
                fill(colorText);
                text(MagWarningTA, 0,100,300,600); 
              }break;
              case Pixel_288:{
                //return 288;
                rectMode(CORNER);
                textSize(80);
                fill(colorText);
                text(MagWarningTA, 0,100,300,600); 
              }break;
              case Pixel_576:{
                 rectMode(CORNER);
                 textSize(80);
                 fill(colorText);
                 text(MagWarningTA, width/2-100,100,300,600); 
              }break;
              default: {
                println("None valide width value for TA");
                //return 0;
              }break;
           }
       }break;
       case Vertical:{
          switch(TA_CurrentAmplitude){
            case Pixel_576:{
               rectMode(CORNER);
               textSize(80);
               fill(colorText);
               text(MagWarningTA, width/2-300,0,800,370.8);             
            }break;
            case Pixel_144:
            case Pixel_288:{
               rectMode(CORNER);
               textSize(80);
               fill(colorText);
               text(MagWarningTA, width/2-300,100,800,370.8);             
            }break;              
            default: {
                println("None valide width value for TA");
                //return 0;
            }break;            
          }

       }break;
       default:{
                  println("None valide direction TA"); 
       }break;
    }
}
