import java.util.*;

enum StateSet{
  /*PreProcedure*/
  StatePreStart,
  StateStart,
  StateUpdateExperimentsPara_xml,
  StateUpdateExperimentRecord_xml,
  StateCreatRecord_csv,
  StateIniParameters,
  StateDecideTask,
  StateWarnSpeed,
  /*Training TA*/
  StateTrainingTA,
  StateTrainingTA_Init,
  StateTrainingTA_Decide,
  StateTrainingTA_Wait,
  /****StateSubTASet****/
    StateSubTTAinit,
    StateSubTTApressed,
    StateSubTTAjudge,
  /********/
  StateTrainingTA_Missed,
  StateTrainingTA_Acquired,
  StateTrialTA_Decide,
  StateTrialTA_Wait,
  
  /*Trial TA*/
  StateTrialTA,
  StateTrialTA_Init,
   /****StateSubTASet****/
    StateSubATAinit,
    StateSubATApressed,
    StateSubATAjudge,
  /********/
  StateTrialTA_Acquired,
  StateTrialTA_Missed,
  
  
  /*Training CC*/
  StateTrainingCC,
  StateTrainingCC_Decide,
  StateTrainingCC_Wait,
  StateTrainingCC_EnterStartArc,
  StateTrainingCC_BeginSteer,
  StateTrainingCC_Succeeded,
  StateTrainingCC_Failed,
  
  /*Trial CC*/
  StateTrialCC,
  StateTrialCC_Decide,
  StateTrialCC_Wait,
  StateTrialCC_EnterStartArc,
  StateTrialCC_BeginSteer,
  StateTrialCC_Succeeded,
  StateTrialCC_Failed,
  
  /*Training CL*/
  StateTrainingCL,
  StateTrainingCL_Decide,
  StateTrainingCL_Wait,
  StateTrainingCC_EnterStartBar,
  StateTrainingCL_BeginSteer,
  StateTrainingCL_Succeeded,
  StateTrainingCL_Failed,
  
    /*Trial CL*/
  StateTrialCL,
  StateTrialCL_Decide,
  StateTrialCL_Wait,
  StateTrialCC_EnterStartBar,
  StateTrialCL_BeginSteer,
  StateTrialCL_Succeeded,
  StateTrialCL_Failed,
  /*save CSV*/
  StateSaveCSV,
  /* Exit XP*/
  StateFishedXP
}

StateSet CurrentState=StateSet.StatePreStart;
StateSet OldState=StateSet.StateTrainingTA;
