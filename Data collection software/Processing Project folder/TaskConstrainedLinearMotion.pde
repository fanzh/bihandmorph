enum CL_HandUsed{
        Left,
        Right
    }
    
enum CL_Direction{
        Leftward,
        Rightward,
        Upward,
        Downward
    }
    
enum CL_Length{
        Pixel_237,
        Pixel_474,
    }
    
enum CL_Width{
        Pixel_40,
        Pixel_54,
        Pixel_80,
    }

public class ConstrainedLinearPara{
    //int RepeatNum=0;
    CL_HandUsed handUsed;
    CL_Direction direction;
    CL_Length length;
    CL_Width width;
}


/*StateTrainingTA concerned variables*/
float TCL_StartButtonCenterX;
float TCL_StartButtonCenterY;
float TCL_StartButtonWidth;
float TCL_StartButtonHeight;
boolean bPassToTCL_Init;
boolean bSucessLinearSteering=false;
boolean bLinearExceed=false;
boolean bLinearEarlyRelease=false;

final boolean enableStartBarEcho=false;

public class ConstrainedLinearMotionPara{
    int RepeatNum=0;
    CL_HandUsed handUsed;
    CL_Direction direction;
    CL_Length length;
    CL_Width width;
    public int getCL_Length(){
      switch (length){
        case Pixel_237:{
          return 237;
        }
        case Pixel_474:{
          return 474;
        }

        default: {
          println("None valide length value for CC");
          return 0;
        }
      }
    }
    
    public int getCL_Width(){
      switch (width){
        case Pixel_40:{
          return 40;
        }
        case Pixel_54:{
          return 54;
        }
        case Pixel_80:{
          return 80;
        }
        default: {
          println("None valide width value for CC");
          return 0;
        }
      }
    }
   
}

//public class CL_AllFailed extends ConstrainedLinearMotionPara{
  boolean L_A474W40= false;
  boolean L_A237W40= false;
  boolean L_A474W54= false;
  boolean L_A237W54= false;
  boolean L_A474W80= false;
  boolean L_A237W80= false;
  boolean R_A474W40= false;
  boolean R_A237W40= false;
  boolean R_A474W54= false;
  boolean R_A237W54= false;
  boolean R_A474W80= false;
  boolean R_A237W80= false; 
  boolean U_A474W40= false;
  boolean U_A237W40= false;
  boolean U_A474W54= false;
  boolean U_A237W54= false;
  boolean U_A474W80= false;
  boolean U_A237W80= false;
  boolean D_A474W40= false;
  boolean D_A237W40= false;
  boolean D_A474W54= false;
  boolean D_A237W54= false;
  boolean D_A474W80= false;
  boolean D_A237W80= false;
  
void initSupCLTasksFlags(){
     L_A474W40= false;
     L_A237W40= false;
     L_A474W54= false;
     L_A237W54= false;
     L_A474W80= false;
     L_A237W80= false;
     R_A474W40= false;
     R_A237W40= false;
     R_A474W54= false;
     R_A237W54= false;
     R_A474W80= false;
     R_A237W80= false; 
     U_A474W40= false;
     U_A237W40= false;
     U_A474W54= false;
     U_A237W54= false;
     U_A474W80= false;
     U_A237W80= false;
     D_A474W40= false;
     D_A237W40= false;
     D_A474W54= false;
     D_A237W54= false;
     D_A474W80= false;
     D_A237W80= false;
}    
  //(left/right)_(L/R/U/D)_(474/237)_(40/54/80)    
  void CL_CheckTakSucceeded(){
    int CheckResult=0;
    switch(CurrentConstrainedLinearMotionPara.direction){
      case Leftward:CheckResult+=100;break;
      case Rightward:CheckResult+=200;break;
      case Upward:CheckResult+=300;break;
      case Downward:CheckResult+=400;break;      
    }
    switch(CurrentConstrainedLinearMotionPara.length){
      case Pixel_474: CheckResult+=10;break;
      case Pixel_237: CheckResult+=20;break;
    }
    switch(CurrentConstrainedLinearMotionPara.width){
      case Pixel_40: CheckResult+=1;break;
      case Pixel_54: CheckResult+=2;break;
      case Pixel_80: CheckResult+=3;break;      
    }
      
    switch(CheckResult){
      case 111:L_A474W40=true;break;
      case 112:L_A474W54=true;break;
      case 113:L_A474W80=true;break;
      case 121:L_A237W40=true;break;
      case 122:L_A237W54=true;break;
      case 123:L_A237W80=true;break; 
      case 211:R_A474W40=true;break;
      case 212:R_A474W54=true;break;
      case 213:R_A474W80=true;break;
      case 221:R_A237W40=true;break;
      case 222:R_A237W54=true;break;
      case 223:R_A237W80=true;break; 
      case 311:U_A474W40=true;break;
      case 312:U_A474W54=true;break;
      case 313:U_A474W80=true;break;
      case 321:U_A237W40=true;break;
      case 322:U_A237W54=true;break;
      case 323:U_A237W80=true;break; 
      case 411:D_A474W40=true;break;
      case 412:D_A474W54=true;break;
      case 413:D_A474W80=true;break;
      case 421:D_A237W40=true;break;
      case 422:D_A237W54=true;break;
      case 423:D_A237W80=true;break; 
    }
  }
 
//public class cl_addFailedTasks{
  //Vector<ConstrainedLinearMotionPara> CLParaVectorToAdd=new Vector<ConstrainedLinearMotionPara>();
//Vector<ConstrainedLinearMotionPara> CLParaVectorToAdd=new Vector<ConstrainedLinearMotionPara>();
//Vector<ConstrainedLinearMotionPara> CL_Sup_Vector=new Vector<ConstrainedLinearMotionPara>();

  void CL_addFailedTaskToVector(){
    //ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();
      if(L_A474W40 &&L_A474W54 &&L_A474W80 &&L_A237W40 &&L_A237W54 &&L_A237W80
       &&R_A474W40 &&R_A474W54 &&R_A474W80 &&R_A237W40 &&R_A237W54 &&R_A237W80
       &&U_A474W40 &&U_A474W54 &&U_A474W80 &&U_A237W40 &&U_A237W54 &&U_A237W80
       &&D_A474W40 &&D_A474W54 &&D_A474W80 &&D_A237W40 &&D_A237W54 &&D_A237W80){
       CurrentState=StateSet.StateDecideTask;
        return;
      } 
      else{
        if(!L_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;
          //CLParaVectorToAdd.add(tempCLParaToAdd);
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W40");
        }
        if(!L_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W54");
        }
        if(!L_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W80");
        }
        if(!L_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W40");
        }
        if(!L_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W54");
        }
        if(!L_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W80");
        }
        if(!R_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W40");
        }
        if(!R_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W54");
        }
        if(!R_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W80");
        }
        if(!R_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W40");
        }
        if(!R_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W54");
        }
        if(!R_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W80");
        }
        if(!U_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W40");
        }
        if(!U_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W54");
        }
        if(!U_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W80");
        }
        if(!U_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W80");
        }
        if(!U_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A237W54");
        }
        if(!U_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A237W80");
        }
        if(!D_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;      
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W40");
        }
        if(!D_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W54");
        }
        if(!D_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W80");
        }
        if(!D_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W40");
        }
        if(!D_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W54");
        }
        if(!D_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W80");
        }
                
                CurrentState=StateSet.StateTrialCL_Decide;
     }

  }
//}



//CL_AllFailed CL_AllFailedData=new CL_AllFailed();

int NumErrorCL=0;
int NumTotalCL=0;
float ErrorRateCL=0.0f;
float UpperLimitErrorRateCL=0.15f;
float LowerLimitErrorRateCL=0.15f;
boolean bEnableCalculateErrorRateCL=false;

public class ConstrainedLinearMotionParavector extends ConstrainedLinearMotionPara{
  Vector<ConstrainedLinearMotionPara> ConstrainedLinearMotionParaVector=new Vector<ConstrainedLinearMotionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initConstrainedLinearMotionParaVectorManu(){
    for(int i=0;i<10;i++){
      ConstrainedLinearMotionParaVector.add(creatConstrainedLinearMotionPara());
      println(i +"th element been created");
    }
  }
  
  public ConstrainedLinearMotionPara creatConstrainedLinearMotionPara(){
    ConstrainedLinearMotionPara TempConstrainedLinearMotionPara= new ConstrainedLinearMotionPara();
    TempConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
    switch(ceil(random(4))){
      case 1:TempConstrainedLinearMotionPara.direction=CL_Direction.Leftward;break;
      case 2:TempConstrainedLinearMotionPara.direction=CL_Direction.Rightward;break;
      case 3:TempConstrainedLinearMotionPara.direction=CL_Direction.Upward;break;
      case 4: TempConstrainedLinearMotionPara.direction=CL_Direction.Downward;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempConstrainedLinearMotionPara.length=CL_Length.Pixel_237;break;
      case 2:TempConstrainedLinearMotionPara.length=CL_Length.Pixel_474;break;
    }
    switch(ceil(random(3))){
      case 1:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_40;break;
      case 2:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_54;break;
      case 3:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_80;break;
    }
    return TempConstrainedLinearMotionPara;
  }
  */
}

void initStateTCL(){
  /*StateTrainingCL concerned variables*/
  TCL_StartButtonCenterX=width/2-200;
  TCL_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TCL_StartButtonWidth=250;
  TCL_StartButtonHeight=100;
  bPassToTCL_Init=false;
}


ConstrainedLinearMotionParavector TCLParavector= new ConstrainedLinearMotionParavector();
ConstrainedLinearMotionPara CurrentConstrainedLinearMotionPara=new ConstrainedLinearMotionPara();

CL_HandUsed CL_CurrentHandUsed=CL_HandUsed.Left;
CL_Direction CL_CurrentDirection=CL_Direction.Leftward;
CL_Length CL_CurrentLength=CL_Length.Pixel_237;
CL_Width CL_CurrentWidth=CL_Width.Pixel_80;

final float startBarExtend=20.0f;
color colorStartBar=color(0, 255, 0);

int currentCLTrainNum=0;
int goalCLTrainNum=4;

void FuncStateTrainingCL(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrainingCL");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained linear motion training set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TCL_StartButtonCenterX,TCL_StartButtonCenterY,TCL_StartButtonWidth,TCL_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTCL_Init){
      //println(StateSet.StateTrainingTA_Init); 
      background(colorBackground);
      fill(255);
          
      //TCLParavector.initConstrainedLinearMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrainingCL"+ TCLParavector.ConstrainedLinearMotionParaVector.size());
      if(TCLParavector.ConstrainedLinearMotionParaVector.size()>0){
              CurrentConstrainedLinearMotionPara=TCLParavector.ConstrainedLinearMotionParaVector.get(0);
               switch(CurrentHandUsed){
                case "right":{//CurrentConstrainedLinearMotionPara
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
                }break;
              }
              CL_CurrentHandUsed=CurrentConstrainedLinearMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedLinearMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedLinearMotionPara.width;
              switch(CL_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingCL_Decide;
              NumTotalCL=0;
              NumErrorCL=0;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonWeight); */
      }
      else{
            println("None valide training set for CL"); 
      }
      //CurrentState=StateSet.StateTrainingTA_Init;
      bPassToTCL_Init=false;
  }
}

void FuncStateTrainingCL_Decide(){
    println("Enter StateTrainingCL_Decide");
    background(colorBackground);
    fill(255);
    if(TCLParavector.ConstrainedLinearMotionParaVector.size()>0){
          CurrentConstrainedLinearMotionPara=TCLParavector.ConstrainedLinearMotionParaVector.get(0);
          TCLParavector.ConstrainedLinearMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrainingCL_Wait;
          currentCLTrainNum=0;
    }
    else{
         CurrentState=StateSet.StateDecideTask;
      println("No more parameters"); 
    }
      
}

void FuncStateTrainingCL_Wait(){
  println("Enter StateTrainingCL_Wait");
  if(currentCLTrainNum<CurrentConstrainedLinearMotionPara.RepeatNum){
      background(colorBackground);
      fill(255);
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
   
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }

          float startBarExtendX_Left=width/2+linearLength/2;
          float startBarExtendX_Right=width/2+startBarExtend+linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
          } 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);

          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }

          float startBarExtendX_Left=width/2-startBarExtend-linearLength/2;
          float startBarExtendX_Right=width/2-linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
                    
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2+linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2+startBarExtend+linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2-startBarExtend-linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2-linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
  }
  
  else{
        CurrentState=StateSet.StateTrainingCL_Decide;
  }
    
}

void FuncStateTrainingCC_EnterStartBar(){
    background(colorBackground);
    fill(255);
    println("Enter StateTrainingCC_EnterStartBar");
    
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX <width/2+linearLength/2){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            println("Leftward");
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            } 
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
          } 
          //text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20,80,50); 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX >width/2-linearLength/2){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
            }             println("Rightward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          if (UsedMousePressed == true && UsedMouseY <height/2+linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(-HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Upward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);         
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          if (UsedMousePressed == true && UsedMouseY >height/2-linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Downward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);        
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
}

void FuncStateTrainingCL_BeginSteer(){
  bEnableCursorTrajectory=true;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){

    case Leftward:{
      if(UsedMousePressed == true){
        if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX>(width/2-linearLength/2))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
        else if(UsedMouseX<=(width/2-linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
        }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    
    case Rightward:{
       if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX<(width/2+linearLength/2))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if((UsedMouseX>=(width/2+linearLength/2))){
                bSucessLinearSteering=true;
                CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    
    case Upward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY>(height/2-linearLength/2-Y_OffsetForComfort))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if(UsedMouseY<=(height/2-linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    case Downward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY<(height/2+linearLength/2-Y_OffsetForComfort))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if (UsedMouseY>=(height/2-Y_OffsetForComfort+linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
  
}

void FuncStateTrainingCL_Succeeded(){
  background(colorBackground);
  fill(255);
  bEnableCursorTrajectory=false;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){
    case Leftward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Rightward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Upward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Downward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
    println("StateTrainingCL_Succeeded");
}
  
void FuncStateTrainingCL_Failed(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
    int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
    int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
    switch (CL_CurrentDirection){
      case Leftward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Rightward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Upward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Downward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
             stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      default:{
              println("None valide direction CL"); 
      }break;
    }
    println("StateTrainingCL_Failed");
    if(bLinearExceed){
      println("bLinearExceed");
    }
    if(bLinearEarlyRelease){
      println("bLinearEarlyRelease");
    }
}



/*StateTrainingTA concerned variables*/
float ACL_StartButtonCenterX;
float ACL_StartButtonCenterY;
float ACL_StartButtonWidth;
float ACL_StartButtonHeight;
boolean bPassToACL_Init;

void initStateACL(){
  /*StateTrainingCL concerned variables*/
  ACL_StartButtonCenterX=width/2-200;
  ACL_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ACL_StartButtonWidth=250;
  ACL_StartButtonHeight=100;
  bPassToACL_Init=false;
}

ConstrainedLinearMotionParavector ACLParavector= new ConstrainedLinearMotionParavector();
//ConstrainedLinearMotionPara CurrentConstrainedLinearMotionPara=new ConstrainedLinearMotionPara();


int currentCLTrialNum=0;
int goalCLTrialNum=4;

void FuncStateTrialCL(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrialCL");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained linear motion Trial set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ACL_StartButtonCenterX,ACL_StartButtonCenterY,ACL_StartButtonWidth,ACL_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToACL_Init){
      //println(StateSet.StateTrialTA_Init); 
      background(colorBackground);
      fill(255);
          
      //ACLParavector.initConstrainedLinearMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrialCL"+ ACLParavector.ConstrainedLinearMotionParaVector.size());
      if(ACLParavector.ConstrainedLinearMotionParaVector.size()>0){
              CurrentConstrainedLinearMotionPara=ACLParavector.ConstrainedLinearMotionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
                }break;
              }
              CL_CurrentHandUsed=CurrentConstrainedLinearMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedLinearMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedLinearMotionPara.width;
              switch(CL_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialCL_Decide;
              initSupCLTasksFlags();
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonWeight); */
      }
      else{
            println("None valide Trial set for CL"); 
      }
      //CurrentState=StateSet.StateTrialTA_Init;
      bPassToACL_Init=false;
  }
}

void FuncStateTrialCL_Decide(){
    println("Enter StateTrialCL_Decide");
    background(colorBackground);
    fill(255);
    if(ACLParavector.ConstrainedLinearMotionParaVector.size()>0){
          CurrentConstrainedLinearMotionPara=ACLParavector.ConstrainedLinearMotionParaVector.get(0);
          ACLParavector.ConstrainedLinearMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrialCL_Wait;
          currentCLTrialNum=0;
    }
    else{
      CL_addFailedTaskToVector();
      //CurrentState=StateSet.StateTrialCL_Wait;
      //currentCLTrialNum=0;
      //CurrentState=StateSet.StateDecideTask;
    }
      
}


void FuncStateTrialCL_Wait(){
  println("Enter StateTrialCL_Wait");
  println("vector size:" + ACLParavector.ConstrainedLinearMotionParaVector.size());
  
  if(currentCLTrialNum<CurrentConstrainedLinearMotionPara.RepeatNum){
      background(colorBackground);
      fill(255);
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      switch(CurrentHandUsed){
       case "right":{
           CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
        }break;
       case "left":{
           CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
        }break;
      }
              
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      
      println("linearLength: "+linearLength);
      println("linearWidth: "+linearWidth);
            
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          float startBarExtendX_Left=width/2+linearLength/2;
          float startBarExtendX_Right=width/2+startBarExtend+linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);
              //}
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          
          //text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20,80,50); 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          float startBarExtendX_Left=width/2-startBarExtend-linearLength/2;
          float startBarExtendX_Right=width/2-linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);
              //}
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2+linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2+startBarExtend+linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);                
              //}
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2-startBarExtend-linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2-linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);                
              //}
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
  }
  
  else{
        CurrentState=StateSet.StateTrialCL_Decide;
  }
    
}

void FuncStateTrialCC_EnterStartBar(){
    background(colorBackground);
    fill(255);
    println("Enter StateTrialCC_EnterStartBar");
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-600,600,370.8); 
    ///*******/
    
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
          rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX <width/2+linearLength/2){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            
            if(bEnableMirrorImage){
              CurrentRow.setString("Direction", "Rightward");
            }
            else{
              CurrentRow.setString("Direction", "Leftward");
            }
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            println("Leftward");
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }                        
            //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX >width/2-linearLength/2){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            if(bEnableMirrorImage){
              CurrentRow.setString("Direction", "Leftward");
            }
            else{
              CurrentRow.setString("Direction", "Rightward");
            }
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);
            
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            println("Rightward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          if (UsedMousePressed == true && UsedMouseY <height/2+linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("Direction", "Upward");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);            
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            //CurrentState=StateSet.StateTrialCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(-HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Upward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);         
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          if (UsedMousePressed == true && UsedMouseY >height/2-linearLength/2-Y_OffsetForComfort){
            
            CurrentState=StateSet.StateTrialCL_BeginSteer;
                        
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("Direction", "Downward");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth); 
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Downward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);        
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
}

void FuncStateTrialCL_BeginSteer(){
  bEnableCursorTrajectory=true;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){

    case Leftward:{
      if(UsedMousePressed == true){
        if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX>(width/2-linearLength/2))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
        else if(UsedMouseX<=(width/2-linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;

        }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    
    case Rightward:{
       if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX<(width/2+linearLength/2))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if((UsedMouseX>=(width/2+linearLength/2))){
                bSucessLinearSteering=true;
                CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    
    case Upward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY>(height/2-linearLength/2-Y_OffsetForComfort))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if(UsedMouseY<=(height/2-linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    case Downward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY<(height/2+linearLength/2-Y_OffsetForComfort))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if (UsedMouseY>=(height/2+linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
  
}

void FuncStateTrialCL_Succeeded(){
  background(colorBackground);
  fill(255);
  bEnableCursorTrajectory=false;
  NumTotalCL++;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  
  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
  EndTime=timestamp.getTime();
  /*log*/
  CurrentRow.setString("EndTime", Long.toString(EndTime));
  CurrentRow.setString("State", "Successful");
  CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
  CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
  CurrentRow.setString("State","Successful");
  
  bRecordTrajectory=false;
  
  switch (CL_CurrentDirection){
    case Leftward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Rightward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Upward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Downward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
    println("StateTrialCL_Succeeded");
    
    CL_CheckTakSucceeded();
}
  
void FuncStateTrialCL_Failed(){
    NumTotalCL++;
    NumErrorCL++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
    int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
    int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
    
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    EndTime=timestamp.getTime();
    /*log*/
    CurrentRow.setString("EndTime", Long.toString(EndTime));
    CurrentRow.setString("State", "Successful");
    CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
    CurrentRow.setString("State","Failed");
    
    bRecordTrajectory=false;
    
    switch (CL_CurrentDirection){
      case Leftward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Rightward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Upward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Downward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
             stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      default:{
              println("None valide direction CL"); 
      }break;
    }
    println("StateTrialCL_Failed");
    if(bLinearExceed){
      println("bLinearExceed");
      CurrentRow.setString("ErrorType","Exceed Path");
    }
    if(bLinearEarlyRelease){
      println("bLinearEarlyRelease");
      CurrentRow.setString("ErrorType","Early Release");
    }
}

void FuncJudgeSpeedWarningCL(){
    if(NumTotalCL%6==0){
      ErrorRateCL= (float)NumErrorCL/ (float)NumTotalCL;
      if(ErrorRateCL>UpperLimitErrorRateCL){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=1;
      }
      else if(ErrorRateCL<LowerLimitErrorRateCL){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=2;
      }
      else{
        CurrentState=StateSet.StateTrialCL_Wait;
        bNeedSpeedUp=0;
       }
    }

}
