enum CC_HandUsed{
        Left,
        Right
    }
    
enum CC_Direction{
        Clockwise, 
        Anticlockwise
    }
    
enum CC_Length{
        Pixel_237,
        Pixel_474,
    }
    
enum CC_Width{
        Pixel_40,
        Pixel_54,
        Pixel_80,
    }

public class ConstrainedCircularPara{
    //int RepeatNum=0;
    CC_HandUsed handUsed;
    CC_Direction direction;
    CC_Length length;
    CC_Width width;
}


/*StateTrainingTA concerned variables*/
float TCC_StartButtonCenterX;
float TCC_StartButtonCenterY;
float TCC_StartButtonWidth;
float TCC_StartButtonHeight;
boolean bPassToTCC_Init;
boolean bSucessCircularSteering=false;
boolean bCircularExceed=false;
boolean bCircularEarlyRelease=false;
boolean bPassSemiCircle=false;
boolean bThreeQuartersCircle=false;
final boolean enableStartArcEcho=false;

public class ConstrainedCircularMotionPara{
    int RepeatNum=0;
    CC_HandUsed handUsed;
    CC_Direction direction;
    CC_Length length;
    CC_Width width;
    public int getCC_Length(){
      switch (length){
        case Pixel_237:{
          return 237;
        }
        case Pixel_474:{
          return 474;
        }

        default: {
          println("None valide length value for CC");
          return 0;
        }
      }
    }
    
    public int getCC_Width(){
      switch (width){
        case Pixel_40:{
          return 40;
        }
        case Pixel_54:{
          return 54;
        }
        case Pixel_80:{
          return 80;
        }
        default: {
          println("None valide width value for CC");
          return 0;
        }
      }
    }
   
}

int NumErrorCC=0;
int NumTotalCC=0;
float ErrorRateCC=0.0f;
float UpperLimitErrorRateCC=0.15f;
float LowerLimitErrorRateCC=0.15f;
boolean bEnableCalculateErrorRateCC=false;


public class ConstrainedCircularMotionParavector extends ConstrainedCircularMotionPara{
  Vector<ConstrainedCircularMotionPara> ConstrainedCircularMotionParaVector=new Vector<ConstrainedCircularMotionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initConstrainedCircularMotionParaVectorManu(){
    for(int i=0;i<10;i++){
      ConstrainedCircularMotionParaVector.add(creatConstrainedCircularMotionPara());
      println(i +"th element been created");
    }
  }
  
  public ConstrainedCircularMotionPara creatConstrainedCircularMotionPara(){
    ConstrainedCircularMotionPara TempConstrainedCircularMotionPara= new ConstrainedCircularMotionPara();
    TempConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
    switch(ceil(random(2))){
      case 1:TempConstrainedCircularMotionPara.direction=CC_Direction.Clockwise;break;
      case 2:TempConstrainedCircularMotionPara.direction=CC_Direction.Anticlockwise;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempConstrainedCircularMotionPara.length=CC_Length.Pixel_237;break;
      case 2:TempConstrainedCircularMotionPara.length=CC_Length.Pixel_474;break;
    }
    switch(ceil(random(3))){
      case 1:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_40;break;
      case 2:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_54;break;
      case 3:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_80;break;
    }
    return TempConstrainedCircularMotionPara;
  }
  */
}

void initStateTCC(){
  /*StateTrainingCC concerned variables*/
  TCC_StartButtonCenterX=width/2-200;
  TCC_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TCC_StartButtonWidth=250;
  TCC_StartButtonHeight=100;
  bPassToTCC_Init=false;
}

boolean circleClicked(float x0, float y0, float arcDiameter,float arcWidth ){
  float rInner=(arcDiameter-arcWidth)/2;
  float rExtern=(arcDiameter+arcWidth)/2;
  if((pow((UsedMouseX-x0),2)+pow((UsedMouseY-y0),2)<=pow(rExtern,2))&&(pow((UsedMouseX-x0),2)+pow((UsedMouseY-y0),2)>=pow(rInner,2))){
   return true;
  } else {
    return false;
  }
}

boolean circleReleased(float x0, float y0, float arcDiameter,float arcWidth,int ReleasedX, int ReleasedY ){
  float rInner=(arcDiameter-arcWidth)/2;
  float rExtern=(arcDiameter+arcWidth)/2;
  if((pow((ReleasedX-x0),2)+pow((ReleasedY-y0),2)<=pow(rExtern,2))&&(pow((ReleasedX-x0),2)+pow((ReleasedY-y0),2)>=pow(rInner,2))){
   return true;
  } else {
    return false;
  }
}

ConstrainedCircularMotionParavector TCCParavector= new ConstrainedCircularMotionParavector();
ConstrainedCircularMotionPara CurrentConstrainedCircularMotionPara=new ConstrainedCircularMotionPara();

CC_HandUsed CC_CurrentHandUsed=CC_HandUsed.Left;
CC_Direction CC_CurrentDirection=CC_Direction.Clockwise;
CC_Length CC_CurrentLength=CC_Length.Pixel_237;
CC_Width CC_CurrentWidth=CC_Width.Pixel_80;
final float degree2randian= (PI/180.0f);
float arcStartAngle=0.0f;
float arcEndAngle=0.0f;
int startArc=15;
color colorStartArc=color(0, 255, 0);

int currentCCTrainNum=0;
int goalCCTrainNum=4;

void FuncStateTrainingCC(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrainingCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained circular motion training set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TCC_StartButtonCenterX,TCC_StartButtonCenterY,TCC_StartButtonWidth,TCC_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTCC_Init){
      //println(StateSet.StateTrainingTA_Init); 
      background(colorBackground);
      fill(255);
          
      //TCCParavector.initConstrainedCircularMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrainingCC"+ TCCParavector.ConstrainedCircularMotionParaVector.size());
      if(TCCParavector.ConstrainedCircularMotionParaVector.size()>0){
              CurrentConstrainedCircularMotionPara=TCCParavector.ConstrainedCircularMotionParaVector.get(0);
              switch(CurrentHandUsed){
                case "right":{//CurrentConstrainedCircularMotionPara
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
                }break;
              }
              CC_CurrentHandUsed=CurrentConstrainedCircularMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedCircularMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedCircularMotionPara.width;
              switch(CC_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingCC_Decide;
      }
      else{
            println("None valide training set for CC"); 
      }
      //CurrentState=StateSet.StateTrainingTA_Init;
      bPassToTCC_Init=false;
  }
}

void FuncStateTrainingCC_Decide(){
    println("Enter StateTrainingCC_Decide");
    background(colorBackground);
    fill(255);
    if(TCCParavector.ConstrainedCircularMotionParaVector.size()>0){
          CurrentConstrainedCircularMotionPara=TCCParavector.ConstrainedCircularMotionParaVector.get(0);
          TCCParavector.ConstrainedCircularMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrainingCC_Wait;
          currentCCTrainNum=0;
    }
    else{
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
    }
      
}

void FuncStateTrainingCC_Wait(){
  println("Enter StateTrainingCC_Wait");
  background(colorBackground);
  fill(255);
  if(currentCCTrainNum<CurrentConstrainedCircularMotionPara.RepeatNum){
      CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
      int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
      int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
      float diameter=circularLength/PI;
      noFill();
      strokeWeight(circularWidth);
      stroke(colorInactivated);
      circle(width/2,height/2-Y_OffsetForComfort,diameter);
      stroke(colorStartLine);
      strokeWeight(widthStartLine);
      strokeCap(SQUARE);
      line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
      
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      
      //switch()
      switch (CC_CurrentDirection){
        case Anticlockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
            }
            else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);             
            }
            println("Anticlockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX>width/2)&&(UsedMouseX<(width/2+diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrainingCC_EnterStartArc;
                         currentCCTrainNum++;
                      }
                  }
              }
            }
          }break;
        case Clockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);           
            }          
            println("Clockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX<width/2)&&(UsedMouseX>(width/2-diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrainingCC_EnterStartArc;
                         currentCCTrainNum++;
                      }
                  }
              }
            }
        }break;
        default:{
                    println("None valide direction CC"); 
         }break;
       }
    }
    else{
        CurrentState=StateSet.StateTrainingCC_Decide;
    }
}

void FuncStateTrainingCC_EnterStartArc(){
    background(colorBackground);
    fill(255);
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorInactivated);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-diameter/2-400,600,370.8); 
    ///*******/
    switch (CC_CurrentDirection){
      case Anticlockwise:{
          
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
          println("Anticlockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX < width/2){
            CurrentState=StateSet.StateTrainingCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
        }
        
      }break;
      
      case Clockwise:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
           
          println("Clockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX > width/2){
            CurrentState=StateSet.StateTrainingCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
        }
      }break;
      default:{
                  println("None valide direction CC"); 
      }break;
    }
}

void FuncStateTrainingCC_BeginSteer(){   
    bEnableCursorTrajectory=true;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    if (UsedMousePressed == true){
       if(!(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth))){
         bCircularExceed=true;
         CurrentState=StateSet.StateTrainingCC_Failed;
       }
       else{
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                if((UsedMouseX>=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             case Clockwise:{
                if((UsedMouseX<=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             
             default:{
                  println("None valide direction CC"); 
             }break;
           }
         } 
         
         if(bPassSemiCircle){
            println("PassSemiCircle");
            if(UsedMouseY<height/2-Y_OffsetForComfort){
              bThreeQuartersCircle=true;
            }
         }
         
         if(bThreeQuartersCircle){
           println("ThreeQuartersCircle");
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                 if((UsedMouseX<=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrainingCC_Succeeded;
                       println("Succeeded steering");
                     }
             }break;
             case Clockwise:{
                   //&&(UsedMouseY<(height/2-diameter/2-circularWidth/2))
                   if((UsedMouseX>=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrainingCC_Succeeded;
                       println("Succeeded steering");
                     }
             }break;
             default:{
                        println("None valide direction C"); 
             }break;
           }
         }
       }
       

    if ((!UsedMousePressed) &&(!bSucessCircularSteering)){
          bCircularEarlyRelease=true;
          CurrentState=StateSet.StateTrainingCC_Failed;
          //println("EarlyRelease");
    }
}

void Func_StateTrainingCC_Failed(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorMissed);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    if(bCircularExceed){
      println("Exceed Path");
    }
    if(bCircularEarlyRelease){
      println("Early Release");
    }
    CurrentState=StateSet.StateTrainingCC_Wait;
}

void Func_StateTrainingCC_Succeeded(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorAcquired);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    println("Succeeded steering");
    CurrentState=StateSet.StateTrainingCC_Wait;
}

/*StateTrainingTA concerned variables*/
float ACC_StartButtonCenterX;
float ACC_StartButtonCenterY;
float ACC_StartButtonWidth;
float ACC_StartButtonHeight;
boolean bPassToACC_Init=false;

void initStateACC(){
  /*StateTrainingCC concerned variables*/
  ACC_StartButtonCenterX=width/2-200;
  ACC_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ACC_StartButtonWidth=250;
  ACC_StartButtonHeight=100;
  bPassToACC_Init=false;
}
ConstrainedCircularMotionParavector ACCParavector= new ConstrainedCircularMotionParavector();
int currentCCTrialNum=0;

void FuncStateTrialCC(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrialCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained circular motion Trial set ", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ACC_StartButtonCenterX,ACC_StartButtonCenterY,ACC_StartButtonWidth,ACC_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToACC_Init){
      //println(StateSet.StateTrialTA_Init); 
      background(colorBackground);
      fill(255);
          
      //ACCParavector.initConstrainedCircularMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrialCC"+ ACCParavector.ConstrainedCircularMotionParaVector.size());
      if(ACCParavector.ConstrainedCircularMotionParaVector.size()>0){
              CurrentConstrainedCircularMotionPara=ACCParavector.ConstrainedCircularMotionParaVector.get(0);
              switch(CurrentHandUsed){
                case "right":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
                }break;
              }
              CC_CurrentHandUsed=CurrentConstrainedCircularMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedCircularMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedCircularMotionPara.width;
              switch(CC_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialCC_Decide;
              initSupCCTasksFlags();
              NumTotalCC=0;
              NumErrorCC=0;
      }
      else{
            println("None valide Trial set for CC"); 
      }
      //CurrentState=StateSet.StateTrialTA_Init;
      bPassToACC_Init=false;
  }
}

  boolean C_A474W40= false;
  boolean C_A237W40= false;
  boolean C_A474W54= false;
  boolean C_A237W54= false;
  boolean C_A474W80= false;
  boolean C_A237W80= false;
  boolean A_A474W40= false;
  boolean A_A237W40= false;
  boolean A_A474W54= false;
  boolean A_A237W54= false;
  boolean A_A474W80= false;
  boolean A_A237W80= false; 
  
  void initSupCCTasksFlags(){
     C_A474W40= false;
     C_A237W40= false;
     C_A474W54= false;
     C_A237W54= false;
     C_A474W80= false;
     C_A237W80= false;
     A_A474W40= false;
     A_A237W40= false;
     A_A474W54= false;
     A_A237W54= false;
     A_A474W80= false;
     A_A237W80= false;   
  }
  void CC_CheckTakSucceeded(){
    int CheckResult=0;
    switch(CurrentConstrainedCircularMotionPara.direction){
      case Clockwise:CheckResult+=100;break;
      case Anticlockwise:CheckResult+=200;break;      
    }
    switch(CurrentConstrainedCircularMotionPara.length){
      case Pixel_474: CheckResult+=10;break;
      case Pixel_237: CheckResult+=20;break;
    }
    switch(CurrentConstrainedCircularMotionPara.width){
      case Pixel_40: CheckResult+=1;break;
      case Pixel_54: CheckResult+=2;break;
      case Pixel_80: CheckResult+=3;break;      
    }
      
    switch(CheckResult){
      case 111:C_A474W40=true;break;
      case 112:C_A474W54=true;break;
      case 113:C_A474W80=true;break;
      case 121:C_A237W40=true;break;
      case 122:C_A237W54=true;break;
      case 123:C_A237W80=true;break; 
      case 211:A_A474W40=true;break;
      case 212:A_A474W54=true;break;
      case 213:A_A474W80=true;break;
      case 221:A_A237W40=true;break;
      case 222:A_A237W54=true;break;
      case 223:A_A237W80=true;break; 
    }
  }
 
//public class cl_addFailedTasks{
  //Vector<ConstrainedCircularMotionPara> CLParaVectorToAdd=new Vector<ConstrainedCircularMotionPara>();
//Vector<ConstrainedCircularMotionPara> CLParaVectorToAdd=new Vector<ConstrainedCircularMotionPara>();
//Vector<ConstrainedCircularMotionPara> CC_Sup_Vector=new Vector<ConstrainedCircularMotionPara>();

  void CC_addFailedTaskToVector(){
    //ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();
      if(C_A474W40 &&C_A474W54 &&C_A474W80 &&C_A237W40 &&C_A237W54 &&C_A237W80
       &&A_A474W40 &&A_A474W54 &&A_A474W80 &&A_A237W40 &&A_A237W54 &&A_A237W80){
       CurrentState=StateSet.StateDecideTask;
        return;
      } 
      else{
        if(!C_A474W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;
          //CLParaVectorToAdd.add(tempCCParaToAdd);
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W40");
        }
        if(!C_A474W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W54");
        }
        if(!C_A474W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W80");
        }
        if(!C_A237W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W40");
        }
        if(!C_A237W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W54");
        }
        if(!C_A237W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W80");
        }
        if(!A_A474W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W40");
        }
        if(!A_A474W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W54");
        }
        if(!A_A474W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W80");
        }
        if(!A_A237W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W40");
        }
        if(!A_A237W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W54");
        }
        if(!A_A237W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          tempCCParaToAdd.handUsed=CC_HandUsed.Left;//tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W80");
        }
                
                CurrentState=StateSet.StateTrialCC_Decide;
     }

  }
  
void FuncStateTrialCC_Decide(){
    println("Enter StateTrainingCC_Decide");
    background(colorBackground);
    fill(255);
    if(ACCParavector.ConstrainedCircularMotionParaVector.size()>0){
          CurrentConstrainedCircularMotionPara=ACCParavector.ConstrainedCircularMotionParaVector.get(0);
          ACCParavector.ConstrainedCircularMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrialCC_Wait;
          currentCCTrialNum=0;
    }
    else{
          //CurrentState=StateSet.StateDecideTask;
          //println("No more parameters"); 
          CC_addFailedTaskToVector();
    }
}

void FuncStateTrialCC_Wait(){
  println("Enter StateTrialCC_Wait");
  if(currentCCTrialNum<CurrentConstrainedCircularMotionPara.RepeatNum){
    
      background(colorBackground);
      fill(255);
      CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
      int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
      int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
      float diameter=circularLength/PI;
      noFill();
      strokeWeight(circularWidth);
      stroke(colorInactivated);
      circle(width/2,height/2-Y_OffsetForComfort,diameter);
      stroke(colorStartLine);
      strokeWeight(widthStartLine);
      strokeCap(SQUARE);
      line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
      
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      
      //switch()
      switch (CC_CurrentDirection){
        case Anticlockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Clockwise");
            }
            else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
              //CurrentRow.setString("Direction", "Anticlockwise");
            }
            
            println("Anticlockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX>width/2)&&(UsedMouseX<(width/2+diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrialCC_EnterStartArc;
                          bRecordTrajectory=true;
                          currentCCTrialNum++;
                          /*log*/
                          CurrentRow= CurrentTable.addRow();                       
                          CurrentRowId++;
                          CurrentRow.setInt("id", CurrentRowId);
                          CurrentRow.setString("TaskType", "CC");
                          CurrentRow.setString("HandUsed", CurrentHandUsed);
                         if(bEnableMirrorImage){ 
                            CurrentRow.setString("Direction", "Clockwise");
                          }
                        else{ 
                            CurrentRow.setString("Direction", "Anticlockwise");
                          }
                      }
                  }
              }
            }
          }break;
        case Clockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            
            if(bEnableMirrorImage){
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Clockwise");
            }
            else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Anticlockwise");
            }
            
            println("Clockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX<width/2)&&(UsedMouseX>(width/2-diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrialCC_EnterStartArc;
                          bRecordTrajectory=true;
                          currentCCTrialNum++;
                          /*log*/
                          CurrentRow= CurrentTable.addRow();                       
                          CurrentRowId++;
                          CurrentRow.setInt("id", CurrentRowId);
                          CurrentRow.setString("TaskType", "CC");
                          CurrentRow.setString("HandUsed", CurrentHandUsed);
                          
                            if(bEnableMirrorImage){
                              CurrentRow.setString("Direction", "Anticlockwise");
                            }
                            else{
                              CurrentRow.setString("Direction", "Clockwise");
                            }
                         currentCCTrialNum++;
                      }
                  }
              }
            }
        }break;
        default:{
                    println("None valide direction CC"); 
         }break;
       }
    }
    else{
        CurrentState=StateSet.StateTrialCC_Decide;
    }
}

void FuncStateTrialCC_EnterStartArc(){
    background(colorBackground);
    fill(255);
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    /*log*/
    CurrentRow.setInt("Amplitude/Length", circularLength);
    CurrentRow.setInt("Width", circularWidth);
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorInactivated);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-diameter/2-400,600,370.8); 
    ///*******/
    switch (CC_CurrentDirection){
      case Anticlockwise:{
          
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          } 
          println("Anticlockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX < width/2){
            CurrentState=StateSet.StateTrialCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
        
      }break;
      
      case Clockwise:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
             text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
          //text("+-->",width/2,height/2-diameter/2-circularWidth-20,80,50); 
          println("Clockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX > width/2){
            CurrentState=StateSet.StateTrialCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
      }break;
      default:{
                  println("None valide direction CC"); 
      }break;
    }
}

void FuncStateTrialCC_BeginSteer(){
    bEnableCursorTrajectory=true;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    if (UsedMousePressed == true){
       if(!(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth))){
         bCircularExceed=true;
         CurrentState=StateSet.StateTrialCC_Failed;
       }
       else{
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                if((UsedMouseX>=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             case Clockwise:{
                if((UsedMouseX<=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             
             default:{
                  println("None valide direction CC"); 
             }break;
           }
         } 
         
         if(bPassSemiCircle){
            println("PassSemiCircle");
            if(UsedMouseY<height/2-Y_OffsetForComfort){
              bThreeQuartersCircle=true;
            }
         }
         
         if(bThreeQuartersCircle){
           println("ThreeQuartersCircle");
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                 if((UsedMouseX<=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrialCC_Succeeded;
                       println("Succeeded steering");
                       bSucessCircularSteering= true;
                       //CurrentState=StateSet.StateTrialCC_Succeeded;
                        
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        EndTime=timestamp.getTime();
                        /*log*/
                        CurrentRow.setString("EndTime", Long.toString(EndTime));
                        CurrentRow.setString("State", "Successful");
                        CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
                        CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
                        CurrentRow.setString("State","Successful");
                        
                        bRecordTrajectory=false;
                     }
             }break;
             case Clockwise:{
                   //&&(UsedMouseY<(height/2-diameter/2-circularWidth/2))
                   if((UsedMouseX>=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrialCC_Succeeded;
                       
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        EndTime=timestamp.getTime();
                        /*log*/
                        CurrentRow.setString("EndTime", Long.toString(EndTime));
                        CurrentRow.setString("State", "Successful");
                        CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
                        CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
                        CurrentRow.setString("State","Successful");
                       println("Succeeded steering");
                       bRecordTrajectory=false;
                     }
             }break;
             default:{
                        println("None valide direction C"); 
             }break;
           }
         }
       }
       

    if ((!UsedMousePressed) &&(!bSucessCircularSteering)){
          bCircularEarlyRelease=true;
          CurrentState=StateSet.StateTrialCC_Failed;
          
          //println("Succeeded steering");
          //println("EarlyRelease");
    }
}

void FuncStateTrialCC_Succeeded(){
    NumTotalCC++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorAcquired);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    println("Succeeded steering");
    CurrentState=StateSet.StateSaveCSV;
    OldState=StateSet.StateTrialCC_Succeeded;
    //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
        
    //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    //EndTime=timestamp.getTime();
    //CurrentRow.setString("EndTime", Long.toString(EndTime));
    //CurrentRow.setString("State", "Successful");
    //CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    //CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
    CC_CheckTakSucceeded();
}

void FuncStateTrialCC_Failed(){
    NumTotalCC++;
    NumErrorCC++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorMissed);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    if(bCircularExceed){
      println("Exceed Path");
      /*log*/
      CurrentRow.setString("ErrorType","Exceed Path");
    }
    if(bCircularEarlyRelease){
      println("Early Release");
      /*log*/
      CurrentRow.setString("ErrorType","Early Release");
    }
    
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    EndTime=timestamp.getTime();
    /*log*/
    CurrentRow.setString("EndTime", Long.toString(EndTime));
    CurrentRow.setString("State", "Successful");
    CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
    CurrentRow.setString("State","Failed");
          
    bRecordTrajectory=false;
    
    //CurrentState=StateSet.StateTrialCC_Wait;
    CurrentState=StateSet.StateSaveCSV;
    OldState=StateSet.StateTrialCC_Failed;
    //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
}

void FuncJudgeSpeedWarningCC(){
    if(NumTotalCC%6==0){
      ErrorRateCC= (float)NumErrorCC/ (float)NumTotalCC;
      if(ErrorRateCC>UpperLimitErrorRateCC){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=1;
      }
      else if(ErrorRateCC<LowerLimitErrorRateCC){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=2;
      }
      else{
        CurrentState=StateSet.StateTrialCC_Wait;
        bNeedSpeedUp=0;
       }
    }

}
