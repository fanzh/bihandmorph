import java.util.*;

enum OrderSet{
  TCL,
  TLC,
  CTL,
  CLT,
  LTC,
  LCT
}

float Pre_StartButtonCenterX;
float Pre_StartButtonCenterY;
float Pre_StartButtonWidth;
float Pre_StartButtonHeight;
boolean bPassToStateUpdateExperimentsPara_xml=false;

XML ExperimentsParaXML;
//save(Environment.getExternalStorageDirectory()+"/testImag.jpg");
File ExternalStorage=Environment.getExternalStorageDirectory();

void initStateStart(){
  /*StateTrainingCC concerned variables*/
  Pre_StartButtonCenterX=width/2-200;
  Pre_StartButtonCenterY=height/2-Y_OffsetForComfort;
  Pre_StartButtonWidth=250;
  Pre_StartButtonHeight=100;
  bPassToStateUpdateExperimentsPara_xml=false;
}

void FuncStateStart(){
      background(colorBackground);
      fill(255);
      println("Enter StateStart");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      text("BiHandMorph", width/2,height/2,600,370.8); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(Pre_StartButtonCenterX,Pre_StartButtonCenterY,Pre_StartButtonWidth,Pre_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToStateUpdateExperimentsPara_xml){
        CurrentState=StateSet.StateUpdateExperimentsPara_xml;
      }
}
/*
Order="TCL"
*/
final int TA_TrainningNum=14;
final int CC_TrainningNum=5;
final int CL_TrainningNum=5;

final int TA_TrialNum=10;
final int CC_TrialNum=5;
final int CL_TrialNum=5;

final int Num_Participants=12;

void FuncStateUpdateExperimentsPara_xml(){
      background(colorBackground);
      fill(255);
      File ExperimentsParaXML = new File(ExternalStorage+"/ExperimentsPara.xml");
      if(ExperimentsParaXML.exists()){
        println(" ExperimentsParaXML already  exist");
      }
      else{
        println("Need to creat ExperimentsPara.xml");
        XML ExperimentsPara_xml=new XML("Record");
        boolean bBeginingHand=true;
        int ExperimentOrder=0;
        for(int i=0;i<Num_Participants;i++){
          XML Experimentation = ExperimentsPara_xml.addChild("Experimentation");
          Experimentation.setInt("id", i);
          //String BeginingHand=bBeginingHand?"right":"left";
          Experimentation.setString("BeginingHand",bBeginingHand?"right":"left");
          //println(bBeginingHand);
          switch(ExperimentOrder%6){
            case 0: Experimentation.setString("Order","TCL");break;
            case 1: Experimentation.setString("Order","TLC");break;
            case 2: Experimentation.setString("Order","CLT");break;
            case 3: Experimentation.setString("Order","LCT");break;
            case 4: Experimentation.setString("Order","LTC");break;
            case 5: Experimentation.setString("Order","LCT");break;
          }
          XML Training=Experimentation.addChild("Training");
          
          XML TA=Training.addChild("TA");
          for(int j=0;j<TA_TrainningNum;j++){
           //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
           //long randSeed=timestamp.getTime();
           //randomSeed(randSeed);
           
            XML Para=TA.addChild("Para");
            Para.setInt("RepeatNum",2);
             switch(ceil(random(2))){
               case 1: Para.setString("Direction","Horizontal");break;
               case 2: Para.setString("Direction","Vertical");break;
             }
             Para.setInt("RepeatNum",2);
             switch(ceil(random(3))){
                 case 1: Para.setInt("Amplitude",144);break;
                 case 2: Para.setInt("Amplitude",288);break;
                 case 3: Para.setInt("Amplitude",576);break;
             }
             switch(ceil(random(3))){
                 case 1: Para.setInt("Width",24);break;
                 case 2: Para.setInt("Width",48);break;
                 case 3: Para.setInt("Width",96);break;
             }
             Para.setContent("TA"+Integer.toString(j) );
          }
          
          XML CC =Training.addChild("CC");
          Set<Integer> CC_TrainingParaSet = new HashSet<Integer>();
          while(CC_TrainingParaSet.size()<CC_TrainningNum*4){
              CC_TrainingParaSet.add(floor(random(CC_TrainningNum*4)));
          }
          Iterator<Integer> it_CC_TrainingParaSet=CC_TrainingParaSet.iterator();
          int CC_TrainingCounter=0;
          while(it_CC_TrainingParaSet.hasNext()){
            int CC_TrainingParaSetNo=it_CC_TrainingParaSet.next();
              switch(CC_TrainingParaSetNo%4){
                case 0:{
                        XML Para=CC.addChild("Para");
                        Para.setInt("RepeatNum",1);
                        Para.setString("Direction","Clockwise");
                        Para.setInt("Length",237);
                        Para.setInt("Width",80);
                        Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                case 1:{
                          XML Para=CC.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Clockwise");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                case 2:{
                        XML Para=CC.addChild("Para");
                        Para.setInt("RepeatNum",1);
                        Para.setString("Direction","Anticlockwise");
                        Para.setInt("Length",237);
                        Para.setInt("Width",80);
                        Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                
                case 3:{
                          XML Para=CC.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Anticlockwise");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
              }
              CC_TrainingCounter++;
          }
          CC_TrainingParaSet.clear();
          
          XML CL =Training.addChild("CL");
          Set<Integer> CL_TrainingParaSet = new HashSet<Integer>();
          while(CL_TrainingParaSet.size()<CL_TrainningNum*8){
              CL_TrainingParaSet.add(floor(random(CL_TrainningNum*8)));
          }
          Iterator<Integer> it_CL_TrainingParaSet=CL_TrainingParaSet.iterator();
          int CL_TrainingCounter=0;
          while(it_CL_TrainingParaSet.hasNext()){
            int CL_TrainingParaSetNo=it_CL_TrainingParaSet.next();
              switch(CL_TrainingParaSetNo%8){
                case 0:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Leftward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 1:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Leftward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 2:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Rightward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 3:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 4:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 5:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 6:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Downward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 7:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Downward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
              }
              CL_TrainingCounter++;
          }          
          CL_TrainingParaSet.clear();
          

          /*trial set*/
          XML Trial=Experimentation.addChild("Trial");
          TA=Trial.addChild("TA");
          Set<Integer> TA_ParaSet = new HashSet<Integer>();
          while(TA_ParaSet.size()<18){
            TA_ParaSet.add(ceil(random(2))*100+ceil(random(3))*10+ceil(random(3)));
          }
          
          int tempElementCounter=0;
          Iterator<Integer> it_totalTA_Para=TA_ParaSet.iterator();  
           while(it_totalTA_Para.hasNext()){  
             XML Para=TA.addChild("Para");
             Para.setInt("RepeatNum",10);
             Para.setContent("TA"+Integer.toString(tempElementCounter) );
             tempElementCounter++;
             int totalTA_Para=it_totalTA_Para.next();
             //println("totalTA_Para" + totalTA_Para);
             switch(totalTA_Para/100){
                case 1: Para.setString("Direction","Horizontal");break;
                case 2: Para.setString("Direction","Vertical");break;
              } 
              switch((totalTA_Para/10)%10){
                case 1: Para.setInt("Amplitude",144);break;
                case 2: Para.setInt("Amplitude",288);break;
                case 3: Para.setInt("Amplitude",576);break;
              }
              
              switch(totalTA_Para%10){
                case 1: Para.setInt("Width",24);break;
                case 2: Para.setInt("Width",48);break;
                case 3: Para.setInt("Width",96);break;
              }
           }  
           println("TA_ParaSet:" + TA_ParaSet);
           TA_ParaSet.clear();
           
          CC=Trial.addChild("CC");
          tempElementCounter=0;
          for(int tempCC_TrialNum=0;tempCC_TrialNum<CC_TrialNum;tempCC_TrialNum++){
            Set<Integer> CC_ParaSet = new HashSet<Integer>();
            while(CC_ParaSet.size()<12){
              CC_ParaSet.add(ceil(random(2))*100+ceil(random(2))*10+ceil(random(3)));
            }
            
            //for (int inerloopCC_Trial=0;inerloopCC_Trial<2;inerloopCC_Trial++){
               Iterator<Integer> it_totalCC_Para=CC_ParaSet.iterator();  
               while(it_totalCC_Para.hasNext()){  
                 XML Para=CC.addChild("Para");
                 Para.setInt("RepeatNum",1);
                 Para.setContent("CC"+Integer.toString(tempElementCounter) );
                 tempElementCounter++;
                 int totalCC_Para=it_totalCC_Para.next();
                 //println("totalTA_Para" + totalTA_Para);
                 switch(totalCC_Para/100){
                    case 1: Para.setString("Direction","Clockwise");break;
                    case 2: Para.setString("Direction","Anticlockwise");break;
                  } 
                  switch((totalCC_Para/10)%10){
                    case 1: Para.setInt("Length",237);break;
                    case 2: Para.setInt("Length",474);break;
                  }
                  
                  switch(totalCC_Para%10){
                    case 1: Para.setInt("Width",40);break;
                    case 2: Para.setInt("Width",54);break;
                    case 3: Para.setInt("Width",80);break;
                  }
               }  
               println("CC_ParaSet:" + CC_ParaSet);
            //}
             CC_ParaSet.clear();
          }
      
          CL=Trial.addChild("CL");
          tempElementCounter=0;
          for(int tempCL_TrialNum=0;tempCL_TrialNum<CL_TrialNum;tempCL_TrialNum++){
            Set<Integer> CL_ParaSet = new HashSet<Integer>();
            while(CL_ParaSet.size()<24){
              CL_ParaSet.add(ceil(random(4))*100+ceil(random(2))*10+ceil(random(3)));
            }
            
             //for (int inerloopCL_Trial=0;inerloopCL_Trial<2;inerloopCL_Trial++){
               Iterator<Integer> it_totalCL_Para=CL_ParaSet.iterator();  
               while(it_totalCL_Para.hasNext()){  
                 XML Para=CL.addChild("Para");
                 Para.setInt("RepeatNum",1);
                 Para.setContent("CL"+Integer.toString(tempElementCounter) );
                 tempElementCounter++;
                 int totalCL_Para=it_totalCL_Para.next();
                 //println("totalTA_Para" + totalTA_Para);
                 switch(totalCL_Para/100){
                    case 1: Para.setString("Direction","Leftward");break;
                    case 2: Para.setString("Direction","Rightward");break;
                    case 3: Para.setString("Direction","Upward");break;
                    case 4: Para.setString("Direction","Downward");break;
                  } 
                  switch((totalCL_Para/10)%10){
                    case 1: Para.setInt("Length",237);break;
                    case 2: Para.setInt("Length",474);break;
                  }
                  
                  switch(totalCL_Para%10){
                    case 1: Para.setInt("Width",40);break;
                    case 2: Para.setInt("Width",54);break;
                    case 3: Para.setInt("Width",80);break;
                  }
               }  
               println("CL_ParaSet:" + CL_ParaSet);
             //}
             CL_ParaSet.clear();
          }
          
          /**/
          ExperimentOrder++;
          bBeginingHand=!bBeginingHand;
        }
        saveXML(ExperimentsPara_xml, ExternalStorage + "/ExperimentsPara.xml");
        println("Saved.");
      }
      CurrentState=StateSet.StateUpdateExperimentRecord_xml;
}


//int verifyconter=0;
int validExpNum=0;
int PassedExp=0;
void Func_StateUpdateExperimentRecord_xml(){
  background(colorBackground);
  fill(255);
  println("StateUpdateExperimentRecord_xml");
  File ExperimentRecordXML = new File(ExternalStorage+"/ExperimentRecord.xml");
  if(ExperimentRecordXML.exists()){
        println("ExperimentRecordXML Already  exist");
        XML ExperimentRecord_xml=loadXML(ExternalStorage+"/ExperimentRecord.xml");
        XML GlobalRecord=ExperimentRecord_xml.getChild("GlobalRecord");
        validExpNum=GlobalRecord.getInt("ValidExp");
        /*Here need better integrated*/
          validExpNum=NumParticipantFromDropDownList;
        /**/
        PassedExp=GlobalRecord.getInt("PassedExp");
        GlobalRecord.setInt("PassedExp",PassedExp+1);
        XML EachRecord=ExperimentRecord_xml.getChild("EachRecord");
        XML Record=EachRecord.addChild("Record");
        Record.setContent("Record"+Integer.toString(PassedExp+1));
        Record.setString("State","Unkown");
        saveXML(ExperimentRecord_xml, ExternalStorage + "/ExperimentRecord.xml");
  }
  else{
      println("Need to creat ExperimentsPara.xml");
      XML ExperimentRecord_xml=new XML("Record");
      XML GlobalRecord=ExperimentRecord_xml.addChild("GlobalRecord");
      GlobalRecord.setContent("GlobalVariable");
      GlobalRecord.setInt("PassedExp",0);
      GlobalRecord.setInt("ValidExp",0);
      GlobalRecord.setInt("NonValidExp",0);
      
      XML EachRecord=ExperimentRecord_xml.addChild("EachRecord");
      XML Record=EachRecord.addChild("Record");
      Record.setContent("Record0");
      Record.setString("State","Unkown");
      saveXML(ExperimentRecord_xml, ExternalStorage + "/ExperimentRecord.xml");
  }
  CurrentState=StateSet.StateCreatRecord_csv;
}

String CurrentTableName;

//
void FuncStateCreatRecord_csv(){
  background(colorBackground);
  fill(255);
  println("StateCreatRecord_csv");
  Table table=new Table();
  table.addColumn("id");
  table.addColumn("Participant");
  table.addColumn("State");
  table.addColumn("TaskType");
  table.addColumn("HandUsed");
  table.addColumn("Direction");
  table.addColumn("SubDirection");
  table.addColumn("Amplitude/Length");
  table.addColumn("Width");
  table.addColumn("StartTime");
  table.addColumn("StartPosition");
  table.addColumn("EndTime");
  table.addColumn("Duration");
  table.addColumn("ErrorType");
  //table.addColumn("ErrorTime");
  table.addColumn("EndPosition");
  //table.addColumn("Trajectory");
  table.addColumn("x");
  table.addColumn("y");
  //table.addColumn("Accelerometer");
  table.addColumn("ax");
  table.addColumn("ay");
  table.addColumn("az");
  //table.addColumn("Gyroscope");
  table.addColumn("wx");
  table.addColumn("wy");
  table.addColumn("wz");
  table.addColumn("Remark");
  //CurrentTable=table;
  
  String TableName="/Record"+Integer.toString(PassedExp)+".csv";
  saveTable(table, Environment.getExternalStorageDirectory()+TableName);
  CurrentState=StateSet.StateIniParameters;
  CurrentRecordCSV = new File(ExternalStorage+TableName);
  CurrentTableName=TableName;
  //CurrentTable=table;
  if(CurrentRecordCSV.exists()){
    println("RecordCSV opened successfully");
    CurrentTable = loadTable(ExternalStorage+TableName, "header");
  }
  else{
    println("RecordCSV opened failed");
  }
  //CurrentTableName=TableName;
}

Vector<TargetAcquisitionPara> TrainTA_Vector=new Vector<TargetAcquisitionPara>();
Vector<TargetAcquisitionPara> TrialTA_Vector=new Vector<TargetAcquisitionPara>();
Vector<ConstrainedLinearMotionPara> TrainCL_Vector=new Vector<ConstrainedLinearMotionPara>();
Vector<ConstrainedLinearMotionPara> TrialCL_Vector=new Vector<ConstrainedLinearMotionPara>();
Vector<ConstrainedCircularMotionPara> TrainCC_Vector=new Vector<ConstrainedCircularMotionPara>();
Vector<ConstrainedCircularMotionPara> TrialCC_Vector=new Vector<ConstrainedCircularMotionPara>();
String CurrentBeginingHand;
String Order;
String CurrentOrder;
char[] TaskArray;
void FuncStateIniParameters(){
  background(colorBackground);
  fill(255);
  println("StateIniParameters");
  XML ExperimentsPara_xml= loadXML(ExternalStorage + "/ExperimentsPara.xml");
  XML[] Experiments = ExperimentsPara_xml.getChildren("Experimentation");
  XML CurrentExperiment=Experiments[validExpNum];
  CurrentBeginingHand=CurrentExperiment.getString("BeginingHand");
  Order=CurrentExperiment.getString("Order");
  TaskArray = Order.toCharArray();
  XML Training=CurrentExperiment.getChild("Training");
  XML TrainingTA=Training.getChild("TA");
  XML[] TrainingTAParas=TrainingTA.getChildren("Para");
  for (int i = 0; i < TrainingTAParas.length; i++) {
    TargetAcquisitionPara TempTrainTAPara=new TargetAcquisitionPara();
    TempTrainTAPara.RepeatNum=TrainingTAParas[i].getInt("RepeatNum");
    switch(TrainingTAParas[i].getInt("Amplitude")){
      case 144: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 288: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 576: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(TrainingTAParas[i].getInt("Width")){
      case 24: TempTrainTAPara.width=TA_Width.Pixel_24;break;
      case 48: TempTrainTAPara.width=TA_Width.Pixel_48;break;
      case 96: TempTrainTAPara.width=TA_Width.Pixel_96;break;
    }
    switch(TrainingTAParas[i].getString("Direction")){
      case "Horizontal" : TempTrainTAPara.direction=TA_Direction.Horizontal;break;
      case "Vertical" : TempTrainTAPara.direction=TA_Direction.Vertical;break;
    } 
    TrainTA_Vector.add(TempTrainTAPara);
  }
  println("StateIniParameters: TrainTA_Vector");
  
  XML TrainingCC=Training.getChild("CC");
  XML[] TrainingCCParas=TrainingCC.getChildren("Para");
  for (int i = 0; i < TrainingCCParas.length; i++) {
    ConstrainedCircularMotionPara TempTrainCCPara=new ConstrainedCircularMotionPara();
    TempTrainCCPara.RepeatNum=TrainingCCParas[i].getInt("RepeatNum");
    switch(TrainingCCParas[i].getInt("Length")){
      case 237: TempTrainCCPara.length=CC_Length.Pixel_237;break;
      case 474: TempTrainCCPara.length=CC_Length.Pixel_474;break;
    }
    switch(TrainingCCParas[i].getInt("Width")){
      case 40: TempTrainCCPara.width=CC_Width.Pixel_40;break;
      case 54: TempTrainCCPara.width=CC_Width.Pixel_54;break;
      case 80: TempTrainCCPara.width=CC_Width.Pixel_80;break;
    }
    switch(TrainingCCParas[i].getString("Direction")){
      case "Clockwise" : TempTrainCCPara.direction=CC_Direction.Clockwise;break;
      case "Anticlockwise" : TempTrainCCPara.direction=CC_Direction.Anticlockwise;break;
    } 
    TrainCC_Vector.add(TempTrainCCPara);
  }
  println("StateIniParameters: TrainCC_Vector");
  
  XML TrainingCL=Training.getChild("CL");
  XML[] TrainingCLParas=TrainingCL.getChildren("Para");
  for (int i = 0; i < TrainingCLParas.length; i++) {
    ConstrainedLinearMotionPara TempTrainCLPara=new ConstrainedLinearMotionPara();
    TempTrainCLPara.RepeatNum=TrainingCLParas[i].getInt("RepeatNum");
    switch(TrainingCLParas[i].getInt("Length")){
      case 237: TempTrainCLPara.length=CL_Length.Pixel_237;break;
      case 474: TempTrainCLPara.length=CL_Length.Pixel_474;break;
    }
    switch(TrainingCLParas[i].getInt("Width")){
      case 40: TempTrainCLPara.width=CL_Width.Pixel_40;break;
      case 54: TempTrainCLPara.width=CL_Width.Pixel_54;break;
      case 80: TempTrainCLPara.width=CL_Width.Pixel_80;break;
    }
    switch(TrainingCLParas[i].getString("Direction")){
      case "Leftward" : TempTrainCLPara.direction=CL_Direction.Leftward;break;
      case "Rightward" : TempTrainCLPara.direction=CL_Direction.Rightward;break;
      case "Upward" : TempTrainCLPara.direction=CL_Direction.Upward;break;
      case "Downward" : TempTrainCLPara.direction=CL_Direction.Downward;break;
    } 
    TrainCL_Vector.add(TempTrainCLPara);
  }
  println("StateIniParameters: TrainCL_Vector");
  
  XML Trial=CurrentExperiment.getChild("Trial");
  XML TrialTA=Trial.getChild("TA");
  XML[] TrialTAParas=TrialTA.getChildren("Para");
  for (int i = 0; i < TrialTAParas.length; i++) {
    //println("TrialTAParas.length + ", TrialTAParas.length);
    TargetAcquisitionPara TempTrialTAPara=new TargetAcquisitionPara();
    TempTrialTAPara.RepeatNum=TrialTAParas[i].getInt("RepeatNum");
    switch(TrialTAParas[i].getInt("Amplitude")){
      case 144: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 288: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 576: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(TrialTAParas[i].getInt("Width")){
      case 24: TempTrialTAPara.width=TA_Width.Pixel_24;break;
      case 48: TempTrialTAPara.width=TA_Width.Pixel_48;break;
      case 96: TempTrialTAPara.width=TA_Width.Pixel_96;break;
    }
    switch(TrialTAParas[i].getString("Direction")){
      case "Horizontal" : TempTrialTAPara.direction=TA_Direction.Horizontal;break;
      case "Vertical" : TempTrialTAPara.direction=TA_Direction.Vertical;break;
    } 
    TrialTA_Vector.add(TempTrialTAPara);
  }
  println("StateIniParameters: TrialTA_Vector");
  XML TrialCC=Trial.getChild("CC");
  XML[] TrialCCParas=TrialCC.getChildren("Para");
  for (int i = 0; i < TrialCCParas.length; i++) {
    ConstrainedCircularMotionPara TempTrialCCPara=new ConstrainedCircularMotionPara();
    TempTrialCCPara.RepeatNum=TrialCCParas[i].getInt("RepeatNum");
    switch(TrialCCParas[i].getInt("Length")){
      case 237: TempTrialCCPara.length=CC_Length.Pixel_237;break;
      case 474: TempTrialCCPara.length=CC_Length.Pixel_474;break;
    }
    switch(TrialCCParas[i].getInt("Width")){
      case 40: TempTrialCCPara.width=CC_Width.Pixel_40;break;
      case 54: TempTrialCCPara.width=CC_Width.Pixel_54;break;
      case 80: TempTrialCCPara.width=CC_Width.Pixel_80;break;
    }
    switch(TrialCCParas[i].getString("Direction")){
      case "Clockwise" : TempTrialCCPara.direction=CC_Direction.Clockwise;break;
      case "Anticlockwise" : TempTrialCCPara.direction=CC_Direction.Anticlockwise;break;
    } 
    TrialCC_Vector.add(TempTrialCCPara);
  }
  println("StateIniParameters: TrialCC_Vector");
  XML TrialCL=Trial.getChild("CL");
  XML[] TrialCLParas=TrialCL.getChildren("Para");
  for (int i = 0; i < TrialCLParas.length; i++) {
    ConstrainedLinearMotionPara TempTrialCLPara=new ConstrainedLinearMotionPara();
    TempTrialCLPara.RepeatNum=TrialCLParas[i].getInt("RepeatNum");
    switch(TrialCLParas[i].getInt("Length")){
      case 237: TempTrialCLPara.length=CL_Length.Pixel_237;break;
      case 474: TempTrialCLPara.length=CL_Length.Pixel_474;break;
    }
    switch(TrialCLParas[i].getInt("Width")){
      case 40: TempTrialCLPara.width=CL_Width.Pixel_40;break;
      case 54: TempTrialCLPara.width=CL_Width.Pixel_54;break;
      case 80: TempTrialCLPara.width=CL_Width.Pixel_80;break;
    }
    switch(TrialCLParas[i].getString("Direction")){
      case "Leftward" : TempTrialCLPara.direction=CL_Direction.Leftward;break;
      case "Rightward" : TempTrialCLPara.direction=CL_Direction.Rightward;break;
      case "Upward" : TempTrialCLPara.direction=CL_Direction.Upward;break;
      case "Downward" : TempTrialCLPara.direction=CL_Direction.Downward;break;
    } 
    TrialCL_Vector.add(TempTrialCLPara);
  }
  println("StateIniParameters: TrialCL_Vector");
  
  CurrentState=StateSet.StateDecideTask;
  //saveXML(ExperimentsPara_xml, ExternalStorage + "/ExperimentsPara.xml");
  //TargetAcquisitionPara;
  //ConstrainedLinearMotionPara;
  //ConstrainedCircularMotionPara;
}

enum TaskType{
  TrainTA,
  TrainCC,
  TrainCL,
  TrialTA,
  TrialCC,
  TrialCL
}

TaskType CurrentTask;
int currentTaskCounter=0;
//int currentHandCounter=0;
String CurrentHandUsed;
/* F*:FirstHand S*:SecondHand  *F:FirstForm *S:SecondForm  *T:ThirdForm **T:TrainingSet **A:TrialSet
0 1 2 3 : FFT FFA SFT SFA
4 5 6 7 : FST FSA SST SSA
8 9 10 11: FTT FTA STT STA 
*/
void FuncStateDecideTask(){
  background(colorBackground);
  fill(255);
  println("StateDecideTask");
  if(currentTaskCounter>=12){
    CurrentState=StateSet.StateFishedXP;
  }
  else{
        switch ((currentTaskCounter/2)%2){
          case 0: {
            CurrentHandUsed=CurrentBeginingHand;
          }break;
          case 1:{
                switch(CurrentBeginingHand){
                  case "right":{CurrentHandUsed="left";}break;
                  case "left":{CurrentHandUsed="right";}break;
                  default:{println("Invalid hand used");}break;
                }
          }break;
       }
            
      switch (TaskArray[currentTaskCounter/4]){ 
        case 'T':{//TARGET AQUISITION
          //CursorSize=8;
          switch(currentTaskCounter%2){
            case 0:{
              //Iterator itr = TrainTA_Vector.iterator(); 
              for(int i=0;i<TrainTA_Vector.size();i++){
                //TargetAcquisitionPara temppara=TrainTA_Vector[i];
                TTAParavector.TargetAcquisitionParaVector.add(TrainTA_Vector.elementAt(i));
              }
              //TTAParavector.TargetAcquisitionParaVector=TrainTA_Vector;
              CurrentState=StateSet.StateTrainingTA;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialTA_Vector.size();i++){
                //TargetAcquisitionPara temppara=TrainTA_Vector[i];
                ATAParavector.TargetAcquisitionParaVector.add(TrialTA_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialTA;
            }break;
        }
          
        }break;
        
        case 'C':{//CONTRAINT CIRCULAR 
        //CursorSize=10;
          switch(currentTaskCounter%2){
            case 0:{
              for(int i=0;i<TrainCC_Vector.size();i++){
                TCCParavector.ConstrainedCircularMotionParaVector.add(TrainCC_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrainingCC;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialCC_Vector.size();i++){
                ACCParavector.ConstrainedCircularMotionParaVector.add(TrialCC_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialCC;
            }break;
        }
        
        } break;
        
        case 'L': {//CONTRAINT LINEAR 
        //CursorSize=10;
          switch(currentTaskCounter%2){
            case 0:{
              for(int i=0;i<TrainCL_Vector.size();i++){
                TCLParavector.ConstrainedLinearMotionParaVector.add(TrainCL_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrainingCL;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialCL_Vector.size();i++){
                ACLParavector.ConstrainedLinearMotionParaVector.add(TrialCL_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialCL;
            }break;
          }
        }break;
        
        default:{
          println("non valid task form"); 
        }break;
      }
    
      currentTaskCounter++;
  }

  
}

void FuncStateSaveCSV(){
  String TrajectoryRecordx= new String();
  String TrajectoryRecordy= new String();
  String AccelerometerRecordx= new String();
  String AccelerometerRecordy= new String();
  String AccelerometerRecordz= new String();
  String GyroscopeRecordx= new String();
  String GyroscopeRecordy= new String();
  String GyroscopeRecordz= new String();
  
  if(bEnableRecordTrajectory){
      while(TrajectoryVector.size()>0){
        PVector TempTrajectoryPoint=TrajectoryVector.get(0);
        TrajectoryRecordx=TrajectoryRecordx+Float.toString(TempTrajectoryPoint.x)+",";
        TrajectoryRecordy=TrajectoryRecordy+Float.toString(TempTrajectoryPoint.y)+",";
        //TrajectoryRecord=TrajectoryRecord+"("+Float.toString(TempTrajectoryPoint.x)+","+Float.toString(TempTrajectoryPoint.y)+"),";
        TrajectoryVector.remove(0);
      }
    
      CurrentRow.setString("x",TrajectoryRecordx);
      CurrentRow.setString("y",TrajectoryRecordy);
      
      while(AccelerometerVector.size()>0){
        PVector TempAccelerometer=AccelerometerVector.get(0);
        AccelerometerRecordx=AccelerometerRecordx+Float.toString(TempAccelerometer.x)+",";
        AccelerometerRecordy=AccelerometerRecordy+Float.toString(TempAccelerometer.y)+",";
        AccelerometerRecordz=AccelerometerRecordz+Float.toString(TempAccelerometer.z)+",";
        //AccelerometerRecord=AccelerometerRecord+"("+Float.toString(TempAccelerometer.x)+","+Float.toString(TempAccelerometer.y)+","+Float.toString(TempAccelerometer.z)+"),";
        AccelerometerVector.remove(0);
      }
    
      CurrentRow.setString("ax",AccelerometerRecordx);
      CurrentRow.setString("ay",AccelerometerRecordy);
      CurrentRow.setString("az",AccelerometerRecordz);
      
      while(GyroscopeVector.size()>0){
        PVector TempGyroscope=GyroscopeVector.get(0);
        GyroscopeRecordx=GyroscopeRecordx+Float.toString(TempGyroscope.x)+",";
        GyroscopeRecordy=GyroscopeRecordy+Float.toString(TempGyroscope.y)+",";
        GyroscopeRecordz=GyroscopeRecordz+Float.toString(TempGyroscope.z)+",";
        //GyroscopeRecord=GyroscopeRecord+"("+Float.toString(TempGyroscope.x)+","+Float.toString(TempGyroscope.y)+","+Float.toString(TempGyroscope.z)+"),";
        GyroscopeVector.remove(0);
      }
    
      CurrentRow.setString("wx",GyroscopeRecordx);   
      CurrentRow.setString("wy",GyroscopeRecordy);  
      CurrentRow.setString("wz",GyroscopeRecordz);  
      bRecordTrajectory=false;    
    }
 
CurrentRow.setInt("Participant",NumParticipantFromDropDownList);
saveTable(CurrentTable, ExternalStorage+CurrentTableName);

  switch(OldState){
    case StateTrialTA_Missed:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialTA_Acquired:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialCC_Succeeded:{
          CurrentState=StateSet.StateTrialCC_Wait;
          FuncJudgeSpeedWarningCC();
        }break;
    case StateTrialCC_Failed:{
          CurrentState=StateSet.StateTrialCC_Wait;
          FuncJudgeSpeedWarningCC();
        }break;
    case StateTrialCL_Succeeded:{
          CurrentState=StateSet.StateTrialCL_Wait;
          FuncJudgeSpeedWarningCL();
        }break;
    case StateTrialCL_Failed:{
          CurrentState=StateSet.StateTrialCL_Wait;
          FuncJudgeSpeedWarningCL();
          }break;
    
    default:{
      println("ERRO STATE IN StateSaveCSV");
    }break;
  }
}

void FuncStateFishedXP(){
        background(colorBackground);
      fill(255);
      println("Enter StateTrainingCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      text(" Thanks a lot for your participation", width/2,height/2,600,370.8); 
}

ControlP5 cp5;
DropdownList dNoParticipant,dNoXpPhase;
Button buttonStartXP;
int NumParticipantFromDropDownList=0;
int NumPaseFromDropDownList=0;
void InitStatePreStartVar(){
  cp5 = new ControlP5(this);
  cp5.setFont(new ControlFont(createFont("CenturyGothic", 30), 30));
  
  dNoParticipant = cp5.addDropdownList("Participant")
          .setPosition(100, 200)
          .setSize(250,800)
          .setScrollSensitivity(0.5)
          ;
  dNoParticipant.setBackgroundColor(color(190));
  dNoParticipant.setItemHeight(60);
  dNoParticipant.setBarHeight(80);
  dNoParticipant.setColorBackground(color(60));
  dNoParticipant.setColorActive(color(255, 128));
  dNoParticipant.getCaptionLabel().set("Participant");
  
          
  for (int i=0;i<Num_Participants;i++) {
    dNoParticipant.addItem("Participant  "+i, i);
  }
  
  dNoXpPhase = cp5.addDropdownList("Phase")
          .setPosition(500, 200)
          .setSize(250,800)
          .setScrollSensitivity(0.5)
          ;
  dNoXpPhase.setBackgroundColor(color(190));
  dNoXpPhase.setItemHeight(60);
  dNoXpPhase.setBarHeight(80);
  dNoXpPhase.setColorBackground(color(60));
  dNoXpPhase.setColorActive(color(255, 128));
  dNoXpPhase.getCaptionLabel().set("Phase");
          
  for (int i=0;i<12;i++) {
    dNoXpPhase.addItem("Phase  "+i, i);
  }
  
  buttonStartXP=cp5.addButton("StartXP")
     .setValue(0)
     .setPosition(width/2-100,1200)
     .setSize(200,200)
     .addCallback(new CallbackListener() {
      public void controlEvent(CallbackEvent event) {
        if (event.getAction() == ControlP5.ACTION_PRESS) {
          println("button PRESSED.");
          bStatePreStartFinished=true;
        }
      }
    })
    ;
          
}

boolean bStatePreStartFinished=false;
void FuncStatePreStart(){
  background(colorBackground);
  if(bStatePreStartFinished){
    bEnableCursor=true;
    CurrentState=StateSet.StateStart;
    dNoXpPhase.remove();
    dNoParticipant.remove();
    buttonStartXP.remove();
    currentTaskCounter=NumPaseFromDropDownList;
  }
}

public void StartXP(int theValue) {
  println("a button event from StartXP: "+theValue);
}
