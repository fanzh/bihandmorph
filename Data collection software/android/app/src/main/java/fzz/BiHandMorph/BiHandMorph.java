package fzz.BiHandMorph;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import android.view.MotionEvent; 
import android.app.ActivityManager; 
import android.annotation.SuppressLint; 
import java.io.IOError; 
import java.sql.Timestamp; 
import android.os.Environment; 
import controlP5.*; 
import java.util.*; 
import java.util.*; 
import java.util.*; 
import android.content.Context; 
import android.hardware.Sensor; 
import android.hardware.SensorManager; 
import android.hardware.SensorEvent; 
import android.hardware.SensorEventListener; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class BiHandMorph extends PApplet {











//import java.util.concurrent.*;

final float Y_OffsetForComfort=32.5f*16;
boolean UsedMousePressed=false;
int UsedMouseX=0;
int UsedMouseY=0;
int pUsedMouseX=0;
int pUsedMouseY=0;
int tempUsedMouseX=0;
int tempUsedMouseY=0;
int oldMouseX=0; 
int oldMouseY=0; 
int oldCorlor;
int colorCursor= color(255,0,0);
int colorCursorTrajectory=color(255,0,255);

int colorInactivated=color(200,200,200);
int colorActivated=color(29,107,0);
int colorAcquired=color(212,199,17);
int colorMissed=color(178,0,25);
int colorBackground=color(226,226,226);
int colorStartLine=color(0);
int widthStartLine=5;
boolean bEnableCursorTrajectory=false;
boolean bEnableCursor=false;

int CursorSize=10;
int colorText=color(0, 102, 153);
int holdImagTime=0;

final boolean bEnableMirrorImage=true;
boolean bEnableRecordTrajectory=true;
boolean bRecordTrajectory=false;
Vector<PVector> TrajectoryVector=new Vector<PVector>();
Vector<PVector> AccelerometerVector=new Vector<PVector>();
Vector<PVector> GyroscopeVector=new Vector<PVector>();

/*log concerned variables*/
File CurrentRecordCSV;
Table CurrentTable;
TableRow CurrentRow;
int CurrentRowId=0;
long StartTime=0;
long EndTime=0;

final int ImagHoldingTime=50;
//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//TimestampBefore=timestamp.getTime();

//int DisableEdgeWidth=140;
public void setup() {
  //size(800,1920);
  
  background(colorBackground);
  InitStatePreStartVar();
  initStateStart();
  initStateTTA();
  initStateTCC();
  initStateTCL();
  initStateATA();
  initStateACC();
  initStateACL();
  initIMU();
}

boolean bNeedSpeedWarning=false;
int bNeedSpeedUp=0;// 2 means need to speed up, 1 means need to slow down, 0 means noting to do
public void draw() {  
  //println("UsedMousePressed : ",UsedMousePressed);
  if(tempUsedMouseX!=UsedMouseX||tempUsedMouseY!=UsedMouseY){
    pUsedMouseX=tempUsedMouseX;
    pUsedMouseY=tempUsedMouseY;
    tempUsedMouseX=UsedMouseX;
    tempUsedMouseY=UsedMouseY;
  }
  
  //TA_Patch();
    
  if (bEnableCursor){
    fill(oldCorlor);
    noStroke();
    if(bEnableMirrorImage){
      ellipse((width-oldMouseX),oldMouseY,CursorSize,CursorSize);
    }
    else{
      ellipse(oldMouseX,oldMouseY,CursorSize,CursorSize); 
    }
  }
  delay(holdImagTime);
  holdImagTime=0;
  
  if(bRecordTrajectory){
    PVector TempTrajectoryPoint = new PVector(UsedMouseX,UsedMouseY);
    TrajectoryVector.add(TempTrajectoryPoint);
    PVector TempAccelerometerVector = new PVector(ax,ay,az);
    AccelerometerVector.add(TempAccelerometerVector);
    PVector TempGyroscopeVector = new PVector(wx,wy,wz);
    GyroscopeVector.add(TempGyroscopeVector);
  }

  
  switch(CurrentState){
    /*PreProcedure*/
    case StatePreStart:{
      FuncStatePreStart();
    }break;
    case StateStart:{
      FuncStateStart();
    }break;
    
    case StateUpdateExperimentsPara_xml:{
      FuncStateUpdateExperimentsPara_xml();
    }break;
    
    case StateUpdateExperimentRecord_xml:{
      Func_StateUpdateExperimentRecord_xml();
    }break;
    
    case StateCreatRecord_csv:{
      FuncStateCreatRecord_csv();    
    }break;

    case StateIniParameters:{
      FuncStateIniParameters();
    }break;
    
    case StateDecideTask:{
      FuncStateDecideTask();
    }break;
    
    case StateWarnSpeed:{
      FuncStateWarnSpeed();
    }break;
    /*TrainingTA*/
    case StateTrainingTA: {
        FuncStateTrainingTA();
    }break;
    
    case StateTrainingTA_Init: {
      FuncStateTrainingTA_Init();
    }break;
    
    case StateTrainingTA_Decide:{
      FuncStateTrainingTA_Decide();
    }break;
    
    case StateTrainingTA_Wait:{
      FuncStateTrainingTA_Wait();
    }break;
    
    case StateSubTTAinit:{
      FuncStateSubTTAinit();
    }break;
    
    case StateSubTTApressed:{
      FuncStateSubTTApressed();
    }break;
    
    case StateSubTTAjudge:{
      FuncStateSubTTAjudge();
    }break;
    
    case StateTrainingTA_Acquired:{
      FuncStateTrainingTA_Acquired();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingTA_Missed:{
      FuncStateTrainingTA_Missed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    
    /*TrialTA*/
    case StateTrialTA:{
        FuncStateTrialTA();
    }break;
    case StateTrialTA_Init:{
      FuncStateTrialTA_Init();
    }break;
    case StateTrialTA_Decide:{
      FuncStateTrialTA_Decide();
    }break;
    case StateTrialTA_Wait:{
      FuncStateTrialTA_Wait();
    }break;
    case StateSubATAinit:{
      FuncStateSubATAinit();
    }break;
    case StateSubATApressed:{
      FuncStateSubATApressed();
    }break;
    case StateSubATAjudge:{
      FuncStateSubATAjudge();
    }break;
    case StateTrialTA_Acquired:{
      FuncStateTrialTA_Acquired();
    }break;
    case StateTrialTA_Missed:{
      FuncStateTrialTA_Missed();
    }break;
    
    
    /*TrainingCC*/
    case StateTrainingCC :{
      FuncStateTrainingCC();
    }break;
    
    case StateTrainingCC_Decide:{
      FuncStateTrainingCC_Decide();
    }break;
    
    case StateTrainingCC_Wait:{
      FuncStateTrainingCC_Wait();
    }break;
    
    case StateTrainingCC_EnterStartArc:{
      FuncStateTrainingCC_EnterStartArc();
    }break;
    
    case StateTrainingCC_BeginSteer:{
      FuncStateTrainingCC_BeginSteer();
    }break;
    
    case StateTrainingCC_Failed:{
      Func_StateTrainingCC_Failed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingCC_Succeeded:{
      Func_StateTrainingCC_Succeeded();
      holdImagTime=ImagHoldingTime;
    }break;  
    
    /*Trial CC*/
    case StateTrialCC:{
      FuncStateTrialCC();
    }break;
    case StateTrialCC_Decide:{
      FuncStateTrialCC_Decide();
    }break;
    case StateTrialCC_Wait:{
      FuncStateTrialCC_Wait();
    }break;
    case StateTrialCC_EnterStartArc:{
      FuncStateTrialCC_EnterStartArc();
    }break;
    case StateTrialCC_BeginSteer:{
      FuncStateTrialCC_BeginSteer();
    }break;
    case   StateTrialCC_Succeeded:{
      FuncStateTrialCC_Succeeded();
    }break;
    case StateTrialCC_Failed:{
      FuncStateTrialCC_Failed();
    }break;
    /*TrainingCL*/
     case StateTrainingCL:{
       FuncStateTrainingCL();
     }break; 
    case StateTrainingCL_Decide:{
      FuncStateTrainingCL_Decide();
    }break;
    
    case StateTrainingCL_Wait:{
      FuncStateTrainingCL_Wait();
    }break;
    
    case StateTrainingCC_EnterStartBar:{
      FuncStateTrainingCC_EnterStartBar();
    }break;
    
    case StateTrainingCL_BeginSteer:{
      FuncStateTrainingCL_BeginSteer();
    }break;
    
    case StateTrainingCL_Succeeded:{
      FuncStateTrainingCL_Succeeded();
      holdImagTime=ImagHoldingTime;
    }break;
    
    case StateTrainingCL_Failed:{
      FuncStateTrainingCL_Failed();
      holdImagTime=ImagHoldingTime;
    }break;
    
    /*Trial CL*/
      case StateTrialCL:{
        FuncStateTrialCL();
      }break;
    case StateTrialCL_Decide:{
      FuncStateTrialCL_Decide();
    }break;
    case StateTrialCL_Wait:{
      FuncStateTrialCL_Wait();
    }break;
    case StateTrialCC_EnterStartBar:{
      FuncStateTrialCC_EnterStartBar();
    }break;
    case StateTrialCL_BeginSteer:{
      FuncStateTrialCL_BeginSteer();
    }break;
    case StateTrialCL_Succeeded:{
      FuncStateTrialCL_Succeeded();
    }break;
    case StateTrialCL_Failed:{
      FuncStateTrialCL_Failed();
    }break;
    
    case StateSaveCSV:{
      FuncStateSaveCSV();
      holdImagTime=ImagHoldingTime;
    }break;
    case StateFishedXP:{
      FuncStateFishedXP();
    }break;
    
    default:
    // Default executes if the case labels
    println("None");   // don't match the switch parameter
    break;
    
    /*case StateTrainingTA_Init: {
      clear();
    }break;*/
  }
  
  if (bEnableCursorTrajectory){
      stroke(colorCursorTrajectory);
      if (UsedMousePressed == true) {
      strokeWeight(CursorSize); 
      strokeCap(ROUND);
      if(bEnableMirrorImage){
        line((width-UsedMouseX), UsedMouseY, (width-pUsedMouseX), pUsedMouseY);
      }
      else{
        line(UsedMouseX, UsedMouseY, pUsedMouseX, pUsedMouseY);      
      }
    }
  }
  
  if (bEnableCursor){
    if (UsedMousePressed == true) {
      oldMouseX=UsedMouseX;
      oldMouseY=UsedMouseY;
      oldCorlor=get(oldMouseX,oldMouseY);
      noStroke();
      fill(colorCursor);
      if(bEnableMirrorImage){
        ellipse((width-UsedMouseX),UsedMouseY,CursorSize,CursorSize);
      }
      else{
        ellipse(UsedMouseX,UsedMouseY,CursorSize,CursorSize); 
      }
    }
  }

}



@SuppressLint("MissingSuperCall")
@Override
public void onBackPressed()
{

   // super.onBackPressed(); // Comment this super call to avoid calling finish() or fragmentmanager's backstack pop operation.
}



int StayInOnePosition=0;

public void mousePressed(){

  switch(CurrentState){
    case StateSubTTAinit:{
      CurrentState=StateSet.StateSubTTApressed;
    }break;
    case StateStart:{
        if(RectButtonClicked(Pre_StartButtonCenterX,Pre_StartButtonCenterY,Pre_StartButtonWidth,Pre_StartButtonHeight)){
        bPassToStateUpdateExperimentsPara_xml=true;
      }
    }break;
    case StateTrainingTA: {
        if(RectButtonClicked(TTA_StartButtonCenterX,TTA_StartButtonCenterY,TTA_StartButtonWidth,TTA_StartButtonHeight)){
        bPassToTTA_Init=true;
      }
    }break;
    case StateTrialTA:{
      if(RectButtonClicked(ATA_StartButtonCenterX,ATA_StartButtonCenterY,ATA_StartButtonWidth,ATA_StartButtonHeight)){
        bPassToATA_Init=true;
      }
    }break;
    case StateTrainingCC:{
        if(RectButtonClicked(TCC_StartButtonCenterX,TCC_StartButtonCenterY,TCC_StartButtonWidth,TCC_StartButtonHeight)){
        bPassToTCC_Init=true;
      }
    } break;
    case StateTrialCC:{
        if(RectButtonClicked(ACC_StartButtonCenterX,ACC_StartButtonCenterY,ACC_StartButtonWidth,ACC_StartButtonHeight)){
        bPassToACC_Init=true;
      }
    } break;
  
    case StateTrainingCL:{
        if(RectButtonClicked(TCL_StartButtonCenterX,TCL_StartButtonCenterY,TCL_StartButtonWidth,TCL_StartButtonHeight)){
        bPassToTCL_Init=true;
      }
    } break;
    case StateTrialCL:{
        if(RectButtonClicked(ACL_StartButtonCenterX,ACL_StartButtonCenterY,ACL_StartButtonWidth,ACL_StartButtonHeight)){
        bPassToACL_Init=true;
      }
    } break;
    default:break;
  }  
  //File ExternalStorage=Environment.getExternalStorageDirectory();
  //save(ExternalStorage+"/testImag.jpg");
  
 
}
  
 


public boolean RectButtonClicked(float x0, float y0, float ButtonWidth, float ButtonHeight ){
  if(bEnableMirrorImage){
        if (UsedMouseX <= (width-x0+ButtonWidth/2.0f) && UsedMouseX >= (width-x0-ButtonWidth/2.0f) && 
          UsedMouseY >= (y0-ButtonHeight/2.0f) && UsedMouseY <= (y0+ButtonHeight/2.0f)) {
        return true;
      } else {
        return false;
      } 
  }
  else{
    if (UsedMouseX >= (x0-ButtonWidth/2.0f) && UsedMouseX <= (x0+ButtonWidth/2.0f) && 
        UsedMouseY >= (y0-ButtonHeight/2.0f) && UsedMouseY <= (y0+ButtonHeight/2.0f)) {
      return true;
    } else {
      return false;
    }  
  }

}

public boolean RectButtonReleased(float x0, float y0, float ButtonWidth, float ButtonHeight,int ReleasedX, int ReleasedY ){
  if (ReleasedX >= (x0-ButtonWidth/2.0f) && ReleasedX <= (x0+ButtonWidth/2.0f) && 
      ReleasedY >= (y0-ButtonHeight/2.0f) && ReleasedY <= (y0+ButtonHeight/2.0f)) {
    return true;
  } else {
    return false;
  }
}

public void FuncStateWarnSpeed(){
  println("enter EchoSpeedWarningInformation");
      background(colorBackground);
      fill(255);
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      switch(bNeedSpeedUp){
        case 2: text("Please try to speed up", width/2,height/2-Y_OffsetForComfort,600,370.8f);holdImagTime=2000;break;
        case 1: text("Please try to slow down", width/2,height/2-Y_OffsetForComfort,600,370.8f);holdImagTime=2000;break;
        case 0: break;
        default: break;
      }
       
    switch(OldState){
    case StateTrialTA_Missed:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialTA_Acquired:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialCC_Succeeded:{CurrentState=StateSet.StateTrialCC_Wait;}break;
    case StateTrialCC_Failed:{CurrentState=StateSet.StateTrialCC_Wait;}break;
    case StateTrialCL_Succeeded:{CurrentState=StateSet.StateTrialCL_Wait;}break;
    case StateTrialCL_Failed:{CurrentState=StateSet.StateTrialCL_Wait;}break;
    
    default:{
      println("ERRO STATE IN StateWarnSpeed");
    }break;
  }
  
}

public void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.

  if (theEvent.isGroup()) {
    // check if the Event was triggered from a ControlGroup
    println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
  } 
  else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
    
    if(theEvent.getController()==dNoParticipant){
      NumParticipantFromDropDownList=(int) theEvent.getController().getValue();
    }
    else if (theEvent.getController()==dNoXpPhase){
      NumPaseFromDropDownList=(int) theEvent.getController().getValue();
    }
  }
}


final float xLeft=140;
final float xRight=940;
final float yTop=10;
final float ybottom=71*16;

//void initScreenBoard(){
//    xLeft=140;
//    xRight=940;
//    yTop=0;
//    ybottom=71*16;
//}
public boolean surfaceTouchEvent(MotionEvent event) {
    //int numPointers = event.getPointerCount();
    ////UsedMousePressed= false;
    //for(int i=0; i < numPointers; i++) {
    //  float x = event.getX(i);
    //  //float y = event.getY(i);
    //  if(x>xLeft && x<xRight){
        
    //    UsedMousePressed=true;
    //    //UsedMouseX=(int)x;
    //    //UsedMouseY=(int)y;
    //    break;
    //  }
    //}
    
    //if(event.getActionMasked() == MotionEvent.ACTION_UP){
    //    UsedMousePressed=false;
    //}  
    //println(event);
    int numPointers = event.getPointerCount();
    
    switch(event.getActionMasked()){
       case MotionEvent.ACTION_UP:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y=event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=false;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
          TA_Patch();
        }
      mousePressed=false;
      }break;
      case MotionEvent.ACTION_POINTER_UP:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y=event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=false;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
          TA_Patch();
        }
      mousePressed=false;
      }break;
      case MotionEvent.ACTION_DOWN:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y = event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=true;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;
        }
        mousePressed=true;
      }break;      
      case MotionEvent.ACTION_POINTER_DOWN:{
        int pointIndex=event.getActionIndex();
        float x = event.getX(pointIndex);
        float y = event.getY(pointIndex);
        if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
          UsedMousePressed=true;
          UsedMouseX=(int)x;
          UsedMouseY=(int)y;       
        }
          mousePressed=true;           
      }break;
      case MotionEvent.ACTION_MOVE:{
        for(int i=0; i < numPointers; i++){
          float x = event.getX(i);
          float y = event.getY(i);        
          if(x>xLeft && x<xRight&&y<ybottom&&y>yTop){
            UsedMousePressed=true;
            UsedMouseX=(int)x;
            UsedMouseY=(int)y;
            break;
          }
        }
        mousePressed=true;
      }break;
      default:break;
    }
    //if(event.getActionMasked()==MotionEvent.ACTION_POINTER_UP){
    //  int pointIndex=event.getActionIndex();
    //  float x = event.getX(pointIndex);
    //  if(x>xLeft && x<xRight){
    //    UsedMousePressed=false;
    //  }
    //}
    return super.surfaceTouchEvent(event);
}

//void drawCursor(){
//  float x, float y;
  
//}


enum OrderSet{
  TCL,
  TLC,
  CTL,
  CLT,
  LTC,
  LCT
}

float Pre_StartButtonCenterX;
float Pre_StartButtonCenterY;
float Pre_StartButtonWidth;
float Pre_StartButtonHeight;
boolean bPassToStateUpdateExperimentsPara_xml=false;

XML ExperimentsParaXML;
//save(Environment.getExternalStorageDirectory()+"/testImag.jpg");
File ExternalStorage=Environment.getExternalStorageDirectory();

public void initStateStart(){
  /*StateTrainingCC concerned variables*/
  Pre_StartButtonCenterX=width/2-200;
  Pre_StartButtonCenterY=height/2-Y_OffsetForComfort;
  Pre_StartButtonWidth=250;
  Pre_StartButtonHeight=100;
  bPassToStateUpdateExperimentsPara_xml=false;
}

public void FuncStateStart(){
      background(colorBackground);
      fill(255);
      println("Enter StateStart");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      text("BiHandMorph", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(Pre_StartButtonCenterX,Pre_StartButtonCenterY,Pre_StartButtonWidth,Pre_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToStateUpdateExperimentsPara_xml){
        CurrentState=StateSet.StateUpdateExperimentsPara_xml;
      }
}
/*
Order="TCL"
*/
final int TA_TrainningNum=14;
final int CC_TrainningNum=5;
final int CL_TrainningNum=5;

final int TA_TrialNum=10;
final int CC_TrialNum=5;
final int CL_TrialNum=5;

final int Num_Participants=12;

public void FuncStateUpdateExperimentsPara_xml(){
      background(colorBackground);
      fill(255);
      File ExperimentsParaXML = new File(ExternalStorage+"/ExperimentsPara.xml");
      if(ExperimentsParaXML.exists()){
        println(" ExperimentsParaXML already  exist");
      }
      else{
        println("Need to creat ExperimentsPara.xml");
        XML ExperimentsPara_xml=new XML("Record");
        boolean bBeginingHand=true;
        int ExperimentOrder=0;
        for(int i=0;i<Num_Participants;i++){
          XML Experimentation = ExperimentsPara_xml.addChild("Experimentation");
          Experimentation.setInt("id", i);
          //String BeginingHand=bBeginingHand?"right":"left";
          Experimentation.setString("BeginingHand",bBeginingHand?"right":"left");
          //println(bBeginingHand);
          switch(ExperimentOrder%6){
            case 0: Experimentation.setString("Order","TCL");break;
            case 1: Experimentation.setString("Order","TLC");break;
            case 2: Experimentation.setString("Order","CLT");break;
            case 3: Experimentation.setString("Order","LCT");break;
            case 4: Experimentation.setString("Order","LTC");break;
            case 5: Experimentation.setString("Order","LCT");break;
          }
          XML Training=Experimentation.addChild("Training");
          
          XML TA=Training.addChild("TA");
          for(int j=0;j<TA_TrainningNum;j++){
           //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
           //long randSeed=timestamp.getTime();
           //randomSeed(randSeed);
           
            XML Para=TA.addChild("Para");
            Para.setInt("RepeatNum",2);
             switch(ceil(random(2))){
               case 1: Para.setString("Direction","Horizontal");break;
               case 2: Para.setString("Direction","Vertical");break;
             }
             Para.setInt("RepeatNum",2);
             switch(ceil(random(3))){
                 case 1: Para.setInt("Amplitude",144);break;
                 case 2: Para.setInt("Amplitude",288);break;
                 case 3: Para.setInt("Amplitude",576);break;
             }
             switch(ceil(random(3))){
                 case 1: Para.setInt("Width",24);break;
                 case 2: Para.setInt("Width",48);break;
                 case 3: Para.setInt("Width",96);break;
             }
             Para.setContent("TA"+Integer.toString(j) );
          }
          
          XML CC =Training.addChild("CC");
          Set<Integer> CC_TrainingParaSet = new HashSet<Integer>();
          while(CC_TrainingParaSet.size()<CC_TrainningNum*4){
              CC_TrainingParaSet.add(floor(random(CC_TrainningNum*4)));
          }
          Iterator<Integer> it_CC_TrainingParaSet=CC_TrainingParaSet.iterator();
          int CC_TrainingCounter=0;
          while(it_CC_TrainingParaSet.hasNext()){
            int CC_TrainingParaSetNo=it_CC_TrainingParaSet.next();
              switch(CC_TrainingParaSetNo%4){
                case 0:{
                        XML Para=CC.addChild("Para");
                        Para.setInt("RepeatNum",1);
                        Para.setString("Direction","Clockwise");
                        Para.setInt("Length",237);
                        Para.setInt("Width",80);
                        Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                case 1:{
                          XML Para=CC.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Clockwise");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                case 2:{
                        XML Para=CC.addChild("Para");
                        Para.setInt("RepeatNum",1);
                        Para.setString("Direction","Anticlockwise");
                        Para.setInt("Length",237);
                        Para.setInt("Width",80);
                        Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
                
                case 3:{
                          XML Para=CC.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Anticlockwise");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CC"+Integer.toString(CC_TrainingCounter));
                }break;
              }
              CC_TrainingCounter++;
          }
          CC_TrainingParaSet.clear();
          
          XML CL =Training.addChild("CL");
          Set<Integer> CL_TrainingParaSet = new HashSet<Integer>();
          while(CL_TrainingParaSet.size()<CL_TrainningNum*8){
              CL_TrainingParaSet.add(floor(random(CL_TrainningNum*8)));
          }
          Iterator<Integer> it_CL_TrainingParaSet=CL_TrainingParaSet.iterator();
          int CL_TrainingCounter=0;
          while(it_CL_TrainingParaSet.hasNext()){
            int CL_TrainingParaSetNo=it_CL_TrainingParaSet.next();
              switch(CL_TrainingParaSetNo%8){
                case 0:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Leftward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 1:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Leftward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 2:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Rightward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 3:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 4:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 5:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Upward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                case 6:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Downward");
                          Para.setInt("Length",237);
                          Para.setInt("Width",80);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
                
                case 7:{
                          XML Para=CL.addChild("Para");
                          Para.setInt("RepeatNum",1);
                          Para.setString("Direction","Downward");
                          Para.setInt("Length",474);
                          Para.setInt("Width",40);
                          Para.setContent("CL"+Integer.toString(CL_TrainingCounter));
                }break;
              }
              CL_TrainingCounter++;
          }          
          CL_TrainingParaSet.clear();
          

          /*trial set*/
          XML Trial=Experimentation.addChild("Trial");
          TA=Trial.addChild("TA");
          Set<Integer> TA_ParaSet = new HashSet<Integer>();
          while(TA_ParaSet.size()<18){
            TA_ParaSet.add(ceil(random(2))*100+ceil(random(3))*10+ceil(random(3)));
          }
          
          int tempElementCounter=0;
          Iterator<Integer> it_totalTA_Para=TA_ParaSet.iterator();  
           while(it_totalTA_Para.hasNext()){  
             XML Para=TA.addChild("Para");
             Para.setInt("RepeatNum",10);
             Para.setContent("TA"+Integer.toString(tempElementCounter) );
             tempElementCounter++;
             int totalTA_Para=it_totalTA_Para.next();
             //println("totalTA_Para" + totalTA_Para);
             switch(totalTA_Para/100){
                case 1: Para.setString("Direction","Horizontal");break;
                case 2: Para.setString("Direction","Vertical");break;
              } 
              switch((totalTA_Para/10)%10){
                case 1: Para.setInt("Amplitude",144);break;
                case 2: Para.setInt("Amplitude",288);break;
                case 3: Para.setInt("Amplitude",576);break;
              }
              
              switch(totalTA_Para%10){
                case 1: Para.setInt("Width",24);break;
                case 2: Para.setInt("Width",48);break;
                case 3: Para.setInt("Width",96);break;
              }
           }  
           println("TA_ParaSet:" + TA_ParaSet);
           TA_ParaSet.clear();
           
          CC=Trial.addChild("CC");
          tempElementCounter=0;
          for(int tempCC_TrialNum=0;tempCC_TrialNum<CC_TrialNum;tempCC_TrialNum++){
            Set<Integer> CC_ParaSet = new HashSet<Integer>();
            while(CC_ParaSet.size()<12){
              CC_ParaSet.add(ceil(random(2))*100+ceil(random(2))*10+ceil(random(3)));
            }
            
            //for (int inerloopCC_Trial=0;inerloopCC_Trial<2;inerloopCC_Trial++){
               Iterator<Integer> it_totalCC_Para=CC_ParaSet.iterator();  
               while(it_totalCC_Para.hasNext()){  
                 XML Para=CC.addChild("Para");
                 Para.setInt("RepeatNum",1);
                 Para.setContent("CC"+Integer.toString(tempElementCounter) );
                 tempElementCounter++;
                 int totalCC_Para=it_totalCC_Para.next();
                 //println("totalTA_Para" + totalTA_Para);
                 switch(totalCC_Para/100){
                    case 1: Para.setString("Direction","Clockwise");break;
                    case 2: Para.setString("Direction","Anticlockwise");break;
                  } 
                  switch((totalCC_Para/10)%10){
                    case 1: Para.setInt("Length",237);break;
                    case 2: Para.setInt("Length",474);break;
                  }
                  
                  switch(totalCC_Para%10){
                    case 1: Para.setInt("Width",40);break;
                    case 2: Para.setInt("Width",54);break;
                    case 3: Para.setInt("Width",80);break;
                  }
               }  
               println("CC_ParaSet:" + CC_ParaSet);
            //}
             CC_ParaSet.clear();
          }
      
          CL=Trial.addChild("CL");
          tempElementCounter=0;
          for(int tempCL_TrialNum=0;tempCL_TrialNum<CL_TrialNum;tempCL_TrialNum++){
            Set<Integer> CL_ParaSet = new HashSet<Integer>();
            while(CL_ParaSet.size()<24){
              CL_ParaSet.add(ceil(random(4))*100+ceil(random(2))*10+ceil(random(3)));
            }
            
             //for (int inerloopCL_Trial=0;inerloopCL_Trial<2;inerloopCL_Trial++){
               Iterator<Integer> it_totalCL_Para=CL_ParaSet.iterator();  
               while(it_totalCL_Para.hasNext()){  
                 XML Para=CL.addChild("Para");
                 Para.setInt("RepeatNum",1);
                 Para.setContent("CL"+Integer.toString(tempElementCounter) );
                 tempElementCounter++;
                 int totalCL_Para=it_totalCL_Para.next();
                 //println("totalTA_Para" + totalTA_Para);
                 switch(totalCL_Para/100){
                    case 1: Para.setString("Direction","Leftward");break;
                    case 2: Para.setString("Direction","Rightward");break;
                    case 3: Para.setString("Direction","Upward");break;
                    case 4: Para.setString("Direction","Downward");break;
                  } 
                  switch((totalCL_Para/10)%10){
                    case 1: Para.setInt("Length",237);break;
                    case 2: Para.setInt("Length",474);break;
                  }
                  
                  switch(totalCL_Para%10){
                    case 1: Para.setInt("Width",40);break;
                    case 2: Para.setInt("Width",54);break;
                    case 3: Para.setInt("Width",80);break;
                  }
               }  
               println("CL_ParaSet:" + CL_ParaSet);
             //}
             CL_ParaSet.clear();
          }
          
          /**/
          ExperimentOrder++;
          bBeginingHand=!bBeginingHand;
        }
        saveXML(ExperimentsPara_xml, ExternalStorage + "/ExperimentsPara.xml");
        println("Saved.");
      }
      CurrentState=StateSet.StateUpdateExperimentRecord_xml;
}


//int verifyconter=0;
int validExpNum=0;
int PassedExp=0;
public void Func_StateUpdateExperimentRecord_xml(){
  background(colorBackground);
  fill(255);
  println("StateUpdateExperimentRecord_xml");
  File ExperimentRecordXML = new File(ExternalStorage+"/ExperimentRecord.xml");
  if(ExperimentRecordXML.exists()){
        println("ExperimentRecordXML Already  exist");
        XML ExperimentRecord_xml=loadXML(ExternalStorage+"/ExperimentRecord.xml");
        XML GlobalRecord=ExperimentRecord_xml.getChild("GlobalRecord");
        validExpNum=GlobalRecord.getInt("ValidExp");
        /*Here need better integrated*/
          validExpNum=NumParticipantFromDropDownList;
        /**/
        PassedExp=GlobalRecord.getInt("PassedExp");
        GlobalRecord.setInt("PassedExp",PassedExp+1);
        XML EachRecord=ExperimentRecord_xml.getChild("EachRecord");
        XML Record=EachRecord.addChild("Record");
        Record.setContent("Record"+Integer.toString(PassedExp+1));
        Record.setString("State","Unkown");
        saveXML(ExperimentRecord_xml, ExternalStorage + "/ExperimentRecord.xml");
  }
  else{
      println("Need to creat ExperimentsPara.xml");
      XML ExperimentRecord_xml=new XML("Record");
      XML GlobalRecord=ExperimentRecord_xml.addChild("GlobalRecord");
      GlobalRecord.setContent("GlobalVariable");
      GlobalRecord.setInt("PassedExp",0);
      GlobalRecord.setInt("ValidExp",0);
      GlobalRecord.setInt("NonValidExp",0);
      
      XML EachRecord=ExperimentRecord_xml.addChild("EachRecord");
      XML Record=EachRecord.addChild("Record");
      Record.setContent("Record0");
      Record.setString("State","Unkown");
      saveXML(ExperimentRecord_xml, ExternalStorage + "/ExperimentRecord.xml");
  }
  CurrentState=StateSet.StateCreatRecord_csv;
}

String CurrentTableName;

//
public void FuncStateCreatRecord_csv(){
  background(colorBackground);
  fill(255);
  println("StateCreatRecord_csv");
  Table table=new Table();
  table.addColumn("id");
  table.addColumn("Participant");
  table.addColumn("State");
  table.addColumn("TaskType");
  table.addColumn("HandUsed");
  table.addColumn("Direction");
  table.addColumn("SubDirection");
  table.addColumn("Amplitude/Length");
  table.addColumn("Width");
  table.addColumn("StartTime");
  table.addColumn("StartPosition");
  table.addColumn("EndTime");
  table.addColumn("Duration");
  table.addColumn("ErrorType");
  //table.addColumn("ErrorTime");
  table.addColumn("EndPosition");
  //table.addColumn("Trajectory");
  table.addColumn("x");
  table.addColumn("y");
  //table.addColumn("Accelerometer");
  table.addColumn("ax");
  table.addColumn("ay");
  table.addColumn("az");
  //table.addColumn("Gyroscope");
  table.addColumn("wx");
  table.addColumn("wy");
  table.addColumn("wz");
  table.addColumn("Remark");
  //CurrentTable=table;
  
  String TableName="/Record"+Integer.toString(PassedExp)+".csv";
  saveTable(table, Environment.getExternalStorageDirectory()+TableName);
  CurrentState=StateSet.StateIniParameters;
  CurrentRecordCSV = new File(ExternalStorage+TableName);
  CurrentTableName=TableName;
  //CurrentTable=table;
  if(CurrentRecordCSV.exists()){
    println("RecordCSV opened successfully");
    CurrentTable = loadTable(ExternalStorage+TableName, "header");
  }
  else{
    println("RecordCSV opened failed");
  }
  //CurrentTableName=TableName;
}

Vector<TargetAcquisitionPara> TrainTA_Vector=new Vector<TargetAcquisitionPara>();
Vector<TargetAcquisitionPara> TrialTA_Vector=new Vector<TargetAcquisitionPara>();
Vector<ConstrainedLinearMotionPara> TrainCL_Vector=new Vector<ConstrainedLinearMotionPara>();
Vector<ConstrainedLinearMotionPara> TrialCL_Vector=new Vector<ConstrainedLinearMotionPara>();
Vector<ConstrainedCircularMotionPara> TrainCC_Vector=new Vector<ConstrainedCircularMotionPara>();
Vector<ConstrainedCircularMotionPara> TrialCC_Vector=new Vector<ConstrainedCircularMotionPara>();
String CurrentBeginingHand;
String Order;
String CurrentOrder;
char[] TaskArray;
public void FuncStateIniParameters(){
  background(colorBackground);
  fill(255);
  println("StateIniParameters");
  XML ExperimentsPara_xml= loadXML(ExternalStorage + "/ExperimentsPara.xml");
  XML[] Experiments = ExperimentsPara_xml.getChildren("Experimentation");
  XML CurrentExperiment=Experiments[validExpNum];
  CurrentBeginingHand=CurrentExperiment.getString("BeginingHand");
  Order=CurrentExperiment.getString("Order");
  TaskArray = Order.toCharArray();
  XML Training=CurrentExperiment.getChild("Training");
  XML TrainingTA=Training.getChild("TA");
  XML[] TrainingTAParas=TrainingTA.getChildren("Para");
  for (int i = 0; i < TrainingTAParas.length; i++) {
    TargetAcquisitionPara TempTrainTAPara=new TargetAcquisitionPara();
    TempTrainTAPara.RepeatNum=TrainingTAParas[i].getInt("RepeatNum");
    switch(TrainingTAParas[i].getInt("Amplitude")){
      case 144: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 288: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 576: TempTrainTAPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(TrainingTAParas[i].getInt("Width")){
      case 24: TempTrainTAPara.width=TA_Width.Pixel_24;break;
      case 48: TempTrainTAPara.width=TA_Width.Pixel_48;break;
      case 96: TempTrainTAPara.width=TA_Width.Pixel_96;break;
    }
    switch(TrainingTAParas[i].getString("Direction")){
      case "Horizontal" : TempTrainTAPara.direction=TA_Direction.Horizontal;break;
      case "Vertical" : TempTrainTAPara.direction=TA_Direction.Vertical;break;
    } 
    TrainTA_Vector.add(TempTrainTAPara);
  }
  println("StateIniParameters: TrainTA_Vector");
  
  XML TrainingCC=Training.getChild("CC");
  XML[] TrainingCCParas=TrainingCC.getChildren("Para");
  for (int i = 0; i < TrainingCCParas.length; i++) {
    ConstrainedCircularMotionPara TempTrainCCPara=new ConstrainedCircularMotionPara();
    TempTrainCCPara.RepeatNum=TrainingCCParas[i].getInt("RepeatNum");
    switch(TrainingCCParas[i].getInt("Length")){
      case 237: TempTrainCCPara.length=CC_Length.Pixel_237;break;
      case 474: TempTrainCCPara.length=CC_Length.Pixel_474;break;
    }
    switch(TrainingCCParas[i].getInt("Width")){
      case 40: TempTrainCCPara.width=CC_Width.Pixel_40;break;
      case 54: TempTrainCCPara.width=CC_Width.Pixel_54;break;
      case 80: TempTrainCCPara.width=CC_Width.Pixel_80;break;
    }
    switch(TrainingCCParas[i].getString("Direction")){
      case "Clockwise" : TempTrainCCPara.direction=CC_Direction.Clockwise;break;
      case "Anticlockwise" : TempTrainCCPara.direction=CC_Direction.Anticlockwise;break;
    } 
    TrainCC_Vector.add(TempTrainCCPara);
  }
  println("StateIniParameters: TrainCC_Vector");
  
  XML TrainingCL=Training.getChild("CL");
  XML[] TrainingCLParas=TrainingCL.getChildren("Para");
  for (int i = 0; i < TrainingCLParas.length; i++) {
    ConstrainedLinearMotionPara TempTrainCLPara=new ConstrainedLinearMotionPara();
    TempTrainCLPara.RepeatNum=TrainingCLParas[i].getInt("RepeatNum");
    switch(TrainingCLParas[i].getInt("Length")){
      case 237: TempTrainCLPara.length=CL_Length.Pixel_237;break;
      case 474: TempTrainCLPara.length=CL_Length.Pixel_474;break;
    }
    switch(TrainingCLParas[i].getInt("Width")){
      case 40: TempTrainCLPara.width=CL_Width.Pixel_40;break;
      case 54: TempTrainCLPara.width=CL_Width.Pixel_54;break;
      case 80: TempTrainCLPara.width=CL_Width.Pixel_80;break;
    }
    switch(TrainingCLParas[i].getString("Direction")){
      case "Leftward" : TempTrainCLPara.direction=CL_Direction.Leftward;break;
      case "Rightward" : TempTrainCLPara.direction=CL_Direction.Rightward;break;
      case "Upward" : TempTrainCLPara.direction=CL_Direction.Upward;break;
      case "Downward" : TempTrainCLPara.direction=CL_Direction.Downward;break;
    } 
    TrainCL_Vector.add(TempTrainCLPara);
  }
  println("StateIniParameters: TrainCL_Vector");
  
  XML Trial=CurrentExperiment.getChild("Trial");
  XML TrialTA=Trial.getChild("TA");
  XML[] TrialTAParas=TrialTA.getChildren("Para");
  for (int i = 0; i < TrialTAParas.length; i++) {
    //println("TrialTAParas.length + ", TrialTAParas.length);
    TargetAcquisitionPara TempTrialTAPara=new TargetAcquisitionPara();
    TempTrialTAPara.RepeatNum=TrialTAParas[i].getInt("RepeatNum");
    switch(TrialTAParas[i].getInt("Amplitude")){
      case 144: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 288: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 576: TempTrialTAPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(TrialTAParas[i].getInt("Width")){
      case 24: TempTrialTAPara.width=TA_Width.Pixel_24;break;
      case 48: TempTrialTAPara.width=TA_Width.Pixel_48;break;
      case 96: TempTrialTAPara.width=TA_Width.Pixel_96;break;
    }
    switch(TrialTAParas[i].getString("Direction")){
      case "Horizontal" : TempTrialTAPara.direction=TA_Direction.Horizontal;break;
      case "Vertical" : TempTrialTAPara.direction=TA_Direction.Vertical;break;
    } 
    TrialTA_Vector.add(TempTrialTAPara);
  }
  println("StateIniParameters: TrialTA_Vector");
  XML TrialCC=Trial.getChild("CC");
  XML[] TrialCCParas=TrialCC.getChildren("Para");
  for (int i = 0; i < TrialCCParas.length; i++) {
    ConstrainedCircularMotionPara TempTrialCCPara=new ConstrainedCircularMotionPara();
    TempTrialCCPara.RepeatNum=TrialCCParas[i].getInt("RepeatNum");
    switch(TrialCCParas[i].getInt("Length")){
      case 237: TempTrialCCPara.length=CC_Length.Pixel_237;break;
      case 474: TempTrialCCPara.length=CC_Length.Pixel_474;break;
    }
    switch(TrialCCParas[i].getInt("Width")){
      case 40: TempTrialCCPara.width=CC_Width.Pixel_40;break;
      case 54: TempTrialCCPara.width=CC_Width.Pixel_54;break;
      case 80: TempTrialCCPara.width=CC_Width.Pixel_80;break;
    }
    switch(TrialCCParas[i].getString("Direction")){
      case "Clockwise" : TempTrialCCPara.direction=CC_Direction.Clockwise;break;
      case "Anticlockwise" : TempTrialCCPara.direction=CC_Direction.Anticlockwise;break;
    } 
    TrialCC_Vector.add(TempTrialCCPara);
  }
  println("StateIniParameters: TrialCC_Vector");
  XML TrialCL=Trial.getChild("CL");
  XML[] TrialCLParas=TrialCL.getChildren("Para");
  for (int i = 0; i < TrialCLParas.length; i++) {
    ConstrainedLinearMotionPara TempTrialCLPara=new ConstrainedLinearMotionPara();
    TempTrialCLPara.RepeatNum=TrialCLParas[i].getInt("RepeatNum");
    switch(TrialCLParas[i].getInt("Length")){
      case 237: TempTrialCLPara.length=CL_Length.Pixel_237;break;
      case 474: TempTrialCLPara.length=CL_Length.Pixel_474;break;
    }
    switch(TrialCLParas[i].getInt("Width")){
      case 40: TempTrialCLPara.width=CL_Width.Pixel_40;break;
      case 54: TempTrialCLPara.width=CL_Width.Pixel_54;break;
      case 80: TempTrialCLPara.width=CL_Width.Pixel_80;break;
    }
    switch(TrialCLParas[i].getString("Direction")){
      case "Leftward" : TempTrialCLPara.direction=CL_Direction.Leftward;break;
      case "Rightward" : TempTrialCLPara.direction=CL_Direction.Rightward;break;
      case "Upward" : TempTrialCLPara.direction=CL_Direction.Upward;break;
      case "Downward" : TempTrialCLPara.direction=CL_Direction.Downward;break;
    } 
    TrialCL_Vector.add(TempTrialCLPara);
  }
  println("StateIniParameters: TrialCL_Vector");
  
  CurrentState=StateSet.StateDecideTask;
  //saveXML(ExperimentsPara_xml, ExternalStorage + "/ExperimentsPara.xml");
  //TargetAcquisitionPara;
  //ConstrainedLinearMotionPara;
  //ConstrainedCircularMotionPara;
}

enum TaskType{
  TrainTA,
  TrainCC,
  TrainCL,
  TrialTA,
  TrialCC,
  TrialCL
}

TaskType CurrentTask;
int currentTaskCounter=0;
//int currentHandCounter=0;
String CurrentHandUsed;
/* F*:FirstHand S*:SecondHand  *F:FirstForm *S:SecondForm  *T:ThirdForm **T:TrainingSet **A:TrialSet
0 1 2 3 : FFT FFA SFT SFA
4 5 6 7 : FST FSA SST SSA
8 9 10 11: FTT FTA STT STA 
*/
public void FuncStateDecideTask(){
  background(colorBackground);
  fill(255);
  println("StateDecideTask");
  if(currentTaskCounter>=12){
    CurrentState=StateSet.StateFishedXP;
  }
  else{
        switch ((currentTaskCounter/2)%2){
          case 0: {
            CurrentHandUsed=CurrentBeginingHand;
          }break;
          case 1:{
                switch(CurrentBeginingHand){
                  case "right":{CurrentHandUsed="left";}break;
                  case "left":{CurrentHandUsed="right";}break;
                  default:{println("Invalid hand used");}break;
                }
          }break;
       }
            
      switch (TaskArray[currentTaskCounter/4]){ 
        case 'T':{//TARGET AQUISITION
          //CursorSize=8;
          switch(currentTaskCounter%2){
            case 0:{
              //Iterator itr = TrainTA_Vector.iterator(); 
              for(int i=0;i<TrainTA_Vector.size();i++){
                //TargetAcquisitionPara temppara=TrainTA_Vector[i];
                TTAParavector.TargetAcquisitionParaVector.add(TrainTA_Vector.elementAt(i));
              }
              //TTAParavector.TargetAcquisitionParaVector=TrainTA_Vector;
              CurrentState=StateSet.StateTrainingTA;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialTA_Vector.size();i++){
                //TargetAcquisitionPara temppara=TrainTA_Vector[i];
                ATAParavector.TargetAcquisitionParaVector.add(TrialTA_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialTA;
            }break;
        }
          
        }break;
        
        case 'C':{//CONTRAINT CIRCULAR 
        //CursorSize=10;
          switch(currentTaskCounter%2){
            case 0:{
              for(int i=0;i<TrainCC_Vector.size();i++){
                TCCParavector.ConstrainedCircularMotionParaVector.add(TrainCC_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrainingCC;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialCC_Vector.size();i++){
                ACCParavector.ConstrainedCircularMotionParaVector.add(TrialCC_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialCC;
            }break;
        }
        
        } break;
        
        case 'L': {//CONTRAINT LINEAR 
        //CursorSize=10;
          switch(currentTaskCounter%2){
            case 0:{
              for(int i=0;i<TrainCL_Vector.size();i++){
                TCLParavector.ConstrainedLinearMotionParaVector.add(TrainCL_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrainingCL;
            }break;
            case 1:{
              //ATAParavector.TargetAcquisitionParaVector=TrialTA_Vector;
              for(int i=0;i<TrialCL_Vector.size();i++){
                ACLParavector.ConstrainedLinearMotionParaVector.add(TrialCL_Vector.elementAt(i));
              }
              CurrentState=StateSet.StateTrialCL;
            }break;
          }
        }break;
        
        default:{
          println("non valid task form"); 
        }break;
      }
    
      currentTaskCounter++;
  }

  
}

public void FuncStateSaveCSV(){
  String TrajectoryRecordx= new String();
  String TrajectoryRecordy= new String();
  String AccelerometerRecordx= new String();
  String AccelerometerRecordy= new String();
  String AccelerometerRecordz= new String();
  String GyroscopeRecordx= new String();
  String GyroscopeRecordy= new String();
  String GyroscopeRecordz= new String();
  
  if(bEnableRecordTrajectory){
      while(TrajectoryVector.size()>0){
        PVector TempTrajectoryPoint=TrajectoryVector.get(0);
        TrajectoryRecordx=TrajectoryRecordx+Float.toString(TempTrajectoryPoint.x)+",";
        TrajectoryRecordy=TrajectoryRecordy+Float.toString(TempTrajectoryPoint.y)+",";
        //TrajectoryRecord=TrajectoryRecord+"("+Float.toString(TempTrajectoryPoint.x)+","+Float.toString(TempTrajectoryPoint.y)+"),";
        TrajectoryVector.remove(0);
      }
    
      CurrentRow.setString("x",TrajectoryRecordx);
      CurrentRow.setString("y",TrajectoryRecordy);
      
      while(AccelerometerVector.size()>0){
        PVector TempAccelerometer=AccelerometerVector.get(0);
        AccelerometerRecordx=AccelerometerRecordx+Float.toString(TempAccelerometer.x)+",";
        AccelerometerRecordy=AccelerometerRecordy+Float.toString(TempAccelerometer.y)+",";
        AccelerometerRecordz=AccelerometerRecordz+Float.toString(TempAccelerometer.z)+",";
        //AccelerometerRecord=AccelerometerRecord+"("+Float.toString(TempAccelerometer.x)+","+Float.toString(TempAccelerometer.y)+","+Float.toString(TempAccelerometer.z)+"),";
        AccelerometerVector.remove(0);
      }
    
      CurrentRow.setString("ax",AccelerometerRecordx);
      CurrentRow.setString("ay",AccelerometerRecordy);
      CurrentRow.setString("az",AccelerometerRecordz);
      
      while(GyroscopeVector.size()>0){
        PVector TempGyroscope=GyroscopeVector.get(0);
        GyroscopeRecordx=GyroscopeRecordx+Float.toString(TempGyroscope.x)+",";
        GyroscopeRecordy=GyroscopeRecordy+Float.toString(TempGyroscope.y)+",";
        GyroscopeRecordz=GyroscopeRecordz+Float.toString(TempGyroscope.z)+",";
        //GyroscopeRecord=GyroscopeRecord+"("+Float.toString(TempGyroscope.x)+","+Float.toString(TempGyroscope.y)+","+Float.toString(TempGyroscope.z)+"),";
        GyroscopeVector.remove(0);
      }
    
      CurrentRow.setString("wx",GyroscopeRecordx);   
      CurrentRow.setString("wy",GyroscopeRecordy);  
      CurrentRow.setString("wz",GyroscopeRecordz);  
      bRecordTrajectory=false;    
    }
 
CurrentRow.setInt("Participant",NumParticipantFromDropDownList);
saveTable(CurrentTable, ExternalStorage+CurrentTableName);

  switch(OldState){
    case StateTrialTA_Missed:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialTA_Acquired:{CurrentState=StateSet.StateTrialTA_Wait;}break;
    case StateTrialCC_Succeeded:{
          CurrentState=StateSet.StateTrialCC_Wait;
          FuncJudgeSpeedWarningCC();
        }break;
    case StateTrialCC_Failed:{
          CurrentState=StateSet.StateTrialCC_Wait;
          FuncJudgeSpeedWarningCC();
        }break;
    case StateTrialCL_Succeeded:{
          CurrentState=StateSet.StateTrialCL_Wait;
          FuncJudgeSpeedWarningCL();
        }break;
    case StateTrialCL_Failed:{
          CurrentState=StateSet.StateTrialCL_Wait;
          FuncJudgeSpeedWarningCL();
          }break;
    
    default:{
      println("ERRO STATE IN StateSaveCSV");
    }break;
  }
}

public void FuncStateFishedXP(){
        background(colorBackground);
      fill(255);
      println("Enter StateTrainingCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(80);
      fill(colorText);
      text(" Thanks a lot for your participation", width/2,height/2,600,370.8f); 
}

ControlP5 cp5;
DropdownList dNoParticipant,dNoXpPhase;
Button buttonStartXP;
int NumParticipantFromDropDownList=0;
int NumPaseFromDropDownList=0;
public void InitStatePreStartVar(){
  cp5 = new ControlP5(this);
  cp5.setFont(new ControlFont(createFont("CenturyGothic", 30), 30));
  
  dNoParticipant = cp5.addDropdownList("Participant")
          .setPosition(100, 200)
          .setSize(250,800)
          .setScrollSensitivity(0.5f)
          ;
  dNoParticipant.setBackgroundColor(color(190));
  dNoParticipant.setItemHeight(60);
  dNoParticipant.setBarHeight(80);
  dNoParticipant.setColorBackground(color(60));
  dNoParticipant.setColorActive(color(255, 128));
  dNoParticipant.getCaptionLabel().set("Participant");
  
          
  for (int i=0;i<Num_Participants;i++) {
    dNoParticipant.addItem("Participant  "+i, i);
  }
  
  dNoXpPhase = cp5.addDropdownList("Phase")
          .setPosition(500, 200)
          .setSize(250,800)
          .setScrollSensitivity(0.5f)
          ;
  dNoXpPhase.setBackgroundColor(color(190));
  dNoXpPhase.setItemHeight(60);
  dNoXpPhase.setBarHeight(80);
  dNoXpPhase.setColorBackground(color(60));
  dNoXpPhase.setColorActive(color(255, 128));
  dNoXpPhase.getCaptionLabel().set("Phase");
          
  for (int i=0;i<12;i++) {
    dNoXpPhase.addItem("Phase  "+i, i);
  }
  
  buttonStartXP=cp5.addButton("StartXP")
     .setValue(0)
     .setPosition(width/2-100,1200)
     .setSize(200,200)
     .addCallback(new CallbackListener() {
      public void controlEvent(CallbackEvent event) {
        if (event.getAction() == ControlP5.ACTION_PRESS) {
          println("button PRESSED.");
          bStatePreStartFinished=true;
        }
      }
    })
    ;
          
}

boolean bStatePreStartFinished=false;
public void FuncStatePreStart(){
  background(colorBackground);
  if(bStatePreStartFinished){
    bEnableCursor=true;
    CurrentState=StateSet.StateStart;
    dNoXpPhase.remove();
    dNoParticipant.remove();
    buttonStartXP.remove();
    currentTaskCounter=NumPaseFromDropDownList;
  }
}

public void StartXP(int theValue) {
  println("a button event from StartXP: "+theValue);
}


enum StateSet{
  /*PreProcedure*/
  StatePreStart,
  StateStart,
  StateUpdateExperimentsPara_xml,
  StateUpdateExperimentRecord_xml,
  StateCreatRecord_csv,
  StateIniParameters,
  StateDecideTask,
  StateWarnSpeed,
  /*Training TA*/
  StateTrainingTA,
  StateTrainingTA_Init,
  StateTrainingTA_Decide,
  StateTrainingTA_Wait,
  /****StateSubTASet****/
    StateSubTTAinit,
    StateSubTTApressed,
    StateSubTTAjudge,
  /********/
  StateTrainingTA_Missed,
  StateTrainingTA_Acquired,
  StateTrialTA_Decide,
  StateTrialTA_Wait,
  
  /*Trial TA*/
  StateTrialTA,
  StateTrialTA_Init,
   /****StateSubTASet****/
    StateSubATAinit,
    StateSubATApressed,
    StateSubATAjudge,
  /********/
  StateTrialTA_Acquired,
  StateTrialTA_Missed,
  
  
  /*Training CC*/
  StateTrainingCC,
  StateTrainingCC_Decide,
  StateTrainingCC_Wait,
  StateTrainingCC_EnterStartArc,
  StateTrainingCC_BeginSteer,
  StateTrainingCC_Succeeded,
  StateTrainingCC_Failed,
  
  /*Trial CC*/
  StateTrialCC,
  StateTrialCC_Decide,
  StateTrialCC_Wait,
  StateTrialCC_EnterStartArc,
  StateTrialCC_BeginSteer,
  StateTrialCC_Succeeded,
  StateTrialCC_Failed,
  
  /*Training CL*/
  StateTrainingCL,
  StateTrainingCL_Decide,
  StateTrainingCL_Wait,
  StateTrainingCC_EnterStartBar,
  StateTrainingCL_BeginSteer,
  StateTrainingCL_Succeeded,
  StateTrainingCL_Failed,
  
    /*Trial CL*/
  StateTrialCL,
  StateTrialCL_Decide,
  StateTrialCL_Wait,
  StateTrialCC_EnterStartBar,
  StateTrialCL_BeginSteer,
  StateTrialCL_Succeeded,
  StateTrialCL_Failed,
  /*save CSV*/
  StateSaveCSV,
  /* Exit XP*/
  StateFishedXP
}

StateSet CurrentState=StateSet.StatePreStart;
StateSet OldState=StateSet.StateTrainingTA;
enum CC_HandUsed{
        Left,
        Right
    }
    
enum CC_Direction{
        Clockwise, 
        Anticlockwise
    }
    
enum CC_Length{
        Pixel_237,
        Pixel_474,
    }
    
enum CC_Width{
        Pixel_40,
        Pixel_54,
        Pixel_80,
    }

public class ConstrainedCircularPara{
    //int RepeatNum=0;
    CC_HandUsed handUsed;
    CC_Direction direction;
    CC_Length length;
    CC_Width width;
}


/*StateTrainingTA concerned variables*/
float TCC_StartButtonCenterX;
float TCC_StartButtonCenterY;
float TCC_StartButtonWidth;
float TCC_StartButtonHeight;
boolean bPassToTCC_Init;
boolean bSucessCircularSteering=false;
boolean bCircularExceed=false;
boolean bCircularEarlyRelease=false;
boolean bPassSemiCircle=false;
boolean bThreeQuartersCircle=false;
final boolean enableStartArcEcho=false;

public class ConstrainedCircularMotionPara{
    int RepeatNum=0;
    CC_HandUsed handUsed;
    CC_Direction direction;
    CC_Length length;
    CC_Width width;
    public int getCC_Length(){
      switch (length){
        case Pixel_237:{
          return 237;
        }
        case Pixel_474:{
          return 474;
        }

        default: {
          println("None valide length value for CC");
          return 0;
        }
      }
    }
    
    public int getCC_Width(){
      switch (width){
        case Pixel_40:{
          return 40;
        }
        case Pixel_54:{
          return 54;
        }
        case Pixel_80:{
          return 80;
        }
        default: {
          println("None valide width value for CC");
          return 0;
        }
      }
    }
   
}

int NumErrorCC=0;
int NumTotalCC=0;
float ErrorRateCC=0.0f;
float UpperLimitErrorRateCC=0.15f;
float LowerLimitErrorRateCC=0.15f;
boolean bEnableCalculateErrorRateCC=false;


public class ConstrainedCircularMotionParavector extends ConstrainedCircularMotionPara{
  Vector<ConstrainedCircularMotionPara> ConstrainedCircularMotionParaVector=new Vector<ConstrainedCircularMotionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initConstrainedCircularMotionParaVectorManu(){
    for(int i=0;i<10;i++){
      ConstrainedCircularMotionParaVector.add(creatConstrainedCircularMotionPara());
      println(i +"th element been created");
    }
  }
  
  public ConstrainedCircularMotionPara creatConstrainedCircularMotionPara(){
    ConstrainedCircularMotionPara TempConstrainedCircularMotionPara= new ConstrainedCircularMotionPara();
    TempConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
    switch(ceil(random(2))){
      case 1:TempConstrainedCircularMotionPara.direction=CC_Direction.Clockwise;break;
      case 2:TempConstrainedCircularMotionPara.direction=CC_Direction.Anticlockwise;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempConstrainedCircularMotionPara.length=CC_Length.Pixel_237;break;
      case 2:TempConstrainedCircularMotionPara.length=CC_Length.Pixel_474;break;
    }
    switch(ceil(random(3))){
      case 1:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_40;break;
      case 2:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_54;break;
      case 3:TempConstrainedCircularMotionPara.width=CC_Width.Pixel_80;break;
    }
    return TempConstrainedCircularMotionPara;
  }
  */
}

public void initStateTCC(){
  /*StateTrainingCC concerned variables*/
  TCC_StartButtonCenterX=width/2-200;
  TCC_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TCC_StartButtonWidth=250;
  TCC_StartButtonHeight=100;
  bPassToTCC_Init=false;
}

public boolean circleClicked(float x0, float y0, float arcDiameter,float arcWidth ){
  float rInner=(arcDiameter-arcWidth)/2;
  float rExtern=(arcDiameter+arcWidth)/2;
  if((pow((UsedMouseX-x0),2)+pow((UsedMouseY-y0),2)<=pow(rExtern,2))&&(pow((UsedMouseX-x0),2)+pow((UsedMouseY-y0),2)>=pow(rInner,2))){
   return true;
  } else {
    return false;
  }
}

public boolean circleReleased(float x0, float y0, float arcDiameter,float arcWidth,int ReleasedX, int ReleasedY ){
  float rInner=(arcDiameter-arcWidth)/2;
  float rExtern=(arcDiameter+arcWidth)/2;
  if((pow((ReleasedX-x0),2)+pow((ReleasedY-y0),2)<=pow(rExtern,2))&&(pow((ReleasedX-x0),2)+pow((ReleasedY-y0),2)>=pow(rInner,2))){
   return true;
  } else {
    return false;
  }
}

ConstrainedCircularMotionParavector TCCParavector= new ConstrainedCircularMotionParavector();
ConstrainedCircularMotionPara CurrentConstrainedCircularMotionPara=new ConstrainedCircularMotionPara();

CC_HandUsed CC_CurrentHandUsed=CC_HandUsed.Left;
CC_Direction CC_CurrentDirection=CC_Direction.Clockwise;
CC_Length CC_CurrentLength=CC_Length.Pixel_237;
CC_Width CC_CurrentWidth=CC_Width.Pixel_80;
final float degree2randian= (PI/180.0f);
float arcStartAngle=0.0f;
float arcEndAngle=0.0f;
int startArc=15;
int colorStartArc=color(0, 255, 0);

int currentCCTrainNum=0;
int goalCCTrainNum=4;

public void FuncStateTrainingCC(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrainingCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained circular motion training set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TCC_StartButtonCenterX,TCC_StartButtonCenterY,TCC_StartButtonWidth,TCC_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTCC_Init){
      //println(StateSet.StateTrainingTA_Init); 
      background(colorBackground);
      fill(255);
          
      //TCCParavector.initConstrainedCircularMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrainingCC"+ TCCParavector.ConstrainedCircularMotionParaVector.size());
      if(TCCParavector.ConstrainedCircularMotionParaVector.size()>0){
              CurrentConstrainedCircularMotionPara=TCCParavector.ConstrainedCircularMotionParaVector.get(0);
              switch(CurrentHandUsed){
                case "right":{//CurrentConstrainedCircularMotionPara
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
                }break;
              }
              CC_CurrentHandUsed=CurrentConstrainedCircularMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedCircularMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedCircularMotionPara.width;
              switch(CC_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingCC_Decide;
      }
      else{
            println("None valide training set for CC"); 
      }
      //CurrentState=StateSet.StateTrainingTA_Init;
      bPassToTCC_Init=false;
  }
}

public void FuncStateTrainingCC_Decide(){
    println("Enter StateTrainingCC_Decide");
    background(colorBackground);
    fill(255);
    if(TCCParavector.ConstrainedCircularMotionParaVector.size()>0){
          CurrentConstrainedCircularMotionPara=TCCParavector.ConstrainedCircularMotionParaVector.get(0);
          TCCParavector.ConstrainedCircularMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrainingCC_Wait;
          currentCCTrainNum=0;
    }
    else{
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
    }
      
}

public void FuncStateTrainingCC_Wait(){
  println("Enter StateTrainingCC_Wait");
  background(colorBackground);
  fill(255);
  if(currentCCTrainNum<CurrentConstrainedCircularMotionPara.RepeatNum){
      CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
      int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
      int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
      float diameter=circularLength/PI;
      noFill();
      strokeWeight(circularWidth);
      stroke(colorInactivated);
      circle(width/2,height/2-Y_OffsetForComfort,diameter);
      stroke(colorStartLine);
      strokeWeight(widthStartLine);
      strokeCap(SQUARE);
      line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
      
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      
      //switch()
      switch (CC_CurrentDirection){
        case Anticlockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
            }
            else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);             
            }
            println("Anticlockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX>width/2)&&(UsedMouseX<(width/2+diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrainingCC_EnterStartArc;
                         currentCCTrainNum++;
                      }
                  }
              }
            }
          }break;
        case Clockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);           
            }          
            println("Clockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX<width/2)&&(UsedMouseX>(width/2-diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrainingCC_EnterStartArc;
                         currentCCTrainNum++;
                      }
                  }
              }
            }
        }break;
        default:{
                    println("None valide direction CC"); 
         }break;
       }
    }
    else{
        CurrentState=StateSet.StateTrainingCC_Decide;
    }
}

public void FuncStateTrainingCC_EnterStartArc(){
    background(colorBackground);
    fill(255);
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorInactivated);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-diameter/2-400,600,370.8); 
    ///*******/
    switch (CC_CurrentDirection){
      case Anticlockwise:{
          
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
          println("Anticlockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX < width/2){
            CurrentState=StateSet.StateTrainingCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
        }
        
      }break;
      
      case Clockwise:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
           
          println("Clockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX > width/2){
            CurrentState=StateSet.StateTrainingCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
        }
      }break;
      default:{
                  println("None valide direction CC"); 
      }break;
    }
}

public void FuncStateTrainingCC_BeginSteer(){   
    bEnableCursorTrajectory=true;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    if (UsedMousePressed == true){
       if(!(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth))){
         bCircularExceed=true;
         CurrentState=StateSet.StateTrainingCC_Failed;
       }
       else{
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                if((UsedMouseX>=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             case Clockwise:{
                if((UsedMouseX<=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             
             default:{
                  println("None valide direction CC"); 
             }break;
           }
         } 
         
         if(bPassSemiCircle){
            println("PassSemiCircle");
            if(UsedMouseY<height/2-Y_OffsetForComfort){
              bThreeQuartersCircle=true;
            }
         }
         
         if(bThreeQuartersCircle){
           println("ThreeQuartersCircle");
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                 if((UsedMouseX<=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrainingCC_Succeeded;
                       println("Succeeded steering");
                     }
             }break;
             case Clockwise:{
                   //&&(UsedMouseY<(height/2-diameter/2-circularWidth/2))
                   if((UsedMouseX>=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrainingCC_Succeeded;
                       println("Succeeded steering");
                     }
             }break;
             default:{
                        println("None valide direction C"); 
             }break;
           }
         }
       }
       

    if ((!UsedMousePressed) &&(!bSucessCircularSteering)){
          bCircularEarlyRelease=true;
          CurrentState=StateSet.StateTrainingCC_Failed;
          //println("EarlyRelease");
    }
}

public void Func_StateTrainingCC_Failed(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorMissed);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    if(bCircularExceed){
      println("Exceed Path");
    }
    if(bCircularEarlyRelease){
      println("Early Release");
    }
    CurrentState=StateSet.StateTrainingCC_Wait;
}

public void Func_StateTrainingCC_Succeeded(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorAcquired);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    println("Succeeded steering");
    CurrentState=StateSet.StateTrainingCC_Wait;
}

/*StateTrainingTA concerned variables*/
float ACC_StartButtonCenterX;
float ACC_StartButtonCenterY;
float ACC_StartButtonWidth;
float ACC_StartButtonHeight;
boolean bPassToACC_Init=false;

public void initStateACC(){
  /*StateTrainingCC concerned variables*/
  ACC_StartButtonCenterX=width/2-200;
  ACC_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ACC_StartButtonWidth=250;
  ACC_StartButtonHeight=100;
  bPassToACC_Init=false;
}
ConstrainedCircularMotionParavector ACCParavector= new ConstrainedCircularMotionParavector();
int currentCCTrialNum=0;

public void FuncStateTrialCC(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrialCC");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained circular motion Trial set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ACC_StartButtonCenterX,ACC_StartButtonCenterY,ACC_StartButtonWidth,ACC_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToACC_Init){
      //println(StateSet.StateTrialTA_Init); 
      background(colorBackground);
      fill(255);
          
      //ACCParavector.initConstrainedCircularMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrialCC"+ ACCParavector.ConstrainedCircularMotionParaVector.size());
      if(ACCParavector.ConstrainedCircularMotionParaVector.size()>0){
              CurrentConstrainedCircularMotionPara=ACCParavector.ConstrainedCircularMotionParaVector.get(0);
              switch(CurrentHandUsed){
                case "right":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedCircularMotionPara.handUsed=CC_HandUsed.Left;
                }break;
              }
              CC_CurrentHandUsed=CurrentConstrainedCircularMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedCircularMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedCircularMotionPara.width;
              switch(CC_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialCC_Decide;
              initSupCCTasksFlags();
              NumTotalCC=0;
              NumErrorCC=0;
      }
      else{
            println("None valide Trial set for CC"); 
      }
      //CurrentState=StateSet.StateTrialTA_Init;
      bPassToACC_Init=false;
  }
}

  boolean C_A474W40= false;
  boolean C_A237W40= false;
  boolean C_A474W54= false;
  boolean C_A237W54= false;
  boolean C_A474W80= false;
  boolean C_A237W80= false;
  boolean A_A474W40= false;
  boolean A_A237W40= false;
  boolean A_A474W54= false;
  boolean A_A237W54= false;
  boolean A_A474W80= false;
  boolean A_A237W80= false; 
  
  public void initSupCCTasksFlags(){
     C_A474W40= false;
     C_A237W40= false;
     C_A474W54= false;
     C_A237W54= false;
     C_A474W80= false;
     C_A237W80= false;
     A_A474W40= false;
     A_A237W40= false;
     A_A474W54= false;
     A_A237W54= false;
     A_A474W80= false;
     A_A237W80= false;   
  }
  public void CC_CheckTakSucceeded(){
    int CheckResult=0;
    switch(CurrentConstrainedCircularMotionPara.direction){
      case Clockwise:CheckResult+=100;break;
      case Anticlockwise:CheckResult+=200;break;      
    }
    switch(CurrentConstrainedCircularMotionPara.length){
      case Pixel_474: CheckResult+=10;break;
      case Pixel_237: CheckResult+=20;break;
    }
    switch(CurrentConstrainedCircularMotionPara.width){
      case Pixel_40: CheckResult+=1;break;
      case Pixel_54: CheckResult+=2;break;
      case Pixel_80: CheckResult+=3;break;      
    }
      
    switch(CheckResult){
      case 111:C_A474W40=true;break;
      case 112:C_A474W54=true;break;
      case 113:C_A474W80=true;break;
      case 121:C_A237W40=true;break;
      case 122:C_A237W54=true;break;
      case 123:C_A237W80=true;break; 
      case 211:A_A474W40=true;break;
      case 212:A_A474W54=true;break;
      case 213:A_A474W80=true;break;
      case 221:A_A237W40=true;break;
      case 222:A_A237W54=true;break;
      case 223:A_A237W80=true;break; 
    }
  }
 
//public class cl_addFailedTasks{
  //Vector<ConstrainedCircularMotionPara> CLParaVectorToAdd=new Vector<ConstrainedCircularMotionPara>();
//Vector<ConstrainedCircularMotionPara> CLParaVectorToAdd=new Vector<ConstrainedCircularMotionPara>();
//Vector<ConstrainedCircularMotionPara> CC_Sup_Vector=new Vector<ConstrainedCircularMotionPara>();

  public void CC_addFailedTaskToVector(){
    //ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();
      if(C_A474W40 &&C_A474W54 &&C_A474W80 &&C_A237W40 &&C_A237W54 &&C_A237W80
       &&A_A474W40 &&A_A474W54 &&A_A474W80 &&A_A237W40 &&A_A237W54 &&A_A237W80){
       CurrentState=StateSet.StateDecideTask;
        return;
      } 
      else{
        if(!C_A474W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;
          //CLParaVectorToAdd.add(tempCCParaToAdd);
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W40");
        }
        if(!C_A474W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W54");
        }
        if(!C_A474W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A474W80");
        }
        if(!C_A237W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W40");
        }
        if(!C_A237W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W54");
        }
        if(!C_A237W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Clockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("C_A237W80");
        }
        if(!A_A474W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W40");
        }
        if(!A_A474W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W54");
        }
        if(!A_A474W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_474;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A474W80");
        }
        if(!A_A237W40){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_40;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W40");
        }
        if(!A_A237W54){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          //tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_54;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W54");
        }
        if(!A_A237W80){
          ConstrainedCircularMotionPara tempCCParaToAdd =new ConstrainedCircularMotionPara();       
          tempCCParaToAdd.handUsed=CC_HandUsed.Left;//tempCCParaToAdd.handUsed=CC_HandUsed.Left;
          tempCCParaToAdd.direction=CC_Direction.Anticlockwise;
          tempCCParaToAdd.length=CC_Length.Pixel_237;
          tempCCParaToAdd.width=CC_Width.Pixel_80;
          tempCCParaToAdd.RepeatNum=1;       
          ACCParavector.ConstrainedCircularMotionParaVector.add(tempCCParaToAdd);
          println("A_A237W80");
        }
                
                CurrentState=StateSet.StateTrialCC_Decide;
     }

  }
  
public void FuncStateTrialCC_Decide(){
    println("Enter StateTrainingCC_Decide");
    background(colorBackground);
    fill(255);
    if(ACCParavector.ConstrainedCircularMotionParaVector.size()>0){
          CurrentConstrainedCircularMotionPara=ACCParavector.ConstrainedCircularMotionParaVector.get(0);
          ACCParavector.ConstrainedCircularMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrialCC_Wait;
          currentCCTrialNum=0;
    }
    else{
          //CurrentState=StateSet.StateDecideTask;
          //println("No more parameters"); 
          CC_addFailedTaskToVector();
    }
}

public void FuncStateTrialCC_Wait(){
  println("Enter StateTrialCC_Wait");
  if(currentCCTrialNum<CurrentConstrainedCircularMotionPara.RepeatNum){
    
      background(colorBackground);
      fill(255);
      CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
      int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
      int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
      float diameter=circularLength/PI;
      noFill();
      strokeWeight(circularWidth);
      stroke(colorInactivated);
      circle(width/2,height/2-Y_OffsetForComfort,diameter);
      stroke(colorStartLine);
      strokeWeight(widthStartLine);
      strokeCap(SQUARE);
      line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
      
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      
      //switch()
      switch (CC_CurrentDirection){
        case Anticlockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Clockwise");
            }
            else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
              //CurrentRow.setString("Direction", "Anticlockwise");
            }
            
            println("Anticlockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX>width/2)&&(UsedMouseX<(width/2+diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrialCC_EnterStartArc;
                          bRecordTrajectory=true;
                          currentCCTrialNum++;
                          /*log*/
                          CurrentRow= CurrentTable.addRow();                       
                          CurrentRowId++;
                          CurrentRow.setInt("id", CurrentRowId);
                          CurrentRow.setString("TaskType", "CC");
                          CurrentRow.setString("HandUsed", CurrentHandUsed);
                         if(bEnableMirrorImage){ 
                            CurrentRow.setString("Direction", "Clockwise");
                          }
                        else{ 
                            CurrentRow.setString("Direction", "Anticlockwise");
                          }
                      }
                  }
              }
            }
          }break;
        case Clockwise:{
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            
            if(bEnableMirrorImage){
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Clockwise");
            }
            else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
              //CurrentRow.setString("Direction", "Anticlockwise");
            }
            
            println("Clockwise");
            noFill();
            strokeWeight(circularWidth);
            if(enableStartArcEcho){stroke(colorStartArc);}
            else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
            arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
            if (UsedMousePressed == true){
              if(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth)){
                  if((UsedMouseX<width/2)&&(UsedMouseX>(width/2-diameter*sin(startArc*degree2randian)/2))){
                      if(UsedMouseY<((height-(diameter-circularWidth)*sin(75*degree2randian))/2-Y_OffsetForComfort)){
                         CurrentState=StateSet.StateTrialCC_EnterStartArc;
                          bRecordTrajectory=true;
                          currentCCTrialNum++;
                          /*log*/
                          CurrentRow= CurrentTable.addRow();                       
                          CurrentRowId++;
                          CurrentRow.setInt("id", CurrentRowId);
                          CurrentRow.setString("TaskType", "CC");
                          CurrentRow.setString("HandUsed", CurrentHandUsed);
                          
                            if(bEnableMirrorImage){
                              CurrentRow.setString("Direction", "Anticlockwise");
                            }
                            else{
                              CurrentRow.setString("Direction", "Clockwise");
                            }
                         currentCCTrialNum++;
                      }
                  }
              }
            }
        }break;
        default:{
                    println("None valide direction CC"); 
         }break;
       }
    }
    else{
        CurrentState=StateSet.StateTrialCC_Decide;
    }
}

public void FuncStateTrialCC_EnterStartArc(){
    background(colorBackground);
    fill(255);
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    /*log*/
    CurrentRow.setInt("Amplitude/Length", circularLength);
    CurrentRow.setInt("Width", circularWidth);
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorInactivated);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-diameter/2-400,600,370.8); 
    ///*******/
    switch (CC_CurrentDirection){
      case Anticlockwise:{
          
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
              text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          } 
          println("Anticlockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
            if(bEnableMirrorImage){
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
            }
            else{
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
            }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX < width/2){
            CurrentState=StateSet.StateTrialCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
        
      }break;
      
      case Clockwise:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
             text("<--+",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50);
          }
          else{
              text("+-->",width/2,height/2-diameter/2-circularWidth-20-Y_OffsetForComfort,80,50); 
          }
          //text("+-->",width/2,height/2-diameter/2-circularWidth-20,80,50); 
          println("Clockwise");
          noFill();
          strokeWeight(circularWidth);
          if(enableStartArcEcho){stroke(colorStartArc);}
          else{noStroke();}
          if(bEnableMirrorImage){
              arcStartAngle=270*degree2randian;
              arcEndAngle=(270+startArc)*degree2randian;
           }
           else{
              arcStartAngle=(270-startArc)*degree2randian;
              arcEndAngle=270*degree2randian;
           }
          arc(width/2,height/2-Y_OffsetForComfort,diameter,diameter,arcStartAngle,arcEndAngle,OPEN);
          
        if (UsedMousePressed == true && UsedMouseX > width/2){
            CurrentState=StateSet.StateTrialCC_BeginSteer;
            bSucessCircularSteering=false;
            bCircularExceed=false;
            bCircularEarlyRelease=false;
            bPassSemiCircle=false;
            bThreeQuartersCircle=false;
            noFill();
            strokeWeight(circularWidth);
            stroke(colorActivated);
            circle(width/2,height/2-Y_OffsetForComfort,diameter);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            strokeCap(SQUARE);
            line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
      }break;
      default:{
                  println("None valide direction CC"); 
      }break;
    }
}

public void FuncStateTrialCC_BeginSteer(){
    bEnableCursorTrajectory=true;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    if (UsedMousePressed == true){
       if(!(circleClicked(width/2,height/2-Y_OffsetForComfort,diameter,circularWidth))){
         bCircularExceed=true;
         CurrentState=StateSet.StateTrialCC_Failed;
       }
       else{
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                if((UsedMouseX>=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             case Clockwise:{
                if((UsedMouseX<=width/2)&&(UsedMouseY>(height/2+diameter/2-circularWidth/2-Y_OffsetForComfort))){
                         bPassSemiCircle=true;
                }
             }break;
             
             default:{
                  println("None valide direction CC"); 
             }break;
           }
         } 
         
         if(bPassSemiCircle){
            println("PassSemiCircle");
            if(UsedMouseY<height/2-Y_OffsetForComfort){
              bThreeQuartersCircle=true;
            }
         }
         
         if(bThreeQuartersCircle){
           println("ThreeQuartersCircle");
           switch (CC_CurrentDirection){
             case Anticlockwise:{
                 if((UsedMouseX<=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrialCC_Succeeded;
                       println("Succeeded steering");
                       bSucessCircularSteering= true;
                       //CurrentState=StateSet.StateTrialCC_Succeeded;
                        
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        EndTime=timestamp.getTime();
                        /*log*/
                        CurrentRow.setString("EndTime", Long.toString(EndTime));
                        CurrentRow.setString("State", "Successful");
                        CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
                        CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
                        CurrentRow.setString("State","Successful");
                        
                        bRecordTrajectory=false;
                     }
             }break;
             case Clockwise:{
                   //&&(UsedMouseY<(height/2-diameter/2-circularWidth/2))
                   if((UsedMouseX>=width/2)){
                       bSucessCircularSteering= true;
                       CurrentState=StateSet.StateTrialCC_Succeeded;
                       
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        EndTime=timestamp.getTime();
                        /*log*/
                        CurrentRow.setString("EndTime", Long.toString(EndTime));
                        CurrentRow.setString("State", "Successful");
                        CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
                        CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
                        CurrentRow.setString("State","Successful");
                       println("Succeeded steering");
                       bRecordTrajectory=false;
                     }
             }break;
             default:{
                        println("None valide direction C"); 
             }break;
           }
         }
       }
       

    if ((!UsedMousePressed) &&(!bSucessCircularSteering)){
          bCircularEarlyRelease=true;
          CurrentState=StateSet.StateTrialCC_Failed;
          
          //println("Succeeded steering");
          //println("EarlyRelease");
    }
}

public void FuncStateTrialCC_Succeeded(){
    NumTotalCC++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorAcquired);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    println("Succeeded steering");
    CurrentState=StateSet.StateSaveCSV;
    OldState=StateSet.StateTrialCC_Succeeded;
    //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
        
    //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    //EndTime=timestamp.getTime();
    //CurrentRow.setString("EndTime", Long.toString(EndTime));
    //CurrentRow.setString("State", "Successful");
    //CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    //CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
    CC_CheckTakSucceeded();
}

public void FuncStateTrialCC_Failed(){
    NumTotalCC++;
    NumErrorCC++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CC_CurrentDirection=CurrentConstrainedCircularMotionPara.direction;
    int circularWidth=CurrentConstrainedCircularMotionPara.getCC_Width();
    int circularLength=CurrentConstrainedCircularMotionPara.getCC_Length();
    float diameter=circularLength/PI;
    noFill();
    strokeWeight(circularWidth);
    stroke(colorMissed);
    circle(width/2,height/2-Y_OffsetForComfort,diameter);
    stroke(colorStartLine);
    strokeWeight(widthStartLine);
    strokeCap(SQUARE);
    line(width/2,height/2-diameter/2-circularWidth/2-Y_OffsetForComfort,width/2,height/2-diameter/2+circularWidth/2-Y_OffsetForComfort);
    if(bCircularExceed){
      println("Exceed Path");
      /*log*/
      CurrentRow.setString("ErrorType","Exceed Path");
    }
    if(bCircularEarlyRelease){
      println("Early Release");
      /*log*/
      CurrentRow.setString("ErrorType","Early Release");
    }
    
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    EndTime=timestamp.getTime();
    /*log*/
    CurrentRow.setString("EndTime", Long.toString(EndTime));
    CurrentRow.setString("State", "Successful");
    CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
    CurrentRow.setString("State","Failed");
          
    bRecordTrajectory=false;
    
    //CurrentState=StateSet.StateTrialCC_Wait;
    CurrentState=StateSet.StateSaveCSV;
    OldState=StateSet.StateTrialCC_Failed;
    //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
}

public void FuncJudgeSpeedWarningCC(){
    if(NumTotalCC%6==0){
      ErrorRateCC= (float)NumErrorCC/ (float)NumTotalCC;
      if(ErrorRateCC>UpperLimitErrorRateCC){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=1;
      }
      else if(ErrorRateCC<LowerLimitErrorRateCC){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=2;
      }
      else{
        CurrentState=StateSet.StateTrialCC_Wait;
        bNeedSpeedUp=0;
       }
    }

}
enum CL_HandUsed{
        Left,
        Right
    }
    
enum CL_Direction{
        Leftward,
        Rightward,
        Upward,
        Downward
    }
    
enum CL_Length{
        Pixel_237,
        Pixel_474,
    }
    
enum CL_Width{
        Pixel_40,
        Pixel_54,
        Pixel_80,
    }

public class ConstrainedLinearPara{
    //int RepeatNum=0;
    CL_HandUsed handUsed;
    CL_Direction direction;
    CL_Length length;
    CL_Width width;
}


/*StateTrainingTA concerned variables*/
float TCL_StartButtonCenterX;
float TCL_StartButtonCenterY;
float TCL_StartButtonWidth;
float TCL_StartButtonHeight;
boolean bPassToTCL_Init;
boolean bSucessLinearSteering=false;
boolean bLinearExceed=false;
boolean bLinearEarlyRelease=false;

final boolean enableStartBarEcho=false;

public class ConstrainedLinearMotionPara{
    int RepeatNum=0;
    CL_HandUsed handUsed;
    CL_Direction direction;
    CL_Length length;
    CL_Width width;
    public int getCL_Length(){
      switch (length){
        case Pixel_237:{
          return 237;
        }
        case Pixel_474:{
          return 474;
        }

        default: {
          println("None valide length value for CC");
          return 0;
        }
      }
    }
    
    public int getCL_Width(){
      switch (width){
        case Pixel_40:{
          return 40;
        }
        case Pixel_54:{
          return 54;
        }
        case Pixel_80:{
          return 80;
        }
        default: {
          println("None valide width value for CC");
          return 0;
        }
      }
    }
   
}

//public class CL_AllFailed extends ConstrainedLinearMotionPara{
  boolean L_A474W40= false;
  boolean L_A237W40= false;
  boolean L_A474W54= false;
  boolean L_A237W54= false;
  boolean L_A474W80= false;
  boolean L_A237W80= false;
  boolean R_A474W40= false;
  boolean R_A237W40= false;
  boolean R_A474W54= false;
  boolean R_A237W54= false;
  boolean R_A474W80= false;
  boolean R_A237W80= false; 
  boolean U_A474W40= false;
  boolean U_A237W40= false;
  boolean U_A474W54= false;
  boolean U_A237W54= false;
  boolean U_A474W80= false;
  boolean U_A237W80= false;
  boolean D_A474W40= false;
  boolean D_A237W40= false;
  boolean D_A474W54= false;
  boolean D_A237W54= false;
  boolean D_A474W80= false;
  boolean D_A237W80= false;
  
public void initSupCLTasksFlags(){
     L_A474W40= false;
     L_A237W40= false;
     L_A474W54= false;
     L_A237W54= false;
     L_A474W80= false;
     L_A237W80= false;
     R_A474W40= false;
     R_A237W40= false;
     R_A474W54= false;
     R_A237W54= false;
     R_A474W80= false;
     R_A237W80= false; 
     U_A474W40= false;
     U_A237W40= false;
     U_A474W54= false;
     U_A237W54= false;
     U_A474W80= false;
     U_A237W80= false;
     D_A474W40= false;
     D_A237W40= false;
     D_A474W54= false;
     D_A237W54= false;
     D_A474W80= false;
     D_A237W80= false;
}    
  //(left/right)_(L/R/U/D)_(474/237)_(40/54/80)    
  public void CL_CheckTakSucceeded(){
    int CheckResult=0;
    switch(CurrentConstrainedLinearMotionPara.direction){
      case Leftward:CheckResult+=100;break;
      case Rightward:CheckResult+=200;break;
      case Upward:CheckResult+=300;break;
      case Downward:CheckResult+=400;break;      
    }
    switch(CurrentConstrainedLinearMotionPara.length){
      case Pixel_474: CheckResult+=10;break;
      case Pixel_237: CheckResult+=20;break;
    }
    switch(CurrentConstrainedLinearMotionPara.width){
      case Pixel_40: CheckResult+=1;break;
      case Pixel_54: CheckResult+=2;break;
      case Pixel_80: CheckResult+=3;break;      
    }
      
    switch(CheckResult){
      case 111:L_A474W40=true;break;
      case 112:L_A474W54=true;break;
      case 113:L_A474W80=true;break;
      case 121:L_A237W40=true;break;
      case 122:L_A237W54=true;break;
      case 123:L_A237W80=true;break; 
      case 211:R_A474W40=true;break;
      case 212:R_A474W54=true;break;
      case 213:R_A474W80=true;break;
      case 221:R_A237W40=true;break;
      case 222:R_A237W54=true;break;
      case 223:R_A237W80=true;break; 
      case 311:U_A474W40=true;break;
      case 312:U_A474W54=true;break;
      case 313:U_A474W80=true;break;
      case 321:U_A237W40=true;break;
      case 322:U_A237W54=true;break;
      case 323:U_A237W80=true;break; 
      case 411:D_A474W40=true;break;
      case 412:D_A474W54=true;break;
      case 413:D_A474W80=true;break;
      case 421:D_A237W40=true;break;
      case 422:D_A237W54=true;break;
      case 423:D_A237W80=true;break; 
    }
  }
 
//public class cl_addFailedTasks{
  //Vector<ConstrainedLinearMotionPara> CLParaVectorToAdd=new Vector<ConstrainedLinearMotionPara>();
//Vector<ConstrainedLinearMotionPara> CLParaVectorToAdd=new Vector<ConstrainedLinearMotionPara>();
//Vector<ConstrainedLinearMotionPara> CL_Sup_Vector=new Vector<ConstrainedLinearMotionPara>();

  public void CL_addFailedTaskToVector(){
    //ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();
      if(L_A474W40 &&L_A474W54 &&L_A474W80 &&L_A237W40 &&L_A237W54 &&L_A237W80
       &&R_A474W40 &&R_A474W54 &&R_A474W80 &&R_A237W40 &&R_A237W54 &&R_A237W80
       &&U_A474W40 &&U_A474W54 &&U_A474W80 &&U_A237W40 &&U_A237W54 &&U_A237W80
       &&D_A474W40 &&D_A474W54 &&D_A474W80 &&D_A237W40 &&D_A237W54 &&D_A237W80){
       CurrentState=StateSet.StateDecideTask;
        return;
      } 
      else{
        if(!L_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;
          //CLParaVectorToAdd.add(tempCLParaToAdd);
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W40");
        }
        if(!L_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W54");
        }
        if(!L_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A474W80");
        }
        if(!L_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W40");
        }
        if(!L_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W54");
        }
        if(!L_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Leftward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("L_A237W80");
        }
        if(!R_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W40");
        }
        if(!R_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W54");
        }
        if(!R_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A474W80");
        }
        if(!R_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W40");
        }
        if(!R_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W54");
        }
        if(!R_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Rightward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("R_A237W80");
        }
        if(!U_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W40");
        }
        if(!U_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W54");
        }
        if(!U_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W80");
        }
        if(!U_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A474W80");
        }
        if(!U_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A237W54");
        }
        if(!U_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Upward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("U_A237W80");
        }
        if(!D_A474W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;      
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W40");
        }
        if(!D_A474W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W54");
        }
        if(!D_A474W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_474;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A474W80");
        }
        if(!D_A237W40){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_40;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W40");
        }
        if(!D_A237W54){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_54;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W54");
        }
        if(!D_A237W80){
          ConstrainedLinearMotionPara tempCLParaToAdd =new ConstrainedLinearMotionPara();       
          //tempCLParaToAdd.handUsed=CL_HandUsed.Left;
          tempCLParaToAdd.direction=CL_Direction.Downward;
          tempCLParaToAdd.length=CL_Length.Pixel_237;
          tempCLParaToAdd.width=CL_Width.Pixel_80;
          tempCLParaToAdd.RepeatNum=1;       
          ACLParavector.ConstrainedLinearMotionParaVector.add(tempCLParaToAdd);
          println("D_A237W80");
        }
                
                CurrentState=StateSet.StateTrialCL_Decide;
     }

  }
//}



//CL_AllFailed CL_AllFailedData=new CL_AllFailed();

int NumErrorCL=0;
int NumTotalCL=0;
float ErrorRateCL=0.0f;
float UpperLimitErrorRateCL=0.15f;
float LowerLimitErrorRateCL=0.15f;
boolean bEnableCalculateErrorRateCL=false;

public class ConstrainedLinearMotionParavector extends ConstrainedLinearMotionPara{
  Vector<ConstrainedLinearMotionPara> ConstrainedLinearMotionParaVector=new Vector<ConstrainedLinearMotionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initConstrainedLinearMotionParaVectorManu(){
    for(int i=0;i<10;i++){
      ConstrainedLinearMotionParaVector.add(creatConstrainedLinearMotionPara());
      println(i +"th element been created");
    }
  }
  
  public ConstrainedLinearMotionPara creatConstrainedLinearMotionPara(){
    ConstrainedLinearMotionPara TempConstrainedLinearMotionPara= new ConstrainedLinearMotionPara();
    TempConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
    switch(ceil(random(4))){
      case 1:TempConstrainedLinearMotionPara.direction=CL_Direction.Leftward;break;
      case 2:TempConstrainedLinearMotionPara.direction=CL_Direction.Rightward;break;
      case 3:TempConstrainedLinearMotionPara.direction=CL_Direction.Upward;break;
      case 4: TempConstrainedLinearMotionPara.direction=CL_Direction.Downward;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempConstrainedLinearMotionPara.length=CL_Length.Pixel_237;break;
      case 2:TempConstrainedLinearMotionPara.length=CL_Length.Pixel_474;break;
    }
    switch(ceil(random(3))){
      case 1:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_40;break;
      case 2:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_54;break;
      case 3:TempConstrainedLinearMotionPara.width=CL_Width.Pixel_80;break;
    }
    return TempConstrainedLinearMotionPara;
  }
  */
}

public void initStateTCL(){
  /*StateTrainingCL concerned variables*/
  TCL_StartButtonCenterX=width/2-200;
  TCL_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TCL_StartButtonWidth=250;
  TCL_StartButtonHeight=100;
  bPassToTCL_Init=false;
}


ConstrainedLinearMotionParavector TCLParavector= new ConstrainedLinearMotionParavector();
ConstrainedLinearMotionPara CurrentConstrainedLinearMotionPara=new ConstrainedLinearMotionPara();

CL_HandUsed CL_CurrentHandUsed=CL_HandUsed.Left;
CL_Direction CL_CurrentDirection=CL_Direction.Leftward;
CL_Length CL_CurrentLength=CL_Length.Pixel_237;
CL_Width CL_CurrentWidth=CL_Width.Pixel_80;

final float startBarExtend=20.0f;
int colorStartBar=color(0, 255, 0);

int currentCLTrainNum=0;
int goalCLTrainNum=4;

public void FuncStateTrainingCL(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrainingCL");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained linear motion training set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TCL_StartButtonCenterX,TCL_StartButtonCenterY,TCL_StartButtonWidth,TCL_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTCL_Init){
      //println(StateSet.StateTrainingTA_Init); 
      background(colorBackground);
      fill(255);
          
      //TCLParavector.initConstrainedLinearMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrainingCL"+ TCLParavector.ConstrainedLinearMotionParaVector.size());
      if(TCLParavector.ConstrainedLinearMotionParaVector.size()>0){
              CurrentConstrainedLinearMotionPara=TCLParavector.ConstrainedLinearMotionParaVector.get(0);
               switch(CurrentHandUsed){
                case "right":{//CurrentConstrainedLinearMotionPara
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
                }break;
              }
              CL_CurrentHandUsed=CurrentConstrainedLinearMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedLinearMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedLinearMotionPara.width;
              switch(CL_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingCL_Decide;
              NumTotalCL=0;
              NumErrorCL=0;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonWeight); */
      }
      else{
            println("None valide training set for CL"); 
      }
      //CurrentState=StateSet.StateTrainingTA_Init;
      bPassToTCL_Init=false;
  }
}

public void FuncStateTrainingCL_Decide(){
    println("Enter StateTrainingCL_Decide");
    background(colorBackground);
    fill(255);
    if(TCLParavector.ConstrainedLinearMotionParaVector.size()>0){
          CurrentConstrainedLinearMotionPara=TCLParavector.ConstrainedLinearMotionParaVector.get(0);
          TCLParavector.ConstrainedLinearMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrainingCL_Wait;
          currentCLTrainNum=0;
    }
    else{
         CurrentState=StateSet.StateDecideTask;
      println("No more parameters"); 
    }
      
}

public void FuncStateTrainingCL_Wait(){
  println("Enter StateTrainingCL_Wait");
  if(currentCLTrainNum<CurrentConstrainedLinearMotionPara.RepeatNum){
      background(colorBackground);
      fill(255);
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
   
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }

          float startBarExtendX_Left=width/2+linearLength/2;
          float startBarExtendX_Right=width/2+startBarExtend+linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
          } 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);

          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }

          float startBarExtendX_Left=width/2-startBarExtend-linearLength/2;
          float startBarExtendX_Right=width/2-linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
                    
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2+linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2+startBarExtend+linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2-startBarExtend-linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2-linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrainingCC_EnterStartBar;
                currentCLTrainNum++;
              //}
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
  }
  
  else{
        CurrentState=StateSet.StateTrainingCL_Decide;
  }
    
}

public void FuncStateTrainingCC_EnterStartBar(){
    background(colorBackground);
    fill(255);
    println("Enter StateTrainingCC_EnterStartBar");
    
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX <width/2+linearLength/2){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            println("Leftward");
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            } 
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
          } 
          //text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20,80,50); 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX >width/2-linearLength/2){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50); 
            }             println("Rightward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          if (UsedMousePressed == true && UsedMouseY <height/2+linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(-HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Upward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);         
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          if (UsedMousePressed == true && UsedMouseY >height/2-linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrainingCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Downward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);        
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
}

public void FuncStateTrainingCL_BeginSteer(){
  bEnableCursorTrajectory=true;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){

    case Leftward:{
      if(UsedMousePressed == true){
        if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX>(width/2-linearLength/2))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
        else if(UsedMouseX<=(width/2-linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
        }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    
    case Rightward:{
       if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX<(width/2+linearLength/2))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if((UsedMouseX>=(width/2+linearLength/2))){
                bSucessLinearSteering=true;
                CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    
    case Upward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY>(height/2-linearLength/2-Y_OffsetForComfort))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if(UsedMouseY<=(height/2-linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    }break;
    case Downward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY<(height/2+linearLength/2-Y_OffsetForComfort))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrainingCL_Failed;
          }
          else if (UsedMouseY>=(height/2-Y_OffsetForComfort+linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrainingCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrainingCL_Failed;
        }
      }
    
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
  
}

public void FuncStateTrainingCL_Succeeded(){
  background(colorBackground);
  fill(255);
  bEnableCursorTrajectory=false;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){
    case Leftward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Rightward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Upward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    case Downward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          CurrentState=StateSet.StateTrainingCL_Wait;
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
    println("StateTrainingCL_Succeeded");
}
  
public void FuncStateTrainingCL_Failed(){
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
    int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
    int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
    switch (CL_CurrentDirection){
      case Leftward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Rightward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Upward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      case Downward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
             stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            CurrentState=StateSet.StateTrainingCL_Wait;
      }break;
      default:{
              println("None valide direction CL"); 
      }break;
    }
    println("StateTrainingCL_Failed");
    if(bLinearExceed){
      println("bLinearExceed");
    }
    if(bLinearEarlyRelease){
      println("bLinearEarlyRelease");
    }
}



/*StateTrainingTA concerned variables*/
float ACL_StartButtonCenterX;
float ACL_StartButtonCenterY;
float ACL_StartButtonWidth;
float ACL_StartButtonHeight;
boolean bPassToACL_Init;

public void initStateACL(){
  /*StateTrainingCL concerned variables*/
  ACL_StartButtonCenterX=width/2-200;
  ACL_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ACL_StartButtonWidth=250;
  ACL_StartButtonHeight=100;
  bPassToACL_Init=false;
}

ConstrainedLinearMotionParavector ACLParavector= new ConstrainedLinearMotionParavector();
//ConstrainedLinearMotionPara CurrentConstrainedLinearMotionPara=new ConstrainedLinearMotionPara();


int currentCLTrialNum=0;
int goalCLTrialNum=4;

public void FuncStateTrialCL(){
      background(colorBackground);
      fill(255);
      println("Enter StateTrialCL");
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to constrained linear motion Trial set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ACL_StartButtonCenterX,ACL_StartButtonCenterY,ACL_StartButtonWidth,ACL_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToACL_Init){
      //println(StateSet.StateTrialTA_Init); 
      background(colorBackground);
      fill(255);
          
      //ACLParavector.initConstrainedLinearMotionParaVectorManu();
      //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
      println("StateTrialCL"+ ACLParavector.ConstrainedLinearMotionParaVector.size());
      if(ACLParavector.ConstrainedLinearMotionParaVector.size()>0){
              CurrentConstrainedLinearMotionPara=ACLParavector.ConstrainedLinearMotionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
                }break;
                case "left":{
                  CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
                }break;
              }
              CL_CurrentHandUsed=CurrentConstrainedLinearMotionPara.handUsed;
              //TA_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
              //TA_CurrentAmplitude=CurrentConstrainedLinearMotionPara.amplitude;
              //TA_CurrentWidth=CurrentConstrainedLinearMotionPara.width;
              switch(CL_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to steer the path", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialCL_Decide;
              initSupCLTasksFlags();
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonWeight); */
      }
      else{
            println("None valide Trial set for CL"); 
      }
      //CurrentState=StateSet.StateTrialTA_Init;
      bPassToACL_Init=false;
  }
}

public void FuncStateTrialCL_Decide(){
    println("Enter StateTrialCL_Decide");
    background(colorBackground);
    fill(255);
    if(ACLParavector.ConstrainedLinearMotionParaVector.size()>0){
          CurrentConstrainedLinearMotionPara=ACLParavector.ConstrainedLinearMotionParaVector.get(0);
          ACLParavector.ConstrainedLinearMotionParaVector.remove(0);
          CurrentState=StateSet.StateTrialCL_Wait;
          currentCLTrialNum=0;
    }
    else{
      CL_addFailedTaskToVector();
      //CurrentState=StateSet.StateTrialCL_Wait;
      //currentCLTrialNum=0;
      //CurrentState=StateSet.StateDecideTask;
    }
      
}


public void FuncStateTrialCL_Wait(){
  println("Enter StateTrialCL_Wait");
  println("vector size:" + ACLParavector.ConstrainedLinearMotionParaVector.size());
  
  if(currentCLTrialNum<CurrentConstrainedLinearMotionPara.RepeatNum){
      background(colorBackground);
      fill(255);
      /*********/
      //rectMode(CENTER);
      //textSize(50);
      //fill(colorText);
      //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2+200,600,370.8); 
      /*******/
      switch(CurrentHandUsed){
       case "right":{
           CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Right;
        }break;
       case "left":{
           CurrentConstrainedLinearMotionPara.handUsed=CL_HandUsed.Left;
        }break;
      }
              
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      
      println("linearLength: "+linearLength);
      println("linearWidth: "+linearWidth);
            
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          float startBarExtendX_Left=width/2+linearLength/2;
          float startBarExtendX_Right=width/2+startBarExtend+linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);
              //}
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          } 
          
          //text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20,80,50); 
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          float startBarExtendX_Left=width/2-startBarExtend-linearLength/2;
          float startBarExtendX_Right=width/2-linearLength/2;
          float startBarExtendY_Up=height/2-Y_OffsetForComfort-linearWidth/2;
          float startBarExtendY_Down=height/2-Y_OffsetForComfort+linearWidth/2;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);
              //}
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2+linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2+startBarExtend+linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);                
              //}
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          float startBarExtendX_Left=width/2-linearWidth/2;
          float startBarExtendX_Right=width/2+linearWidth/2;
          float startBarExtendY_Up=height/2-startBarExtend-linearLength/2-Y_OffsetForComfort;
          float startBarExtendY_Down=height/2-linearLength/2-Y_OffsetForComfort;
          
          if (UsedMousePressed == true&& 
              UsedMouseX>startBarExtendX_Left&&
              UsedMouseX<startBarExtendX_Right&&
              UsedMouseY>startBarExtendY_Up&&UsedMouseY<startBarExtendY_Down){
              //if(RectButtonClicked(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend)){
                CurrentState=StateSet.StateTrialCC_EnterStartBar;
                bRecordTrajectory=true;
                currentCLTrialNum++;
                  /*log*/
                  CurrentRow= CurrentTable.addRow();                       
                  CurrentRowId++;
                  CurrentRow.setInt("id", CurrentRowId);
                  CurrentRow.setString("TaskType", "CL");
                  CurrentRow.setString("HandUsed", CurrentHandUsed);                
              //}
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
  }
  
  else{
        CurrentState=StateSet.StateTrialCL_Decide;
  }
    
}

public void FuncStateTrialCC_EnterStartBar(){
    background(colorBackground);
    fill(255);
    println("Enter StateTrialCC_EnterStartBar");
    ///*********/
    //rectMode(CENTER);
    //textSize(50);
    //fill(colorText);
    //text("Please put your index finger behind the start line and follow the arrow as steering direction ", width/2,height/2-600,600,370.8); 
    ///*******/
    
      CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
      int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
      int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
      switch(CL_CurrentDirection){
        case Leftward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          println("Leftward");
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
          rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX <width/2+linearLength/2){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            
            if(bEnableMirrorImage){
              CurrentRow.setString("Direction", "Rightward");
            }
            else{
              CurrentRow.setString("Direction", "Leftward");
            }
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            println("Leftward");
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }                        
            //text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20,80,50); 
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Rightward:{
          rectMode(CENTER);
          textSize(40);
          fill(colorText);
          if(bEnableMirrorImage){
            text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          else{
            text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
          }
          println("Rightward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          if(bEnableMirrorImage){
            rect(width/2+startBarExtend/2+linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          else{
            rect(width/2-startBarExtend/2-linearLength/2, height/2-Y_OffsetForComfort, startBarExtend,linearWidth);
          }
          if (UsedMousePressed == true && UsedMouseX >width/2-linearLength/2){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            if(bEnableMirrorImage){
              CurrentRow.setString("Direction", "Leftward");
            }
            else{
              CurrentRow.setString("Direction", "Rightward");
            }
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);
            
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            rectMode(CENTER);
            textSize(40);
            fill(colorText);
            if(bEnableMirrorImage){
              text("<--+",width/2+startBarExtend/2+linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            else{
              text("+-->",width/2-startBarExtend/2-linearLength/2,height/2-linearWidth/2-20-Y_OffsetForComfort,80,50);
            }
            println("Rightward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);         
          }
        }break;
        case Upward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(-HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Upward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
            
          if (UsedMousePressed == true && UsedMouseY <height/2+linearLength/2-Y_OffsetForComfort){
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("Direction", "Upward");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth);            
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            //CurrentState=StateSet.StateTrialCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2+startBarExtend/2+linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(-HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Upward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);         
          }
        }break;
        case Downward:{
          textAlign(CENTER,BOTTOM);
          pushMatrix();
          translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
          textSize(40);
          fill(colorText);
          rotate(HALF_PI);
          text("+-->",0,0); 
          popMatrix();
          println("Downward");
          noStroke();
          fill(colorInactivated);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          noStroke();
          if(enableStartBarEcho)fill(colorStartBar);
          else noFill();
          rectMode(CENTER);
          rect(width/2, height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort, linearWidth,startBarExtend);
          //rect(width/2+startBarExtend/2, height/2, startBarExtend,linearWidth);
          if (UsedMousePressed == true && UsedMouseY >height/2-linearLength/2-Y_OffsetForComfort){
            
            CurrentState=StateSet.StateTrialCL_BeginSteer;
                        
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            StartTime=timestamp.getTime();
            /*log*/
            
            CurrentRow.setString("Direction", "Downward");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
            CurrentRow.setString("StartTime", Long.toString(StartTime));
            CurrentRow.setInt("Amplitude/Length", linearLength);
            CurrentRow.setInt("Width", linearWidth); 
            
            bSucessLinearSteering=false;
            bLinearExceed=false;
            bLinearEarlyRelease=false;
            
            CurrentState=StateSet.StateTrialCL_BeginSteer;
            textAlign(CENTER,BOTTOM);
            pushMatrix();
            translate(width/2-linearWidth/2-50,height/2-startBarExtend/2-linearLength/2-Y_OffsetForComfort);
            textSize(40);
            fill(colorText);
            rotate(HALF_PI);
            text("+-->",0,0); 
            popMatrix();
            println("Downward");
            noStroke();
            fill(colorActivated);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);        
          }
        }break;
        default:{
            println("None valide direction CL"); 
        }break;
      }
}

public void FuncStateTrialCL_BeginSteer(){
  bEnableCursorTrajectory=true;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  switch (CL_CurrentDirection){

    case Leftward:{
      if(UsedMousePressed == true){
        if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX>(width/2-linearLength/2))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
        else if(UsedMouseX<=(width/2-linearLength/2)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;

        }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    
    case Rightward:{
       if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth))&&(UsedMouseX<(width/2+linearLength/2))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if((UsedMouseX>=(width/2+linearLength/2))){
                bSucessLinearSteering=true;
                CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    
    case Upward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY>(height/2-linearLength/2-Y_OffsetForComfort))){
            bLinearExceed=true;
            CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if(UsedMouseY<=(height/2-linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    }break;
    case Downward:{
        if(UsedMousePressed == true){
          if((!RectButtonClicked(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength))&&(UsedMouseY<(height/2+linearLength/2-Y_OffsetForComfort))){
              bLinearExceed=true;
              CurrentState=StateSet.StateTrialCL_Failed;
          }
          else if (UsedMouseY>=(height/2+linearLength/2-Y_OffsetForComfort)){
            bSucessLinearSteering=true;
            CurrentState=StateSet.StateTrialCL_Succeeded;
          }
      }
      else{
        if(!bSucessLinearSteering){
            bLinearEarlyRelease=true;
            CurrentState=StateSet.StateTrialCL_Failed;
        }
      }
    
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
  
}

public void FuncStateTrialCL_Succeeded(){
  background(colorBackground);
  fill(255);
  bEnableCursorTrajectory=false;
  NumTotalCL++;
  CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
  int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
  int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
  
  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
  EndTime=timestamp.getTime();
  /*log*/
  CurrentRow.setString("EndTime", Long.toString(EndTime));
  CurrentRow.setString("State", "Successful");
  CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
  CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
  CurrentRow.setString("State","Successful");
  
  bRecordTrajectory=false;
  
  switch (CL_CurrentDirection){
    case Leftward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Rightward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Upward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
          stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    case Downward:{
          noStroke();
          fill(colorAcquired);
          rectMode(CENTER);
          rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
          //println("linearLength: "+linearLength);
          //println("linearWidth: "+linearWidth);
           stroke(colorStartLine);
          strokeWeight(widthStartLine);
          /*Startline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
          /*Endline*/
          strokeCap(SQUARE);
          line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Succeeded;
    }break;
    default:{
            println("None valide direction CL"); 
    }break;
  }
    println("StateTrialCL_Succeeded");
    
    CL_CheckTakSucceeded();
}
  
public void FuncStateTrialCL_Failed(){
    NumTotalCL++;
    NumErrorCL++;
    background(colorBackground);
    fill(255);
    bEnableCursorTrajectory=false;
    CL_CurrentDirection=CurrentConstrainedLinearMotionPara.direction;
    int linearWidth=CurrentConstrainedLinearMotionPara.getCL_Width();
    int linearLength=CurrentConstrainedLinearMotionPara.getCL_Length();
    
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    EndTime=timestamp.getTime();
    /*log*/
    CurrentRow.setString("EndTime", Long.toString(EndTime));
    CurrentRow.setString("State", "Successful");
    CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
    CurrentRow.setString("EndPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
    CurrentRow.setString("State","Failed");
    
    bRecordTrajectory=false;
    
    switch (CL_CurrentDirection){
      case Leftward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Rightward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearLength,linearWidth);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2-linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2-linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearLength/2,height/2-linearWidth/2-Y_OffsetForComfort,width/2+linearLength/2,height/2+linearWidth/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Upward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
            stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      case Downward:{
            noStroke();
            fill(colorMissed);
            rectMode(CENTER);
            rect(width/2, height/2-Y_OffsetForComfort, linearWidth,linearLength);
            //println("linearLength: "+linearLength);
            //println("linearWidth: "+linearWidth);
             stroke(colorStartLine);
            strokeWeight(widthStartLine);
            /*Startline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2-linearLength/2-Y_OffsetForComfort);
            /*Endline*/
            strokeCap(SQUARE);
            line(width/2+linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort,width/2-linearWidth/2,height/2+linearLength/2-Y_OffsetForComfort);
            //CurrentState=StateSet.StateTrialCL_Wait;
            CurrentState=StateSet.StateSaveCSV;
            OldState=StateSet.StateTrialCL_Failed;
      }break;
      default:{
              println("None valide direction CL"); 
      }break;
    }
    println("StateTrialCL_Failed");
    if(bLinearExceed){
      println("bLinearExceed");
      CurrentRow.setString("ErrorType","Exceed Path");
    }
    if(bLinearEarlyRelease){
      println("bLinearEarlyRelease");
      CurrentRow.setString("ErrorType","Early Release");
    }
}

public void FuncJudgeSpeedWarningCL(){
    if(NumTotalCL%6==0){
      ErrorRateCL= (float)NumErrorCL/ (float)NumTotalCL;
      if(ErrorRateCL>UpperLimitErrorRateCL){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=1;
      }
      else if(ErrorRateCL<LowerLimitErrorRateCL){
        CurrentState=StateSet.StateWarnSpeed;
        bNeedSpeedUp=2;
      }
      else{
        CurrentState=StateSet.StateTrialCL_Wait;
        bNeedSpeedUp=0;
       }
    }

}


enum TA_HandUsed{
        Left,
        Right
    }

enum TA_Direction{
        Horizontal,//0
        Vertical //1
    } 
    
enum TA_Amplitude{
        Pixel_144,
        Pixel_288,
        Pixel_576,
    }
enum TA_Width{
        Pixel_24,
        Pixel_48,
        Pixel_96,
    }

public class TargetAcquisitionPara{
    int RepeatNum=0;
    TA_HandUsed handUsed;
    TA_Direction direction;
    TA_Amplitude amplitude;
    TA_Width width;
    public int getTA_Amplitude(){
      switch (amplitude){
        case Pixel_144:{
          return 144;
        }
        case Pixel_288:{
          return 288;
        }
        case Pixel_576:{
          return 576;
        }
        default: {
          println("None valide width value for TA");
          return 0;
        }
      }
    }
    public int getTA_Width(){
      switch (width){
        case Pixel_24:{
          return 24;
        }
        case Pixel_48:{
          return 48;
        }
        case Pixel_96:{
          return 96;
        }
        default: {
          println("None valide width value for TA");
          return 0;
        }
      }
    }
   
}

int NumErrorTA=0;
int NumTotalTA=0;
float ErrorRateTA=0.0f;
float UpperLimitErrorRateTA=0.04f;
float LowerLimitErrorRateTA=0.04f;
boolean bEnableCalculateErrorRateTA=false;
boolean bCalculateErrorRateTA=true;
int TA_WarningState=0;

final int TA_StayInOnePositionThreshold=60;//Nearly 1000ms

public class TargetAcquisitionParavector extends TargetAcquisitionPara{
  Vector<TargetAcquisitionPara> TargetAcquisitionParaVector=new Vector<TargetAcquisitionPara>();
  //List <TargetAcquisitionPara> TargetAcquisitionParaVector= Collections.synchronizedList (new ArrayList<TargetAcquisitionPara>());
  //Iterator<TargetAcquisitionPara> itTTAParavector = TargetAcquisitionParaVector.iterator();
  /*
  // following code is to generate experiment parameters randomly for debug phase 
  public void initTargetAcquisitionParaVectorManu(){
    
    for(int i=0;i<10;i++){
      TargetAcquisitionParaVector.add(creatTargetAcquisitionPara());
      println(i +"th element been created");
    }
  }
  
  public TargetAcquisitionPara creatTargetAcquisitionPara(){
    TargetAcquisitionPara TempTargetAcquisitionPara= new TargetAcquisitionPara();
    TempTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
    switch(ceil(random(2))){
      case 1:TempTargetAcquisitionPara.direction=TA_Direction.Horizontal;break;
      case 2:TempTargetAcquisitionPara.direction=TA_Direction.Vertical;break;
    }
    //TempTargetAcquisitionPara.amplitude=();
    switch(ceil(random(3))){
      case 1:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_144;break;
      case 2:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_288;break;
      case 3:TempTargetAcquisitionPara.amplitude=TA_Amplitude.Pixel_576;break;
    }
    switch(ceil(random(3))){
      case 1:TempTargetAcquisitionPara.width=TA_Width.Pixel_24;break;
      case 2:TempTargetAcquisitionPara.width=TA_Width.Pixel_48;break;
      case 3:TempTargetAcquisitionPara.width=TA_Width.Pixel_96;break;
    }
    return TempTargetAcquisitionPara;
  }
  */
}

/*StateTrainingTA concerned variables*/
float TTA_StartButtonCenterX;
float TTA_StartButtonCenterY;
float TTA_StartButtonWidth;
float TTA_StartButtonHeight;
boolean bPassToTTA_Init;

TargetAcquisitionPara CurrentTargetAcquisitionPara=new TargetAcquisitionPara();

TA_HandUsed TA_CurrentHandUsed=TA_HandUsed.Left;
TA_Direction TA_CurrentDirection=TA_Direction.Horizontal;
TA_Amplitude TA_CurrentAmplitude=TA_Amplitude.Pixel_144;
TA_Width TA_CurrentWidth=TA_Width.Pixel_24;

/*StateTrainingTA_Decide concerned variables*/
boolean whichSideToHighlight=false;//False as left, ture as right
float TTA_TargetBarCenterX;
float TTA_TargetBarCenterY;
float TTA_TargetBarWidth;
float TTA_TargetBarHeight;
int TTA_ReleasedX;
int TTA_ReleasedY;

/*StateTrainingTA_Wait concerned variables*/
//int goalTATrainNum=4;
int currentTATrainNum=0;
//StateSubTASet CurrentStateSubTA=StateSubTASet.StateSubTAinit;

TargetAcquisitionParavector TTAParavector= new TargetAcquisitionParavector();
//Semaphore semaphoreTTAParavector = new Semaphore(1);
//Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
long TimestampBefore=0;
long TimestampCurrent=0;
final int awaitingTimeAfterInit =1000;

public void initStateTTA(){
  /*StateTrainingTA concerned variables*/
  TTA_StartButtonCenterX=width/2-200;
  TTA_StartButtonCenterY=height/2-Y_OffsetForComfort;
  TTA_StartButtonWidth=250;
  TTA_StartButtonHeight=100;
  bPassToTTA_Init=false;
}

public void FuncStateTrainingTA(){
    println("Enter StateTrainingTA");
      background(colorBackground);
      fill(255);
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to \n target acquisition \n training set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(TTA_StartButtonCenterX,TTA_StartButtonCenterY,TTA_StartButtonWidth,TTA_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToTTA_Init){
                 //println(StateSet.StateTrainingTA_Init); 
          background(colorBackground);
          fill(255);
          
          //TTAParavector.initTargetAcquisitionParaVectorManu();
          //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
          println("StateTrainingTA"+ TTAParavector.TargetAcquisitionParaVector.size());
          if(TTAParavector.TargetAcquisitionParaVector.size()>0){
              CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Right;
                }break;
                case "left":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
                }break;
              }
      
              TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
              switch(TA_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to touch highlighted bar", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to touch highlighted bar", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrainingTA_Init;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonHeight); */
          }
          else{
            println("None valide training set for TA"); 
          }
        //CurrentState=StateSet.StateTrainingTA_Init;
        bPassToTTA_Init=false;
      }
}

public void FuncStateTrainingTA_Init(){
  println("Enter StateTrainingTA_Init");
      //println("StateTrainingTA_Init"+ TTAParavector.TargetAcquisitionParaVector.size());
      //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      //TimestampCurrent=timestamp.getTime();
      //static boolean bAlreadyGetPara=false;
      if(TTAParavector.TargetAcquisitionParaVector.size()>0){
        CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
        TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
      }
      /*if((TimestampCurrent-TimestampBefore)>(2*awaitingTimeAfterInit)){
        CurrentState=StateSet.StateTrainingTA_Decide;
      }*/
      //else if((TimestampCurrent-TimestampBefore)>awaitingTimeAfterInit){
          background(colorBackground);
          fill(255);
          switch (TA_CurrentDirection){
            case Horizontal:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
            }break;
            case Vertical:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
            }break;
            default:{
                println("None valide direction TA"); 
            }break;
          }
          
          CurrentState=StateSet.StateTrainingTA_Decide;
          holdImagTime=1000;
      //}
}

public void FuncStateTrainingTA_Decide(){
  println("Enter StateTrainingTA_Decide");
      //println("StateTrainingTA_Decide"+ TTAParavector.TargetAcquisitionParaVector.size());
      background(colorBackground);
      fill(255); 
      //int tempCounter=0;
      if(TTAParavector.TargetAcquisitionParaVector.size()>0){
          //tempCounter++;
          //println(tempCounter);
          CurrentTargetAcquisitionPara=TTAParavector.TargetAcquisitionParaVector.get(0);
          TTAParavector.TargetAcquisitionParaVector.remove(0);
          whichSideToHighlight=(random(1)>0.5f)?true:false;
          CurrentState=StateSet.StateTrainingTA_Wait;
          currentTATrainNum=0;
      }
      else{
          //println(tempCounter);
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
      }
}

public void FuncStateTrainingTA_Wait(){
      println("Enter StateTrainingTA_Wait");
      if(currentTATrainNum<CurrentTargetAcquisitionPara.RepeatNum){
        whichSideToHighlight=!whichSideToHighlight;
        currentTATrainNum++;
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                if(bEnableMirrorImage){
                  TTA_TargetBarCenterX=whichSideToHighlight?(width-barAmplitude)/2:(width+barAmplitude)/2;
                }
                else{
                  TTA_TargetBarCenterX=whichSideToHighlight?(width+barAmplitude)/2:(width-barAmplitude)/2;                
                }
                TTA_TargetBarCenterY=height/2-Y_OffsetForComfort;
                TTA_TargetBarWidth=barWidth;
                TTA_TargetBarHeight=height+2*Y_OffsetForComfort;
                CurrentState=StateSet.StateSubTTAinit;
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                TTA_TargetBarCenterX=width/2;
                TTA_TargetBarCenterY=whichSideToHighlight?(height+barAmplitude)/2-Y_OffsetForComfort:(height-barAmplitude)/2-Y_OffsetForComfort;
                TTA_TargetBarWidth=width;
                TTA_TargetBarHeight=barWidth;
                CurrentState=StateSet.StateSubTTAinit;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      }
      else{
        CurrentState=StateSet.StateTrainingTA_Decide;
      }
}

public void FuncStateSubTTAinit(){
  println("Enter StateSubTTAinit");
        
        // code move to UsedMousePressed Event
        if (UsedMousePressed){
          CurrentState=StateSet.StateSubTTApressed;
        }
        
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}


public void FuncStateSubTTApressed(){
  println("Enter StateSubTTApressed");
        background(colorBackground);
        fill(255);
        
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
       if(UsedMousePressed==false){
            CurrentState=StateSet.StateSubTTAjudge;
            TTA_ReleasedX=UsedMouseX;
            TTA_ReleasedY=UsedMouseY;
            StayInOnePosition=0;       
       }
       //else{
       // if(oldMouseX==UsedMouseX &&oldMouseY==UsedMouseY){
       //   StayInOnePosition++;
       //   if(StayInOnePosition>=TA_StayInOnePositionThreshold){
       //     //CurrentState=StateSet.StateSubTTAjudge;
       //     TTA_ReleasedX=UsedMouseX;
       //     TTA_ReleasedY=UsedMouseY;
       //     //StayInOnePosition=0;
       //   }
       // }
       // else{
       //   StayInOnePosition=0;
       // }       
       //}
      
}

public void TA_Patch(){
   println("Enter TA_Patch");
   TTA_ReleasedX=UsedMouseX;
   TTA_ReleasedY=UsedMouseY;
   ATA_ReleasedX=UsedMouseX;
   ATA_ReleasedY=UsedMouseY;            
  //if(UsedMousePressed==false){
    switch(CurrentState){
      case StateSubTTApressed:{
            CurrentState=StateSet.StateSubTTAjudge;
            StayInOnePosition=0;
      }break;
      case StateSubATApressed:{
            CurrentState=StateSet.StateSubATAjudge;
            StayInOnePosition=0;      
      }break;
      default:break;
    }
  //}
  //else{
  //  switch(CurrentState){
  //    case StateSubTTAinit:{
  //      CurrentState=StateSet.StateSubTTApressed;
  //    }break;
  //    case StateSubATAinit:{
  //      CurrentState=StateSet.StateSubATApressed;
  //    }break;
  //    default:break;      
  //  }

  //}
  
}

public void FuncStateSubTTAjudge(){
  println("Enter StateSubTTAjudge");
      if(RectButtonReleased(TTA_TargetBarCenterX,TTA_TargetBarCenterY,TTA_TargetBarWidth,TTA_TargetBarHeight,TTA_ReleasedX,TTA_ReleasedY)){
        CurrentState=StateSet.StateTrainingTA_Acquired;
      }
      else{
        CurrentState=StateSet.StateTrainingTA_Missed;
      }
}

public void FuncStateTrainingTA_Acquired(){
  println("Enter StateTrainingTA_Acquired");
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}


public void FuncStateTrainingTA_Missed(){
  println("Enter StateTrainingTA_Missed");
        background(colorBackground);
        fill(255);
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           CurrentState=StateSet.StateTrainingTA_Wait;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}

/*StateTrainingTA concerned variables*/
float ATA_StartButtonCenterX;
float ATA_StartButtonCenterY;
float ATA_StartButtonWidth;
float ATA_StartButtonHeight;
boolean bPassToATA_Init;


/*StateTrainingTA_Decide concerned variables*/
//boolean whichSideToHighlight=false;//False as left, ture as right
float ATA_TargetBarCenterX;
float ATA_TargetBarCenterY;
float ATA_TargetBarWidth;
float ATA_TargetBarHeight;
int ATA_ReleasedX;
int ATA_ReleasedY;

/*StateTrainingTA_Wait concerned variables*/
//int goalTATrialNum=4;
int currentTATrialNum=0;
//StateSubTASet CurrentStateSubTA=StateSubTASet.StateSubTAinit;

TargetAcquisitionParavector ATAParavector= new TargetAcquisitionParavector();

public void initStateATA(){
  /*StateTrainingTA concerned variables*/
  ATA_StartButtonCenterX=width/2-200;
  ATA_StartButtonCenterY=height/2-Y_OffsetForComfort;
  ATA_StartButtonWidth=250;
  ATA_StartButtonHeight=100;
  bPassToATA_Init=false;
}

public void FuncStateTrialTA(){
  println("Enter StateTrialTA");
      background(colorBackground);
      fill(255);
      
      println("StateTrialTA"); 
      /*Text of welcome*/
      rectMode(CENTER);
      textSize(60);
      fill(colorText);
      text("Welcome to \n target acquisition \n trial set ", width/2,height/2,600,370.8f); 
      /*Button of begining*/
      rectMode(CENTER);
      fill(colorText);
      rect(ATA_StartButtonCenterX,ATA_StartButtonCenterY,ATA_StartButtonWidth,ATA_StartButtonHeight); 
      rectMode(CENTER);
      textSize(100);
      fill(255);
      text("Start", width/2-200,height/2-Y_OffsetForComfort,250,100);
      if(bPassToATA_Init){
                 //println(StateSet.StateTrainingTA_Init); 
          background(colorBackground);
          fill(255);
          
          //ATAParavector.initTargetAcquisitionParaVectorManu();
          //Iterator<TargetAcquisitionPara> itTTAParavector = TTAParavector.TargetAcquisitionParaVector.iterator();
          println("StateTrainingTA"+ ATAParavector.TargetAcquisitionParaVector.size());
          if(ATAParavector.TargetAcquisitionParaVector.size()>0){
              CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
                switch(CurrentHandUsed){
                case "right":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Right;
                }break;
                case "left":{
                  CurrentTargetAcquisitionPara.handUsed=TA_HandUsed.Left;
                }break;
              }
      
              TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
              //TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
              TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
              TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
              switch(TA_CurrentHandUsed){
                  //TimestampBefore=0;
                  //TimestampCurrent=0;
                  case Left : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your left index finger to touch highlighted bar", width/2,height/2,600,370.8f); 
                  }break;
                  case Right : {
                    rectMode(CENTER);
                    textSize(60);
                    fill(colorText);
                    text("Please use your right index finger to touch highlighted bar", width/2,height/2,600,370.8f); 
                  }break;
                  default:{
                    println("HandUsed not valid"); 
                  }break;
              }
              holdImagTime=1000;
              //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              //TimestampBefore=timestamp.getTime();
              CurrentState=StateSet.StateTrialTA_Init;
              /*delay(awaitingTimeAfterInit);
              background(colorBackground);
              fill(255);*/
              /*rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,height/2,TTA_StartButtonWidth,TTA_StartButtonHeight); */
          }
          else{
            println("None valide trial set for TA"); 
          }
        //CurrentState=StateSet.StateTrainingTA_Init;
        bPassToATA_Init=false;
      }
}

public void FuncStateTrialTA_Init(){
    println("Enter StateTrialTA_Init");
      //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      //TimestampCurrent=timestamp.getTime();
      //static boolean bAlreadyGetPara=false;
      if(ATAParavector.TargetAcquisitionParaVector.size()>0){
        CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
        //TA_CurrentHandUsed=CurrentTargetAcquisitionPara.handUsed;
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
      }
      /*if((TimestampCurrent-TimestampBefore)>(2*awaitingTimeAfterInit)){
        CurrentState=StateSet.StateTrainingTA_Decide;
      }*/
      //else if((TimestampCurrent-TimestampBefore)>awaitingTimeAfterInit){
          background(colorBackground);
          fill(255);
          switch (TA_CurrentDirection){
            case Horizontal:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
            }break;
            case Vertical:{
              int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
              int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
              rectMode(CENTER);
              fill(colorInactivated);
              rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth); 
            }break;
            default:{
                println("None valide direction TA"); 
            }break;
          }
          
          CurrentState=StateSet.StateTrialTA_Decide;
          
          /*Code concerned error rate */
          bEnableCalculateErrorRateTA=false;
          NumErrorTA=0;
          NumTotalTA=0;
          ErrorRateTA=0.04f; // to avoid triggering warning when just begin test 

          holdImagTime=1000;
      //}
}

boolean bRecordTrialTA=false;
public void FuncStateTrialTA_Decide(){
      println("Enter StateTrialTA_Decide");
      background(colorBackground);
      fill(255); 
      //int tempCounter=0;
      if(ATAParavector.TargetAcquisitionParaVector.size()>0){
          //tempCounter++;
          //println(tempCounter);
          CurrentTargetAcquisitionPara=ATAParavector.TargetAcquisitionParaVector.get(0);
          ATAParavector.TargetAcquisitionParaVector.remove(0);
          whichSideToHighlight=(random(1)>0.5f)?true:false;
          CurrentState=StateSet.StateTrialTA_Wait;
          bEnableCalculateErrorRateTA=false;
          currentTATrainNum=0;
          bRecordTrialTA=false;
      }
      else{
          //println(tempCounter);
          CurrentState=StateSet.StateDecideTask;
          println("No more parameters"); 
      }
}

final int additionalTrainingNum=4;

public void FuncStateTrialTA_Wait(){
      println("Enter StateTrialTA_Wait");
      if(currentTATrainNum<CurrentTargetAcquisitionPara.RepeatNum+additionalTrainingNum){
        
         if (currentTATrainNum>=additionalTrainingNum){
              bRecordTrialTA=true;
              bEnableCalculateErrorRateTA=true;// Only trial phase been used to calculate error rate, training phase not.
         } 
        
        if(bRecordTrialTA){
          //CurrentTable= new Table();
          /*log*/
          CurrentRow= CurrentTable.addRow();
          CurrentRowId++;
          CurrentRow.setInt("id", CurrentRowId);
          //println("id:"+ CurrentRowId);
          CurrentRow.setString("TaskType", "TA");
          //println("TaskType:"+ "TA");
          CurrentRow.setString("HandUsed", CurrentHandUsed);
          //println("HandUsed:"+ CurrentHandUsed);
        }
        println("bRecordTrialTA:" + bRecordTrialTA);
        whichSideToHighlight=!whichSideToHighlight;
        currentTATrainNum++;
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        TA_CurrentAmplitude=CurrentTargetAcquisitionPara.amplitude;
        TA_CurrentWidth=CurrentTargetAcquisitionPara.width;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                if(bRecordTrialTA){
                  /*log*/
                  CurrentRow.setString("Direction", "Horizontal");
                  CurrentRow.setInt("Amplitude/Length", barAmplitude);
                  CurrentRow.setInt("Width", barWidth);
                }
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                if(bEnableMirrorImage){
                  ATA_TargetBarCenterX=whichSideToHighlight?(width-barAmplitude)/2:(width+barAmplitude)/2;
                  if(bRecordTrialTA){
                    String SubDirection=whichSideToHighlight?"R":"L";
                    /*log*/
                    CurrentRow.setString("SubDirection", SubDirection);
                  }
                }
                else{
                  ATA_TargetBarCenterX=whichSideToHighlight?(width+barAmplitude)/2:(width-barAmplitude)/2;
                  if(bRecordTrialTA){
                    String SubDirection=whichSideToHighlight?"R":"L";
                    /*log*/
                    CurrentRow.setString("SubDirection", SubDirection);
                  }
                }
                ATA_TargetBarCenterY=height/2-Y_OffsetForComfort;
                ATA_TargetBarWidth=barWidth;
                ATA_TargetBarHeight=height+2*Y_OffsetForComfort;
                CurrentState=StateSet.StateSubATAinit;
        }break;
        case Vertical:{
                if(bRecordTrialTA){
                  /*log*/
                  CurrentRow.setString("Direction", "Vertical");
                  CurrentRow.setInt("Amplitude/Length", barAmplitude);
                  CurrentRow.setInt("Width", barWidth);
                }
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                ATA_TargetBarCenterX=width/2;
                ATA_TargetBarCenterY=whichSideToHighlight?(height+barAmplitude)/2-Y_OffsetForComfort:(height-barAmplitude)/2-Y_OffsetForComfort;
                if(bRecordTrialTA){
                  String SubDirection=whichSideToHighlight?"D":"U";
                  /*log*/
                  CurrentRow.setString("SubDirection", SubDirection);
                }
                ATA_TargetBarWidth=width;
                ATA_TargetBarHeight=barWidth;
                CurrentState=StateSet.StateSubATAinit;
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      }
      else{
        CurrentState=StateSet.StateTrialTA_Decide;
      }
      
}

public void FuncStateSubATAinit(){
  println("Enter StateSubATAinit");
        if (UsedMousePressed == true){
          CurrentState=StateSet.StateSubATApressed;
           if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              StartTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("StartTime", Long.toString(StartTime));
              CurrentRow.setString("StartPosition", "("+Float.toString(UsedMouseX)+","+Float.toString(UsedMouseY)+")");
              CurrentRow.setString("StartTime", Long.toString(StartTime));
              bRecordTrajectory= true;
           }
        }
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
}

public void FuncStateSubATApressed(){
  println("Enter StateSubATApressed");
        /*
        // move to mouseReleased
        ATA_ReleasedX=UsedMouseX;
        ATA_ReleasedY=UsedMouseY;
        if (UsedMousePressed == false){
          CurrentState=StateSet.StateSubATAjudge;
        }
        */
        
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        switch (TA_CurrentDirection){
        case Horizontal:{
                rectMode(CENTER);
                //left false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
                rectMode(CENTER);
                //right true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
        }break;
        case Vertical:{
                rectMode(CENTER);
                //down false
                fill(whichSideToHighlight?colorInactivated:colorActivated);
                rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
                rectMode(CENTER);
                //up true
                fill(whichSideToHighlight?colorActivated:colorInactivated);
                rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
       if(UsedMousePressed==false){
          CurrentState=StateSet.StateSubATAjudge;
          ATA_ReleasedX=UsedMouseX;
          ATA_ReleasedY=UsedMouseY;
          StayInOnePosition=0;
       }
       //else{
       //   if(oldMouseX==UsedMouseX &&oldMouseY==UsedMouseY){
       //   StayInOnePosition++;
       //   if(StayInOnePosition>=TA_StayInOnePositionThreshold){
       //     //CurrentState=StateSet.StateSubATAjudge;
       //     ATA_ReleasedX=UsedMouseX;
       //     ATA_ReleasedY=UsedMouseY;
       //     //StayInOnePosition=0;
       //     CurrentRow.setString("Remark", "Over Time");
       //   }
       // }
       //   else{
       //     StayInOnePosition=0;
       //   }
       //}

  
}

public void FuncStateSubATAjudge(){
  if(bEnableCalculateErrorRateTA){
    DisplayTA_WarningMsg();
  }
  println("Enter StateSubATAjudge");
      if(RectButtonReleased(ATA_TargetBarCenterX,ATA_TargetBarCenterY,ATA_TargetBarWidth,ATA_TargetBarHeight,ATA_ReleasedX,ATA_ReleasedY)){
        CurrentState=StateSet.StateTrialTA_Acquired;
          if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              EndTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("EndTime", Long.toString(EndTime));
              CurrentRow.setString("State", "Successful");
              CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
              CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
              bRecordTrajectory= false;
           }
      }
      else{
        CurrentState=StateSet.StateTrialTA_Missed;
        if(bRecordTrialTA){
              Timestamp timestamp = new Timestamp(System.currentTimeMillis());
              EndTime=timestamp.getTime();
              /*log*/
              CurrentRow.setString("EndTime", Long.toString(EndTime));
              CurrentRow.setString("Duration", Long.toString(EndTime-StartTime));
              CurrentRow.setString("State", "Failed");
              CurrentRow.setString("EndPosition", "("+Float.toString(ATA_ReleasedX)+","+Float.toString(ATA_ReleasedY)+")");
              bRecordTrajectory= false;
              //CurrentRow.setString("StartTime", Long.toString(StartTime));
        }
      }
      
      /*Code concerned error rate */
      if(bCalculateErrorRateTA){
        if(bEnableCalculateErrorRateTA){
          NumTotalTA=NumTotalTA+1;
          println("NumTotalTA :", NumTotalTA,"NumErrorTA:", NumErrorTA, "ErrorRateTA:", ErrorRateTA);
        }
      }
}

public void FuncStateTrialTA_Acquired(){
  println("Enter StateTrialTA_Acquired");
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        //if(bRecordTrialTA){
        //  saveTable(CurrentTable, ExternalStorage+CurrentTableName);
        //}
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        
        if(bCalculateErrorRateTA){
          FuncJudgeSpeedWarningTA();
        }
               
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Acquired;
               //holdImagTime=200;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
          
        }break;
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorAcquired);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorAcquired:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
         if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Acquired;
           }
            else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
      
}

public void FuncStateTrialTA_Missed(){
  println("Enter StateTrialTA_Missed");
        background(colorBackground);
        fill(255);
        if(bEnableCalculateErrorRateTA){
          DisplayTA_WarningMsg();
        }
        TA_CurrentDirection=CurrentTargetAcquisitionPara.direction;
        int barWidth=CurrentTargetAcquisitionPara.getTA_Width();
        int barAmplitude=CurrentTargetAcquisitionPara.getTA_Amplitude();
        
        if(bCalculateErrorRateTA){
            FuncJudgeSpeedWarningTA();
        }
               
        switch (TA_CurrentDirection){
        case Horizontal:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect((width-barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort); 
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect((width+barAmplitude)/2,height/2-Y_OffsetForComfort,barWidth,height+2*Y_OffsetForComfort);
                    
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Missed;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        
        case Vertical:{
           rectMode(CENTER);
           fill(whichSideToHighlight?colorInactivated:colorMissed);
           rect(width/2,(height-barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           rectMode(CENTER);
           fill(whichSideToHighlight?colorMissed:colorInactivated);
           rect(width/2,(height+barAmplitude)/2-Y_OffsetForComfort,width,barWidth);
           if(bRecordTrialTA){
              //saveTable(CurrentTable, ExternalStorage+CurrentTableName);
               CurrentState=StateSet.StateSaveCSV;
               OldState=StateSet.StateTrialTA_Missed;
           }
           else{
                    holdImagTime=ImagHoldingTime;
                    CurrentState=StateSet.StateTrialTA_Wait;
           }
        }break;
        default:{
                  println("None valide direction TA"); 
        }break;
       }
       
      /*Code concerned error rate */
      if(bCalculateErrorRateTA){
        if(bEnableCalculateErrorRateTA){
          NumErrorTA=NumErrorTA+1;
        }
      }
      
}

public void FuncJudgeSpeedWarningTA(){
  println("Enter FuncJudgeSpeedWarningTA");
       if(bEnableCalculateErrorRateTA && NumTotalTA%4==0 && NumTotalTA>=4 ){
           ErrorRateTA= (float)NumErrorTA/ (float)NumTotalTA;
           if(ErrorRateTA>UpperLimitErrorRateTA){
                 CurrentState=StateSet.StateWarnSpeed;
                 TA_WarningState=1; //nead to slow down
           }
           else if(ErrorRateTA<LowerLimitErrorRateTA){
              CurrentState=StateSet.StateWarnSpeed;
              TA_WarningState=2; //nead to speed up
            }
           else{
             TA_WarningState=0;//no need to warn
             CurrentState=StateSet.StateTrialTA_Wait;
           }
        }
        else{
          TA_WarningState=0;
        }
}

public void DisplayTA_WarningMsg(){
    String MagWarningTA;
    
    switch(TA_WarningState){
      case 0: return;
      case 1:MagWarningTA="Try to slow down";break;
      case 2:MagWarningTA="Try to speed up";break;
      default: return;
    }
    
    switch (TA_CurrentDirection){
       case Horizontal:{
           switch(TA_CurrentAmplitude){
              case Pixel_144:{
                //return 144;
                rectMode(CORNER);
                textSize(80);
                fill(colorText);
                text(MagWarningTA, 0,100,300,600); 
              }break;
              case Pixel_288:{
                //return 288;
                rectMode(CORNER);
                textSize(80);
                fill(colorText);
                text(MagWarningTA, 0,100,300,600); 
              }break;
              case Pixel_576:{
                 rectMode(CORNER);
                 textSize(80);
                 fill(colorText);
                 text(MagWarningTA, width/2-100,100,300,600); 
              }break;
              default: {
                println("None valide width value for TA");
                //return 0;
              }break;
           }
       }break;
       case Vertical:{
          switch(TA_CurrentAmplitude){
            case Pixel_576:{
               rectMode(CORNER);
               textSize(80);
               fill(colorText);
               text(MagWarningTA, width/2-300,0,800,370.8f);             
            }break;
            case Pixel_144:
            case Pixel_288:{
               rectMode(CORNER);
               textSize(80);
               fill(colorText);
               text(MagWarningTA, width/2-300,100,800,370.8f);             
            }break;              
            default: {
                println("None valide width value for TA");
                //return 0;
            }break;            
          }

       }break;
       default:{
                  println("None valide direction TA"); 
       }break;
    }
}






Context context;
SensorManager manager;
Sensor sensorAccelerometer;
AccelerometerListener listenersensorAccelerometer;
float ax, ay, az;

Sensor sensorGyroscope;
GyroscopeListener listenersensorGyroscope;
float wx, wy, wz;

public void initIMU() {
  //fullScreen();
  
  context = getActivity();
  manager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
  
  /*Accelerometer*/
  sensorAccelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
  listenersensorAccelerometer = new AccelerometerListener();
  manager.registerListener(listenersensorAccelerometer, sensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
  /*Gyroscope*/
  sensorGyroscope = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
  listenersensorGyroscope = new GyroscopeListener();
  manager.registerListener(listenersensorGyroscope, sensorGyroscope, SensorManager.SENSOR_DELAY_GAME);
  
  //textFont(createFont("SansSerif", 30 * displayDensity));
}

//void draw() {
//  background(0);
//  text("X: " + ax + "\nY: " + ay + "\nZ: " + az, 0, 0, width, height);
  
//  text("X: " + wx + "\nY: " + wy + "\nZ: " + wz,width/2, height/2);
//}

class AccelerometerListener implements SensorEventListener {
  public void onSensorChanged(SensorEvent event) {
    ax = event.values[0];
    ay = event.values[1];
    az = event.values[2];    
  }
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}

class GyroscopeListener implements SensorEventListener {
  public void onSensorChanged(SensorEvent event) {
    wx = event.values[0];
    wy = event.values[1];
    wz = event.values[2];    
  }
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }
}
  public void settings() {  fullScreen(); }
}
